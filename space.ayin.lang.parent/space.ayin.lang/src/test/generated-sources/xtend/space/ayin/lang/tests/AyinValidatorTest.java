package space.ayin.lang.tests;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.testing.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Test;
import org.junit.runner.RunWith;
import space.ayin.lang.ayin.AyinEntity;
import space.ayin.lang.ayin.AyinPackage;
import space.ayin.lang.tests.AyinInjectorProvider;
import space.ayin.lang.validation.AyinValidator;

@RunWith(XtextRunner.class)
@InjectWith(AyinInjectorProvider.class)
@SuppressWarnings("all")
public class AyinValidatorTest {
  @Inject
  @Extension
  private ParseHelper<AyinEntity> _parseHelper;
  
  @Inject
  @Extension
  private ValidationTestHelper _validationTestHelper;
  
  @Inject
  private Provider<ResourceSet> resourceSetProvider;
  
  @Test
  public void noOverrideRetType() {
    try {
      final ResourceSet resourceSet = this.resourceSetProvider.get();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("interface SodaMachineActions {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("void pourSodaWithSyrupOne () ;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("i64 pourSodaWithSyrupOne (i64 x) ;");
      _builder.newLine();
      _builder.append("}");
      final AyinEntity iface = this._parseHelper.parse(_builder, resourceSet);
      this._validationTestHelper.assertError(iface, 
        AyinPackage.eINSTANCE.getAyinInterface(), 
        AyinValidator.NO_OVERRIDE_FOR_RETURN_TYPE, 
        "override of return type if forbidden");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void uniqueEvents() {
    try {
      final ResourceSet resourceSet = this.resourceSetProvider.get();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("interface SodaMachineActions {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("event onStart();");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("event onStart();");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("event onStart(i64 ms);");
      _builder.newLine();
      _builder.append("}");
      final AyinEntity iface = this._parseHelper.parse(_builder, resourceSet);
      this._validationTestHelper.assertError(iface, 
        AyinPackage.eINSTANCE.getAyinInterface(), 
        AyinValidator.NON_UNIQUE_EVENTS, 
        "duplicated events");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void uniqueMethods() {
    try {
      final ResourceSet resourceSet = this.resourceSetProvider.get();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("interface SodaMachineActions {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("void initialize ( ) ;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("void pourSodaWithSyrupOne () ;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("void pourSodaWithSyrupOne () ;");
      _builder.newLine();
      _builder.append("}");
      final AyinEntity iface = this._parseHelper.parse(_builder, resourceSet);
      this._validationTestHelper.assertError(iface, 
        AyinPackage.eINSTANCE.getAyinInterface(), 
        AyinValidator.NON_UNIQUE_METHODS, 
        "duplicated methods");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
