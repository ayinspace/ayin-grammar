package space.ayin.lang.tests

import com.google.inject.Inject
import com.google.inject.Provider
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.Test
import org.junit.runner.RunWith
import space.ayin.lang.ayin.AyinEntity
import space.ayin.lang.ayin.AyinPackage
import space.ayin.lang.validation.AyinValidator

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(AyinInjectorProvider))
class AyinValidatorTest {
	@Inject extension ParseHelper<AyinEntity>
	@Inject extension ValidationTestHelper
	@Inject Provider<ResourceSet> resourceSetProvider
	
	@Test def noOverrideRetType() {
		val resourceSet = resourceSetProvider.get
		val iface ='''
		interface SodaMachineActions {
			void pourSodaWithSyrupOne () ;
			i64 pourSodaWithSyrupOne (i64 x) ;
		}'''.parse(resourceSet)
		iface.assertError(
			AyinPackage.eINSTANCE.ayinInterface,
			AyinValidator.NO_OVERRIDE_FOR_RETURN_TYPE,
			"override of return type if forbidden" // FIXME:
		)
	}
	
	@Test def uniqueEvents() {
		val resourceSet = resourceSetProvider.get
		val iface ='''
		interface SodaMachineActions {
			event onStart();
			event onStart();
			event onStart(i64 ms);
		}'''.parse(resourceSet)
		iface.assertError(
			AyinPackage.eINSTANCE.ayinInterface,
			AyinValidator.NON_UNIQUE_EVENTS,
			"duplicated events" // FIXME:
		)
	}
	
	@Test def uniqueMethods() {
		val resourceSet = resourceSetProvider.get
		val iface ='''
		interface SodaMachineActions {
			void initialize ( ) ;
			void pourSodaWithSyrupOne () ;
			void pourSodaWithSyrupOne () ;
		}'''.parse(resourceSet)
		iface.assertError(
			AyinPackage.eINSTANCE.ayinInterface,
			AyinValidator.NON_UNIQUE_METHODS,
			"duplicated methods" // FIXME:
		)
	}
	
}