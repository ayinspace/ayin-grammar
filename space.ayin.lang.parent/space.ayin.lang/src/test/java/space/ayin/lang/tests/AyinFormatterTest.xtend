package space.ayin.lang.tests

import com.google.inject.Inject
import org.eclipse.xtext.formatting2.FormatterRequest
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.formatter.FormatterTestHelper
import org.eclipse.xtext.testing.formatter.FormatterTestRequest
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(XtextRunner)
@InjectWith(AyinInjectorProvider)
class AyinFormatterTest {

	@Inject extension FormatterTestHelper

	@Test def void structFormat() {
		val testReq = new FormatterTestRequest()
		testReq.request = new FormatterRequest()
		testReq.toBeFormatted = '''struct S{i32 x;i32 y;}'''
		testReq.expectation = '''
		struct S {
			i32 x;
			i32 y;
		}'''
		testReq.assertFormatted
	}

	@Test def void enumFormat() {
		val testReq = new FormatterTestRequest()
		testReq.request = new FormatterRequest()
		testReq.toBeFormatted = '''enum E{E1;E2;E3;}'''
		testReq.expectation = '''
		enum E {
			E1;
			E2;
			E3;
		}'''
		testReq.assertFormatted
	}

	@Test def void interfaceFormat() {
		val testReq = new FormatterTestRequest()
		testReq.request = new FormatterRequest()
		testReq.toBeFormatted = '''interface Base { event onStart(i32 x,i32 y); 
		(i32 x,string msg)get(i64 y,i64 z){this.get(y=0,z=1);}}'''
		testReq.expectation = '''
		interface Base {
			event onStart(i32 x, i32 y);
			(i32 x, string msg) get(i64 y, i64 z) {
				this.get(y = 0, z = 1);
			}
		}'''
		testReq.assertFormatted
	}

	@Test def void executorFormat() {
		val testReq = new FormatterTestRequest()
		testReq.request = new FormatterRequest()

		val mess = '''executor Executor implements BaseEvents,BaseActions{onStart={i32 a=0;
		this.divideAndLog(x,y=this.divideAndLog(),message="test");i32 s,a=this.divideAndLog(x=a,y=0,message="test");}}'''

		val clean = '''
		executor Executor implements BaseEvents, BaseActions {
			onStart = {
				i32 a = 0;
				this.divideAndLog(x, y = this.divideAndLog(), message = "test");
				i32 s, a = this.divideAndLog(x = a, y = 0, message = "test");
			}
		}'''

		testReq.toBeFormatted = mess
		testReq.expectation = clean
		testReq.assertFormatted
	}

	@Test def void multiAssignFormat() {
		val testReq = new FormatterTestRequest()
		testReq.request = new FormatterRequest()
		testReq.toBeFormatted = '''executor A{onStart={i32 x;x=this.test();}}'''
		testReq.expectation = '''
		executor A {
			onStart = {
				i32 x;
				x = this.test();
			}
		}'''
		testReq.assertFormatted
	}
}
