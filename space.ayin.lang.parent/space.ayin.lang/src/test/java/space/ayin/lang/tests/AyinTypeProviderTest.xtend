package space.ayin.lang.tests

import org.junit.runner.RunWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.InjectWith
import com.google.inject.Inject
import org.eclipse.xtext.testing.util.ParseHelper
import space.ayin.lang.ayin.AyinEntity
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import com.google.inject.Provider
import org.eclipse.emf.ecore.resource.ResourceSet

import space.ayin.lang.ayin.AyinExecutor
import org.junit.Test
import space.ayin.lang.ayin.AyinPackage
import space.ayin.lang.validation.AyinValidator

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(AyinInjectorProvider))
class AyinTypeProviderTest {

	@Inject extension ParseHelper<AyinEntity>
	@Inject extension ValidationTestHelper
	@Inject Provider<ResourceSet> resourceSetProvider

	def private AyinExecutor parseExecutor(CharSequence testExp) {
		val resourceSet = resourceSetProvider.get
		'''struct Pin { i8 p; }'''.parse(resourceSet).assertNoErrors()
		'''struct Nip { i8 p; }'''.parse(resourceSet).assertNoErrors()
		'''struct SuperPin extends Pin { string message; }'''.parse(resourceSet).assertNoErrors()
		
		'''exception Exception { i32 code; }'''.parse(resourceSet).assertNoErrors()

		'''datatype List<T>'''.parse(resourceSet).assertNoErrors()

		'''exception SimpleException { i32 code; }'''.parse(resourceSet).assertNoErrors()
		'''exception ComplexException extends SimpleException { string message; }'''.parse(resourceSet).assertNoErrors()

		'''
			interface IBase {
				event onStart();
				void test(string msg, 
							callback test1(i8 code));
				void process(List<?> list);
			}
		'''.parse(resourceSet).assertNoErrors()

		return '''
			executor Executor implements IBase {
				onStart = {
					«testExp»
				}
			}
		'''.parse(resourceSet) as AyinExecutor
	}

	@Test def void callbackBlock() {
		'''
			i32 x = 0;
			i32 y = 0;
			i32 z = 0;
			this.test(msg = "test", test1 = {
				i32 x = 0;
			});
			
			this.test(msg = "test", test1 = {
				i32 a = x;
			});
		'''.parseExecutor.assertError(
			AyinPackage.eINSTANCE.ayinParameter,
			AyinValidator.DUPLICATED_PARAMETER,
			"duplicated parameter"
		)
	}

	@Test def void stringType() {
		'''
			string s = "test string";
		'''.parseExecutor.assertNoErrors()
	}

	@Test def void integerTypes() {
		''' 
			i8 x1 = 125;
			i16 x2 = 250;
			i32 x3 = 50000;
			i64 x4 = 1000000000;
			
			x4 = x4;
			x4 = x3;
			x4 = x2;
			x4 = x1;
			
			x3 = x3;
			x3 = x2;
			x3 = x1;
			
			x2 = x2;
			x2 = x1;
			
			x1 = x1;
		'''.parseExecutor.assertNoErrors()
	}

	@Test def void integerTypesErrors() {
		'''
			i8 x1 = 125;
			i16 x2 = 250;
			
			x1 = x2;
		'''.parseExecutor.assertError(
			AyinPackage.eINSTANCE.ayinAssignment,
			AyinValidator.INCOMPATIBLE_TYPES,
			"expected type 'i8' but got 'i16'"
		)
	}

	@Test def void dataTypes() {
		'''List<Pin> p = new List<Pin>();'''.parseExecutor.assertNoErrors()
	}

	@Test def void dataAntiTypes() {
		'''List<Pin> p = new List<Nip>();'''.parseExecutor.assertError(
			AyinPackage.eINSTANCE.ayinAssignment,
			AyinValidator.INCOMPATIBLE_TYPES,
			"expected type 'List<Pin>' but got 'List<Nip>'"
		)
	}

	@Test def void structTypes() {
		'''
			Pin p = new Pin();
			p.p = 3;
		'''.parseExecutor.assertNoErrors()
	}
	
	@Test def void assignmentSize() {
		'''i8 x, i8 y = 0'''.parseExecutor.assertError(
			AyinPackage.eINSTANCE.ayinAssignment,
			AyinValidator.ASSIGNMENT_SIZES_ERROR,
			"number of elements on the left side of assignment" +
			" is not equal to number of elements on the right side"
		)
	}
	
	@Test def void nonParentableInstantiation() {
		'''IBase b = new IBase();'''.parseExecutor.assertError(
			AyinPackage.eINSTANCE.ayinNewOperator,
			AyinValidator.NON_PARENTABLE_INSTANTIATION,
			"it is not allowed to instantiate AyinInterface"
		)
	}
	
	@Test def void nonExceptionInThrow() {
		'''throw new Pin();'''.parseExecutor.assertError(
			AyinPackage.eINSTANCE.ayinThrow,
			AyinValidator.NON_EXCEPTION_IN_THROW,
			"expected expression of type 'AyinException', but got 'AyinStruct'"
		)
	}
	
	@Test def void covarianceOfStruct() {
		'''Pin p = new SuperPin();'''.parseExecutor.assertNoErrors
	}
}
