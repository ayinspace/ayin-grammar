package space.ayin.lang

import java.util.AbstractMap
import java.util.ArrayList
import java.util.Collections
import java.util.List
import java.util.Map
import org.eclipse.emf.ecore.EObject
import space.ayin.lang.ayin.AyinAssignment
import space.ayin.lang.ayin.AyinBlock
import space.ayin.lang.ayin.AyinCallback
import space.ayin.lang.ayin.AyinEventHandler
import space.ayin.lang.ayin.AyinExpression
import space.ayin.lang.ayin.AyinMethod
import space.ayin.lang.ayin.AyinMethodCall
import space.ayin.lang.ayin.AyinNewOperator
import space.ayin.lang.ayin.AyinParameter
import space.ayin.lang.ayin.AyinParentable
import space.ayin.lang.ayin.AyinRef
import space.ayin.lang.ayin.AyinStructLike

import static extension org.eclipse.xtext.EcoreUtil2.*
import static extension space.ayin.lang.AyinTypeProvider.*
import space.ayin.lang.ayin.AyinEvent
import space.ayin.lang.ayin.AyinStruct

class AyinModelUtil {

	def static boolean isNotConflicting(AyinStruct s1, AyinStruct s2) {
		var res = s1.parameters.size != s2.parameters.size
		res = res && (s1.name == s2.name)
		for (var i = 0; i < s1.parameters.size; i++) {
			val p1 = s1.parameters.get(i)
			val p2 = s2.parameters.get(i)
			res = res && p1.isNotConflicting(p2)
		}
		
		return res
		
	}

	def static boolean isNotConflicting(AyinParameter p1, AyinParameter p2) {
		return (p1.name == p2.name) && (p1.type.isEquals(p2.type))
	}

	def static boolean isNotConflicting(AyinMethod m1, AyinMethod m2) {
		var res = (m1.name == m2.name)
		if (m1.paramStruct === null && m2.paramStruct === null) {
			return res
		}
		res = res && m1.paramStruct.isNotConflicting(m2.paramStruct)
		return res
	}
	
	def static boolean isNotConflicting(AyinEvent m1, AyinEvent m2) {
		return m1.name == m2.name
	}

	/**
	 * This method returns all parent's hierarchy of AyinParentable
	 * (i.e. parent, parent of parent, parent of parent of parent, etc.)
	 */
	def static List<AyinParentable> parentHierarchy(AyinParentable parentable) {
		val visited = new ArrayList<AyinParentable>
		var current = parentable.parent
		while (current !== null && !visited.contains(current)) {
			visited.add(current)
			current = current.parent
		}
		return visited
	}

	/**
	 * This method returns all parameters defined before expressions
	 * this includes:
	 *  1. all parameters of this block defined before
	 *  2. all parameters of outer block defined before the entrance in current block
	 *  3. parameters of event for current event handler
	 *  4. parameters of method (if we are inside script method)
	 *  5. parameters of callback (if we are inside callback block)
	 */
	def static List<AyinParameter> parametersDefinedBefore(AyinExpression expression) {
		val parameters = new ArrayList<AyinParameter>
		parameters.addAll(getParameters(expression))

		val entry = expression.getContainerOfTypeWithElem(typeof(AyinBlock))
		val block = entry.value
		val element = entry.key as AyinExpression

		parameters.addAll(element.parameterDefinedBeforeInBlock(block))
		
		var EObject elem = block
		var cont = block.eContainer
		while (cont !== null) {
			if (cont instanceof AyinBlock) {
				parameters.addAll((elem as AyinExpression).parameterDefinedBeforeInBlock(cont))
			}
			elem = cont
			cont = elem.eContainer
		}

		return parameters
	}
	
	/**
	 * TODO: add documentation
	 */
	def static List<AyinParameter> parameterDefinedBeforeInBlock(AyinExpression expression, AyinBlock block) {
		val parameters = new ArrayList<AyinParameter>
		val index = block.expressions.indexOf(expression)
		for (var i = 0; i < index; i++) {
			val expr = block.expressions.get(i)
			if (expr instanceof AyinAssignment) {
				val assign = expr
				val params = assign.left.filter(typeof(AyinParameter)).toList
				parameters.addAll(params)
			} else if (expr instanceof AyinParameter) {
				parameters.add(expr)
			}
		}
		
		val assignment = block.getContainerOfType(typeof(AyinAssignment))
		if (assignment !== null && assignment.left.size == 1) {
			val left = assignment.left.get(0)
			if (left instanceof AyinRef) {
				val ayinRef = left
				if (ayinRef.ref instanceof AyinCallback) {
					val callback = ayinRef.ref as AyinCallback
					parameters.addAll(callback.arguments)
				}
			}
		}
		
		return parameters
	}

	/**
	 * TODO: add documentation
	 */
	def static <T extends EObject> Map.Entry<EObject, T> getContainerOfTypeWithElem(EObject element, Class<T> type) {
		var elem = element
		var cont = elem
		while (cont !== null) {
			if (type.isInstance(cont)) {
				return new AbstractMap.SimpleEntry(elem, type.cast(cont))
			}
			elem = cont
			cont = cont.eContainer
		}
		return new AbstractMap.SimpleEntry(elem, null)
	}
	
	// bunch of methods to go up to inheritance chain to get all parameters
	
	def static dispatch List<AyinParameter> getParameters(AyinMethodCall methodCall) {
		val parameters = new ArrayList<AyinParameter>
		parameters.addAll(methodCall.method.paramStruct.fields)
		parameters.addAll(methodCall.method.callbacks)
		
		parameters.addAll(getParameters(methodCall.eContainer))
		return parameters
	}


	def static dispatch List<AyinParameter> getParameters(AyinEventHandler eventHandler) {
		val parameters = new ArrayList<AyinParameter>
		parameters.addAll(eventHandler?.event?.paramStruct?.fields ?: Collections.emptyList)
		
		parameters.addAll(getParameters(eventHandler.eContainer))
		return parameters
	}

	def static dispatch List<AyinParameter> getParameters(AyinNewOperator newOperator) {
		val parameters = new ArrayList<AyinParameter>
		if (newOperator.entity instanceof AyinStructLike) {
			parameters.addAll((newOperator.entity as AyinStructLike).fields)	
		} 
		
		parameters.addAll(getParameters(newOperator.eContainer))
		return parameters
	}

	def static dispatch List<AyinParameter> getParameters(AyinMethod method) {
		val parameters = new ArrayList<AyinParameter>
		parameters.addAll(method.resultStruct.fields)
		parameters.addAll(method.paramStruct.fields)
		
		parameters.addAll(getParameters(method.eContainer))
		return parameters
	}

	def static dispatch List<AyinParameter> getParameters(EObject obj) {
		return getParameters(obj.eContainer)
	}

	def static dispatch List<AyinParameter> getParameters(Void obj) {
		return Collections.emptyList
	}
}
