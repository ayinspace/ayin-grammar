/**
 * generated by Xtext 2.16.0
 */
package space.ayin.lang.ayin;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Wildcard</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see space.ayin.lang.ayin.AyinPackage#getAyinWildcard()
 * @model
 * @generated
 */
public interface AyinWildcard extends AyinType
{
} // AyinWildcard
