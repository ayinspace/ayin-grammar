package space.ayin.lang.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import space.ayin.lang.services.AyinGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalAyinParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INTEGER", "RULE_DOUBLE", "RULE_STRING", "RULE_BOOL", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "';'", "'.'", "'enum'", "'{'", "'}'", "'interface'", "'struct'", "'extends'", "'exception'", "'executor'", "'implements'", "','", "'datatype'", "'<'", "'>'", "'event'", "'('", "')'", "'void'", "'throws'", "'callback'", "'='", "'this'", "'if'", "'else'", "'while'", "'try'", "'catch'", "'finally'", "'throw'", "'new'", "'_'", "'?'", "'i8'", "'i16'", "'i32'", "'i64'", "'string'", "'boolean'", "'double'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__14=14;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=9;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=10;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=7;
    public static final int RULE_SL_COMMENT=11;
    public static final int T__37=37;
    public static final int RULE_DOUBLE=6;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=12;
    public static final int RULE_ANY_OTHER=13;
    public static final int RULE_BOOL=8;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int RULE_INTEGER=5;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalAyinParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalAyinParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalAyinParser.tokenNames; }
    public String getGrammarFileName() { return "InternalAyin.g"; }



     	private AyinGrammarAccess grammarAccess;

        public InternalAyinParser(TokenStream input, AyinGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "AyinEntity";
       	}

       	@Override
       	protected AyinGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleAyinEntity"
    // InternalAyin.g:64:1: entryRuleAyinEntity returns [EObject current=null] : iv_ruleAyinEntity= ruleAyinEntity EOF ;
    public final EObject entryRuleAyinEntity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinEntity = null;


        try {
            // InternalAyin.g:64:51: (iv_ruleAyinEntity= ruleAyinEntity EOF )
            // InternalAyin.g:65:2: iv_ruleAyinEntity= ruleAyinEntity EOF
            {
             newCompositeNode(grammarAccess.getAyinEntityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinEntity=ruleAyinEntity();

            state._fsp--;

             current =iv_ruleAyinEntity; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinEntity"


    // $ANTLR start "ruleAyinEntity"
    // InternalAyin.g:71:1: ruleAyinEntity returns [EObject current=null] : ( (this_AyinEnum_0= ruleAyinEnum | this_AyinInterface_1= ruleAyinInterface | this_AyinParentable_2= ruleAyinParentable | this_AyinData_3= ruleAyinData ) (otherlv_4= ';' )? ) ;
    public final EObject ruleAyinEntity() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        EObject this_AyinEnum_0 = null;

        EObject this_AyinInterface_1 = null;

        EObject this_AyinParentable_2 = null;

        EObject this_AyinData_3 = null;



        	enterRule();

        try {
            // InternalAyin.g:77:2: ( ( (this_AyinEnum_0= ruleAyinEnum | this_AyinInterface_1= ruleAyinInterface | this_AyinParentable_2= ruleAyinParentable | this_AyinData_3= ruleAyinData ) (otherlv_4= ';' )? ) )
            // InternalAyin.g:78:2: ( (this_AyinEnum_0= ruleAyinEnum | this_AyinInterface_1= ruleAyinInterface | this_AyinParentable_2= ruleAyinParentable | this_AyinData_3= ruleAyinData ) (otherlv_4= ';' )? )
            {
            // InternalAyin.g:78:2: ( (this_AyinEnum_0= ruleAyinEnum | this_AyinInterface_1= ruleAyinInterface | this_AyinParentable_2= ruleAyinParentable | this_AyinData_3= ruleAyinData ) (otherlv_4= ';' )? )
            // InternalAyin.g:79:3: (this_AyinEnum_0= ruleAyinEnum | this_AyinInterface_1= ruleAyinInterface | this_AyinParentable_2= ruleAyinParentable | this_AyinData_3= ruleAyinData ) (otherlv_4= ';' )?
            {
            // InternalAyin.g:79:3: (this_AyinEnum_0= ruleAyinEnum | this_AyinInterface_1= ruleAyinInterface | this_AyinParentable_2= ruleAyinParentable | this_AyinData_3= ruleAyinData )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 16:
                {
                alt1=1;
                }
                break;
            case 19:
                {
                alt1=2;
                }
                break;
            case 20:
            case 22:
            case 23:
                {
                alt1=3;
                }
                break;
            case 26:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalAyin.g:80:4: this_AyinEnum_0= ruleAyinEnum
                    {

                    				newCompositeNode(grammarAccess.getAyinEntityAccess().getAyinEnumParserRuleCall_0_0());
                    			
                    pushFollow(FOLLOW_3);
                    this_AyinEnum_0=ruleAyinEnum();

                    state._fsp--;


                    				current = this_AyinEnum_0;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;
                case 2 :
                    // InternalAyin.g:89:4: this_AyinInterface_1= ruleAyinInterface
                    {

                    				newCompositeNode(grammarAccess.getAyinEntityAccess().getAyinInterfaceParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_3);
                    this_AyinInterface_1=ruleAyinInterface();

                    state._fsp--;


                    				current = this_AyinInterface_1;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;
                case 3 :
                    // InternalAyin.g:98:4: this_AyinParentable_2= ruleAyinParentable
                    {

                    				newCompositeNode(grammarAccess.getAyinEntityAccess().getAyinParentableParserRuleCall_0_2());
                    			
                    pushFollow(FOLLOW_3);
                    this_AyinParentable_2=ruleAyinParentable();

                    state._fsp--;


                    				current = this_AyinParentable_2;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;
                case 4 :
                    // InternalAyin.g:107:4: this_AyinData_3= ruleAyinData
                    {

                    				newCompositeNode(grammarAccess.getAyinEntityAccess().getAyinDataParserRuleCall_0_3());
                    			
                    pushFollow(FOLLOW_3);
                    this_AyinData_3=ruleAyinData();

                    state._fsp--;


                    				current = this_AyinData_3;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalAyin.g:116:3: (otherlv_4= ';' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==14) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalAyin.g:117:4: otherlv_4= ';'
                    {
                    otherlv_4=(Token)match(input,14,FOLLOW_2); 

                    				newLeafNode(otherlv_4, grammarAccess.getAyinEntityAccess().getSemicolonKeyword_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinEntity"


    // $ANTLR start "entryRuleQN"
    // InternalAyin.g:126:1: entryRuleQN returns [String current=null] : iv_ruleQN= ruleQN EOF ;
    public final String entryRuleQN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQN = null;


        try {
            // InternalAyin.g:126:42: (iv_ruleQN= ruleQN EOF )
            // InternalAyin.g:127:2: iv_ruleQN= ruleQN EOF
            {
             newCompositeNode(grammarAccess.getQNRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQN=ruleQN();

            state._fsp--;

             current =iv_ruleQN.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQN"


    // $ANTLR start "ruleQN"
    // InternalAyin.g:133:1: ruleQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalAyin.g:139:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalAyin.g:140:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalAyin.g:140:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalAyin.g:141:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQNAccess().getIDTerminalRuleCall_0());
            		
            // InternalAyin.g:148:3: (kw= '.' this_ID_2= RULE_ID )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==15) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalAyin.g:149:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,15,FOLLOW_5); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQNAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_4); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQNAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQN"


    // $ANTLR start "entryRuleAyinParentable"
    // InternalAyin.g:166:1: entryRuleAyinParentable returns [EObject current=null] : iv_ruleAyinParentable= ruleAyinParentable EOF ;
    public final EObject entryRuleAyinParentable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinParentable = null;


        try {
            // InternalAyin.g:166:55: (iv_ruleAyinParentable= ruleAyinParentable EOF )
            // InternalAyin.g:167:2: iv_ruleAyinParentable= ruleAyinParentable EOF
            {
             newCompositeNode(grammarAccess.getAyinParentableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinParentable=ruleAyinParentable();

            state._fsp--;

             current =iv_ruleAyinParentable; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinParentable"


    // $ANTLR start "ruleAyinParentable"
    // InternalAyin.g:173:1: ruleAyinParentable returns [EObject current=null] : (this_AyinExecutor_0= ruleAyinExecutor | this_AyinStructLike_1= ruleAyinStructLike ) ;
    public final EObject ruleAyinParentable() throws RecognitionException {
        EObject current = null;

        EObject this_AyinExecutor_0 = null;

        EObject this_AyinStructLike_1 = null;



        	enterRule();

        try {
            // InternalAyin.g:179:2: ( (this_AyinExecutor_0= ruleAyinExecutor | this_AyinStructLike_1= ruleAyinStructLike ) )
            // InternalAyin.g:180:2: (this_AyinExecutor_0= ruleAyinExecutor | this_AyinStructLike_1= ruleAyinStructLike )
            {
            // InternalAyin.g:180:2: (this_AyinExecutor_0= ruleAyinExecutor | this_AyinStructLike_1= ruleAyinStructLike )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==23) ) {
                alt4=1;
            }
            else if ( (LA4_0==20||LA4_0==22) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalAyin.g:181:3: this_AyinExecutor_0= ruleAyinExecutor
                    {

                    			newCompositeNode(grammarAccess.getAyinParentableAccess().getAyinExecutorParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinExecutor_0=ruleAyinExecutor();

                    state._fsp--;


                    			current = this_AyinExecutor_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAyin.g:190:3: this_AyinStructLike_1= ruleAyinStructLike
                    {

                    			newCompositeNode(grammarAccess.getAyinParentableAccess().getAyinStructLikeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinStructLike_1=ruleAyinStructLike();

                    state._fsp--;


                    			current = this_AyinStructLike_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinParentable"


    // $ANTLR start "entryRuleAyinStructLike"
    // InternalAyin.g:202:1: entryRuleAyinStructLike returns [EObject current=null] : iv_ruleAyinStructLike= ruleAyinStructLike EOF ;
    public final EObject entryRuleAyinStructLike() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinStructLike = null;


        try {
            // InternalAyin.g:202:55: (iv_ruleAyinStructLike= ruleAyinStructLike EOF )
            // InternalAyin.g:203:2: iv_ruleAyinStructLike= ruleAyinStructLike EOF
            {
             newCompositeNode(grammarAccess.getAyinStructLikeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinStructLike=ruleAyinStructLike();

            state._fsp--;

             current =iv_ruleAyinStructLike; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinStructLike"


    // $ANTLR start "ruleAyinStructLike"
    // InternalAyin.g:209:1: ruleAyinStructLike returns [EObject current=null] : (this_AyinStruct_0= ruleAyinStruct | this_AyinException_1= ruleAyinException ) ;
    public final EObject ruleAyinStructLike() throws RecognitionException {
        EObject current = null;

        EObject this_AyinStruct_0 = null;

        EObject this_AyinException_1 = null;



        	enterRule();

        try {
            // InternalAyin.g:215:2: ( (this_AyinStruct_0= ruleAyinStruct | this_AyinException_1= ruleAyinException ) )
            // InternalAyin.g:216:2: (this_AyinStruct_0= ruleAyinStruct | this_AyinException_1= ruleAyinException )
            {
            // InternalAyin.g:216:2: (this_AyinStruct_0= ruleAyinStruct | this_AyinException_1= ruleAyinException )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==20) ) {
                alt5=1;
            }
            else if ( (LA5_0==22) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalAyin.g:217:3: this_AyinStruct_0= ruleAyinStruct
                    {

                    			newCompositeNode(grammarAccess.getAyinStructLikeAccess().getAyinStructParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinStruct_0=ruleAyinStruct();

                    state._fsp--;


                    			current = this_AyinStruct_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAyin.g:226:3: this_AyinException_1= ruleAyinException
                    {

                    			newCompositeNode(grammarAccess.getAyinStructLikeAccess().getAyinExceptionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinException_1=ruleAyinException();

                    state._fsp--;


                    			current = this_AyinException_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinStructLike"


    // $ANTLR start "entryRuleAyinEnum"
    // InternalAyin.g:238:1: entryRuleAyinEnum returns [EObject current=null] : iv_ruleAyinEnum= ruleAyinEnum EOF ;
    public final EObject entryRuleAyinEnum() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinEnum = null;


        try {
            // InternalAyin.g:238:49: (iv_ruleAyinEnum= ruleAyinEnum EOF )
            // InternalAyin.g:239:2: iv_ruleAyinEnum= ruleAyinEnum EOF
            {
             newCompositeNode(grammarAccess.getAyinEnumRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinEnum=ruleAyinEnum();

            state._fsp--;

             current =iv_ruleAyinEnum; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinEnum"


    // $ANTLR start "ruleAyinEnum"
    // InternalAyin.g:245:1: ruleAyinEnum returns [EObject current=null] : ( () otherlv_1= 'enum' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_elements_4_0= ruleAyinEnumElement ) ) otherlv_5= ';' )* otherlv_6= '}' ) ;
    public final EObject ruleAyinEnum() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        EObject lv_elements_4_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:251:2: ( ( () otherlv_1= 'enum' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_elements_4_0= ruleAyinEnumElement ) ) otherlv_5= ';' )* otherlv_6= '}' ) )
            // InternalAyin.g:252:2: ( () otherlv_1= 'enum' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_elements_4_0= ruleAyinEnumElement ) ) otherlv_5= ';' )* otherlv_6= '}' )
            {
            // InternalAyin.g:252:2: ( () otherlv_1= 'enum' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_elements_4_0= ruleAyinEnumElement ) ) otherlv_5= ';' )* otherlv_6= '}' )
            // InternalAyin.g:253:3: () otherlv_1= 'enum' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( ( (lv_elements_4_0= ruleAyinEnumElement ) ) otherlv_5= ';' )* otherlv_6= '}'
            {
            // InternalAyin.g:253:3: ()
            // InternalAyin.g:254:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAyinEnumAccess().getAyinEnumAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,16,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getAyinEnumAccess().getEnumKeyword_1());
            		
            // InternalAyin.g:264:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalAyin.g:265:4: (lv_name_2_0= RULE_ID )
            {
            // InternalAyin.g:265:4: (lv_name_2_0= RULE_ID )
            // InternalAyin.g:266:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_2_0, grammarAccess.getAyinEnumAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinEnumRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"space.ayin.lang.Ayin.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,17,FOLLOW_7); 

            			newLeafNode(otherlv_3, grammarAccess.getAyinEnumAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalAyin.g:286:3: ( ( (lv_elements_4_0= ruleAyinEnumElement ) ) otherlv_5= ';' )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==RULE_ID) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalAyin.g:287:4: ( (lv_elements_4_0= ruleAyinEnumElement ) ) otherlv_5= ';'
            	    {
            	    // InternalAyin.g:287:4: ( (lv_elements_4_0= ruleAyinEnumElement ) )
            	    // InternalAyin.g:288:5: (lv_elements_4_0= ruleAyinEnumElement )
            	    {
            	    // InternalAyin.g:288:5: (lv_elements_4_0= ruleAyinEnumElement )
            	    // InternalAyin.g:289:6: lv_elements_4_0= ruleAyinEnumElement
            	    {

            	    						newCompositeNode(grammarAccess.getAyinEnumAccess().getElementsAyinEnumElementParserRuleCall_4_0_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_elements_4_0=ruleAyinEnumElement();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAyinEnumRule());
            	    						}
            	    						add(
            	    							current,
            	    							"elements",
            	    							lv_elements_4_0,
            	    							"space.ayin.lang.Ayin.AyinEnumElement");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_5=(Token)match(input,14,FOLLOW_7); 

            	    				newLeafNode(otherlv_5, grammarAccess.getAyinEnumAccess().getSemicolonKeyword_4_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            otherlv_6=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getAyinEnumAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinEnum"


    // $ANTLR start "entryRuleAyinInterface"
    // InternalAyin.g:319:1: entryRuleAyinInterface returns [EObject current=null] : iv_ruleAyinInterface= ruleAyinInterface EOF ;
    public final EObject entryRuleAyinInterface() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinInterface = null;


        try {
            // InternalAyin.g:319:54: (iv_ruleAyinInterface= ruleAyinInterface EOF )
            // InternalAyin.g:320:2: iv_ruleAyinInterface= ruleAyinInterface EOF
            {
             newCompositeNode(grammarAccess.getAyinInterfaceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinInterface=ruleAyinInterface();

            state._fsp--;

             current =iv_ruleAyinInterface; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinInterface"


    // $ANTLR start "ruleAyinInterface"
    // InternalAyin.g:326:1: ruleAyinInterface returns [EObject current=null] : (otherlv_0= 'interface' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( ( (lv_events_3_0= ruleAyinEvent ) ) otherlv_4= ';' )* ( (lv_methods_5_0= ruleAyinMethod ) )* otherlv_6= '}' ) ;
    public final EObject ruleAyinInterface() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_events_3_0 = null;

        EObject lv_methods_5_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:332:2: ( (otherlv_0= 'interface' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( ( (lv_events_3_0= ruleAyinEvent ) ) otherlv_4= ';' )* ( (lv_methods_5_0= ruleAyinMethod ) )* otherlv_6= '}' ) )
            // InternalAyin.g:333:2: (otherlv_0= 'interface' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( ( (lv_events_3_0= ruleAyinEvent ) ) otherlv_4= ';' )* ( (lv_methods_5_0= ruleAyinMethod ) )* otherlv_6= '}' )
            {
            // InternalAyin.g:333:2: (otherlv_0= 'interface' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( ( (lv_events_3_0= ruleAyinEvent ) ) otherlv_4= ';' )* ( (lv_methods_5_0= ruleAyinMethod ) )* otherlv_6= '}' )
            // InternalAyin.g:334:3: otherlv_0= 'interface' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( ( (lv_events_3_0= ruleAyinEvent ) ) otherlv_4= ';' )* ( (lv_methods_5_0= ruleAyinMethod ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,19,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getAyinInterfaceAccess().getInterfaceKeyword_0());
            		
            // InternalAyin.g:338:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalAyin.g:339:4: (lv_name_1_0= RULE_ID )
            {
            // InternalAyin.g:339:4: (lv_name_1_0= RULE_ID )
            // InternalAyin.g:340:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAyinInterfaceAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinInterfaceRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"space.ayin.lang.Ayin.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_9); 

            			newLeafNode(otherlv_2, grammarAccess.getAyinInterfaceAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalAyin.g:360:3: ( ( (lv_events_3_0= ruleAyinEvent ) ) otherlv_4= ';' )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==29) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalAyin.g:361:4: ( (lv_events_3_0= ruleAyinEvent ) ) otherlv_4= ';'
            	    {
            	    // InternalAyin.g:361:4: ( (lv_events_3_0= ruleAyinEvent ) )
            	    // InternalAyin.g:362:5: (lv_events_3_0= ruleAyinEvent )
            	    {
            	    // InternalAyin.g:362:5: (lv_events_3_0= ruleAyinEvent )
            	    // InternalAyin.g:363:6: lv_events_3_0= ruleAyinEvent
            	    {

            	    						newCompositeNode(grammarAccess.getAyinInterfaceAccess().getEventsAyinEventParserRuleCall_3_0_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_events_3_0=ruleAyinEvent();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAyinInterfaceRule());
            	    						}
            	    						add(
            	    							current,
            	    							"events",
            	    							lv_events_3_0,
            	    							"space.ayin.lang.Ayin.AyinEvent");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_4=(Token)match(input,14,FOLLOW_9); 

            	    				newLeafNode(otherlv_4, grammarAccess.getAyinInterfaceAccess().getSemicolonKeyword_3_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            // InternalAyin.g:385:3: ( (lv_methods_5_0= ruleAyinMethod ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==RULE_ID||LA8_0==30||LA8_0==32||(LA8_0>=47 && LA8_0<=53)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalAyin.g:386:4: (lv_methods_5_0= ruleAyinMethod )
            	    {
            	    // InternalAyin.g:386:4: (lv_methods_5_0= ruleAyinMethod )
            	    // InternalAyin.g:387:5: lv_methods_5_0= ruleAyinMethod
            	    {

            	    					newCompositeNode(grammarAccess.getAyinInterfaceAccess().getMethodsAyinMethodParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_10);
            	    lv_methods_5_0=ruleAyinMethod();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAyinInterfaceRule());
            	    					}
            	    					add(
            	    						current,
            	    						"methods",
            	    						lv_methods_5_0,
            	    						"space.ayin.lang.Ayin.AyinMethod");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            otherlv_6=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getAyinInterfaceAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinInterface"


    // $ANTLR start "entryRuleAyinStruct"
    // InternalAyin.g:412:1: entryRuleAyinStruct returns [EObject current=null] : iv_ruleAyinStruct= ruleAyinStruct EOF ;
    public final EObject entryRuleAyinStruct() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinStruct = null;


        try {
            // InternalAyin.g:412:51: (iv_ruleAyinStruct= ruleAyinStruct EOF )
            // InternalAyin.g:413:2: iv_ruleAyinStruct= ruleAyinStruct EOF
            {
             newCompositeNode(grammarAccess.getAyinStructRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinStruct=ruleAyinStruct();

            state._fsp--;

             current =iv_ruleAyinStruct; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinStruct"


    // $ANTLR start "ruleAyinStruct"
    // InternalAyin.g:419:1: ruleAyinStruct returns [EObject current=null] : (otherlv_0= 'struct' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';' )* otherlv_7= '}' ) ;
    public final EObject ruleAyinStruct() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_fields_5_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:425:2: ( (otherlv_0= 'struct' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';' )* otherlv_7= '}' ) )
            // InternalAyin.g:426:2: (otherlv_0= 'struct' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';' )* otherlv_7= '}' )
            {
            // InternalAyin.g:426:2: (otherlv_0= 'struct' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';' )* otherlv_7= '}' )
            // InternalAyin.g:427:3: otherlv_0= 'struct' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';' )* otherlv_7= '}'
            {
            otherlv_0=(Token)match(input,20,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getAyinStructAccess().getStructKeyword_0());
            		
            // InternalAyin.g:431:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalAyin.g:432:4: (lv_name_1_0= RULE_ID )
            {
            // InternalAyin.g:432:4: (lv_name_1_0= RULE_ID )
            // InternalAyin.g:433:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_11); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAyinStructAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinStructRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"space.ayin.lang.Ayin.ID");
            				

            }


            }

            // InternalAyin.g:449:3: (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==21) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalAyin.g:450:4: otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,21,FOLLOW_5); 

                    				newLeafNode(otherlv_2, grammarAccess.getAyinStructAccess().getExtendsKeyword_2_0());
                    			
                    // InternalAyin.g:454:4: ( (otherlv_3= RULE_ID ) )
                    // InternalAyin.g:455:5: (otherlv_3= RULE_ID )
                    {
                    // InternalAyin.g:455:5: (otherlv_3= RULE_ID )
                    // InternalAyin.g:456:6: otherlv_3= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAyinStructRule());
                    						}
                    					
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_6); 

                    						newLeafNode(otherlv_3, grammarAccess.getAyinStructAccess().getParentAyinStructLikeCrossReference_2_1_0());
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,17,FOLLOW_12); 

            			newLeafNode(otherlv_4, grammarAccess.getAyinStructAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalAyin.g:472:3: ( ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';' )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==RULE_ID||(LA10_0>=47 && LA10_0<=53)) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalAyin.g:473:4: ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';'
            	    {
            	    // InternalAyin.g:473:4: ( (lv_fields_5_0= ruleAyinParameter ) )
            	    // InternalAyin.g:474:5: (lv_fields_5_0= ruleAyinParameter )
            	    {
            	    // InternalAyin.g:474:5: (lv_fields_5_0= ruleAyinParameter )
            	    // InternalAyin.g:475:6: lv_fields_5_0= ruleAyinParameter
            	    {

            	    						newCompositeNode(grammarAccess.getAyinStructAccess().getFieldsAyinParameterParserRuleCall_4_0_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_fields_5_0=ruleAyinParameter();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAyinStructRule());
            	    						}
            	    						add(
            	    							current,
            	    							"fields",
            	    							lv_fields_5_0,
            	    							"space.ayin.lang.Ayin.AyinParameter");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_6=(Token)match(input,14,FOLLOW_12); 

            	    				newLeafNode(otherlv_6, grammarAccess.getAyinStructAccess().getSemicolonKeyword_4_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            otherlv_7=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getAyinStructAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinStruct"


    // $ANTLR start "entryRuleAyinException"
    // InternalAyin.g:505:1: entryRuleAyinException returns [EObject current=null] : iv_ruleAyinException= ruleAyinException EOF ;
    public final EObject entryRuleAyinException() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinException = null;


        try {
            // InternalAyin.g:505:54: (iv_ruleAyinException= ruleAyinException EOF )
            // InternalAyin.g:506:2: iv_ruleAyinException= ruleAyinException EOF
            {
             newCompositeNode(grammarAccess.getAyinExceptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinException=ruleAyinException();

            state._fsp--;

             current =iv_ruleAyinException; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinException"


    // $ANTLR start "ruleAyinException"
    // InternalAyin.g:512:1: ruleAyinException returns [EObject current=null] : (otherlv_0= 'exception' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';' )* otherlv_7= '}' ) ;
    public final EObject ruleAyinException() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_fields_5_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:518:2: ( (otherlv_0= 'exception' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';' )* otherlv_7= '}' ) )
            // InternalAyin.g:519:2: (otherlv_0= 'exception' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';' )* otherlv_7= '}' )
            {
            // InternalAyin.g:519:2: (otherlv_0= 'exception' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';' )* otherlv_7= '}' )
            // InternalAyin.g:520:3: otherlv_0= 'exception' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? otherlv_4= '{' ( ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';' )* otherlv_7= '}'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getAyinExceptionAccess().getExceptionKeyword_0());
            		
            // InternalAyin.g:524:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalAyin.g:525:4: (lv_name_1_0= RULE_ID )
            {
            // InternalAyin.g:525:4: (lv_name_1_0= RULE_ID )
            // InternalAyin.g:526:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_11); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAyinExceptionAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinExceptionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"space.ayin.lang.Ayin.ID");
            				

            }


            }

            // InternalAyin.g:542:3: (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==21) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalAyin.g:543:4: otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,21,FOLLOW_5); 

                    				newLeafNode(otherlv_2, grammarAccess.getAyinExceptionAccess().getExtendsKeyword_2_0());
                    			
                    // InternalAyin.g:547:4: ( (otherlv_3= RULE_ID ) )
                    // InternalAyin.g:548:5: (otherlv_3= RULE_ID )
                    {
                    // InternalAyin.g:548:5: (otherlv_3= RULE_ID )
                    // InternalAyin.g:549:6: otherlv_3= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAyinExceptionRule());
                    						}
                    					
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_6); 

                    						newLeafNode(otherlv_3, grammarAccess.getAyinExceptionAccess().getParentAyinStructLikeCrossReference_2_1_0());
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,17,FOLLOW_12); 

            			newLeafNode(otherlv_4, grammarAccess.getAyinExceptionAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalAyin.g:565:3: ( ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';' )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==RULE_ID||(LA12_0>=47 && LA12_0<=53)) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalAyin.g:566:4: ( (lv_fields_5_0= ruleAyinParameter ) ) otherlv_6= ';'
            	    {
            	    // InternalAyin.g:566:4: ( (lv_fields_5_0= ruleAyinParameter ) )
            	    // InternalAyin.g:567:5: (lv_fields_5_0= ruleAyinParameter )
            	    {
            	    // InternalAyin.g:567:5: (lv_fields_5_0= ruleAyinParameter )
            	    // InternalAyin.g:568:6: lv_fields_5_0= ruleAyinParameter
            	    {

            	    						newCompositeNode(grammarAccess.getAyinExceptionAccess().getFieldsAyinParameterParserRuleCall_4_0_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_fields_5_0=ruleAyinParameter();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAyinExceptionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"fields",
            	    							lv_fields_5_0,
            	    							"space.ayin.lang.Ayin.AyinParameter");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_6=(Token)match(input,14,FOLLOW_12); 

            	    				newLeafNode(otherlv_6, grammarAccess.getAyinExceptionAccess().getSemicolonKeyword_4_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            otherlv_7=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getAyinExceptionAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinException"


    // $ANTLR start "entryRuleAyinExecutor"
    // InternalAyin.g:598:1: entryRuleAyinExecutor returns [EObject current=null] : iv_ruleAyinExecutor= ruleAyinExecutor EOF ;
    public final EObject entryRuleAyinExecutor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinExecutor = null;


        try {
            // InternalAyin.g:598:53: (iv_ruleAyinExecutor= ruleAyinExecutor EOF )
            // InternalAyin.g:599:2: iv_ruleAyinExecutor= ruleAyinExecutor EOF
            {
             newCompositeNode(grammarAccess.getAyinExecutorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinExecutor=ruleAyinExecutor();

            state._fsp--;

             current =iv_ruleAyinExecutor; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinExecutor"


    // $ANTLR start "ruleAyinExecutor"
    // InternalAyin.g:605:1: ruleAyinExecutor returns [EObject current=null] : (otherlv_0= 'executor' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? (otherlv_4= 'implements' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* )? otherlv_8= '{' ( (lv_handlers_9_0= ruleAyinEventHandler ) )* otherlv_10= '}' ) ;
    public final EObject ruleAyinExecutor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_handlers_9_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:611:2: ( (otherlv_0= 'executor' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? (otherlv_4= 'implements' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* )? otherlv_8= '{' ( (lv_handlers_9_0= ruleAyinEventHandler ) )* otherlv_10= '}' ) )
            // InternalAyin.g:612:2: (otherlv_0= 'executor' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? (otherlv_4= 'implements' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* )? otherlv_8= '{' ( (lv_handlers_9_0= ruleAyinEventHandler ) )* otherlv_10= '}' )
            {
            // InternalAyin.g:612:2: (otherlv_0= 'executor' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? (otherlv_4= 'implements' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* )? otherlv_8= '{' ( (lv_handlers_9_0= ruleAyinEventHandler ) )* otherlv_10= '}' )
            // InternalAyin.g:613:3: otherlv_0= 'executor' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )? (otherlv_4= 'implements' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* )? otherlv_8= '{' ( (lv_handlers_9_0= ruleAyinEventHandler ) )* otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,23,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getAyinExecutorAccess().getExecutorKeyword_0());
            		
            // InternalAyin.g:617:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalAyin.g:618:4: (lv_name_1_0= RULE_ID )
            {
            // InternalAyin.g:618:4: (lv_name_1_0= RULE_ID )
            // InternalAyin.g:619:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_13); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAyinExecutorAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinExecutorRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"space.ayin.lang.Ayin.ID");
            				

            }


            }

            // InternalAyin.g:635:3: (otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==21) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalAyin.g:636:4: otherlv_2= 'extends' ( (otherlv_3= RULE_ID ) )
                    {
                    otherlv_2=(Token)match(input,21,FOLLOW_5); 

                    				newLeafNode(otherlv_2, grammarAccess.getAyinExecutorAccess().getExtendsKeyword_2_0());
                    			
                    // InternalAyin.g:640:4: ( (otherlv_3= RULE_ID ) )
                    // InternalAyin.g:641:5: (otherlv_3= RULE_ID )
                    {
                    // InternalAyin.g:641:5: (otherlv_3= RULE_ID )
                    // InternalAyin.g:642:6: otherlv_3= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAyinExecutorRule());
                    						}
                    					
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_14); 

                    						newLeafNode(otherlv_3, grammarAccess.getAyinExecutorAccess().getParentAyinParentableCrossReference_2_1_0());
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalAyin.g:654:3: (otherlv_4= 'implements' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )* )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==24) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalAyin.g:655:4: otherlv_4= 'implements' ( (otherlv_5= RULE_ID ) ) (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )*
                    {
                    otherlv_4=(Token)match(input,24,FOLLOW_5); 

                    				newLeafNode(otherlv_4, grammarAccess.getAyinExecutorAccess().getImplementsKeyword_3_0());
                    			
                    // InternalAyin.g:659:4: ( (otherlv_5= RULE_ID ) )
                    // InternalAyin.g:660:5: (otherlv_5= RULE_ID )
                    {
                    // InternalAyin.g:660:5: (otherlv_5= RULE_ID )
                    // InternalAyin.g:661:6: otherlv_5= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAyinExecutorRule());
                    						}
                    					
                    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_15); 

                    						newLeafNode(otherlv_5, grammarAccess.getAyinExecutorAccess().getInterfacesAyinInterfaceCrossReference_3_1_0());
                    					

                    }


                    }

                    // InternalAyin.g:672:4: (otherlv_6= ',' ( (otherlv_7= RULE_ID ) ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==25) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // InternalAyin.g:673:5: otherlv_6= ',' ( (otherlv_7= RULE_ID ) )
                    	    {
                    	    otherlv_6=(Token)match(input,25,FOLLOW_5); 

                    	    					newLeafNode(otherlv_6, grammarAccess.getAyinExecutorAccess().getCommaKeyword_3_2_0());
                    	    				
                    	    // InternalAyin.g:677:5: ( (otherlv_7= RULE_ID ) )
                    	    // InternalAyin.g:678:6: (otherlv_7= RULE_ID )
                    	    {
                    	    // InternalAyin.g:678:6: (otherlv_7= RULE_ID )
                    	    // InternalAyin.g:679:7: otherlv_7= RULE_ID
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getAyinExecutorRule());
                    	    							}
                    	    						
                    	    otherlv_7=(Token)match(input,RULE_ID,FOLLOW_15); 

                    	    							newLeafNode(otherlv_7, grammarAccess.getAyinExecutorAccess().getInterfacesAyinInterfaceCrossReference_3_2_1_0());
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_8=(Token)match(input,17,FOLLOW_7); 

            			newLeafNode(otherlv_8, grammarAccess.getAyinExecutorAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalAyin.g:696:3: ( (lv_handlers_9_0= ruleAyinEventHandler ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_ID) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalAyin.g:697:4: (lv_handlers_9_0= ruleAyinEventHandler )
            	    {
            	    // InternalAyin.g:697:4: (lv_handlers_9_0= ruleAyinEventHandler )
            	    // InternalAyin.g:698:5: lv_handlers_9_0= ruleAyinEventHandler
            	    {

            	    					newCompositeNode(grammarAccess.getAyinExecutorAccess().getHandlersAyinEventHandlerParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_handlers_9_0=ruleAyinEventHandler();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAyinExecutorRule());
            	    					}
            	    					add(
            	    						current,
            	    						"handlers",
            	    						lv_handlers_9_0,
            	    						"space.ayin.lang.Ayin.AyinEventHandler");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            otherlv_10=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getAyinExecutorAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinExecutor"


    // $ANTLR start "entryRuleAyinData"
    // InternalAyin.g:723:1: entryRuleAyinData returns [EObject current=null] : iv_ruleAyinData= ruleAyinData EOF ;
    public final EObject entryRuleAyinData() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinData = null;


        try {
            // InternalAyin.g:723:49: (iv_ruleAyinData= ruleAyinData EOF )
            // InternalAyin.g:724:2: iv_ruleAyinData= ruleAyinData EOF
            {
             newCompositeNode(grammarAccess.getAyinDataRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinData=ruleAyinData();

            state._fsp--;

             current =iv_ruleAyinData; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinData"


    // $ANTLR start "ruleAyinData"
    // InternalAyin.g:730:1: ruleAyinData returns [EObject current=null] : (otherlv_0= 'datatype' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '<' ( ( (lv_arguments_3_0= RULE_ID ) ) (otherlv_4= ',' ( (lv_arguments_5_0= RULE_ID ) ) )* )? otherlv_6= '>' ) ;
    public final EObject ruleAyinData() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_arguments_3_0=null;
        Token otherlv_4=null;
        Token lv_arguments_5_0=null;
        Token otherlv_6=null;


        	enterRule();

        try {
            // InternalAyin.g:736:2: ( (otherlv_0= 'datatype' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '<' ( ( (lv_arguments_3_0= RULE_ID ) ) (otherlv_4= ',' ( (lv_arguments_5_0= RULE_ID ) ) )* )? otherlv_6= '>' ) )
            // InternalAyin.g:737:2: (otherlv_0= 'datatype' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '<' ( ( (lv_arguments_3_0= RULE_ID ) ) (otherlv_4= ',' ( (lv_arguments_5_0= RULE_ID ) ) )* )? otherlv_6= '>' )
            {
            // InternalAyin.g:737:2: (otherlv_0= 'datatype' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '<' ( ( (lv_arguments_3_0= RULE_ID ) ) (otherlv_4= ',' ( (lv_arguments_5_0= RULE_ID ) ) )* )? otherlv_6= '>' )
            // InternalAyin.g:738:3: otherlv_0= 'datatype' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '<' ( ( (lv_arguments_3_0= RULE_ID ) ) (otherlv_4= ',' ( (lv_arguments_5_0= RULE_ID ) ) )* )? otherlv_6= '>'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getAyinDataAccess().getDatatypeKeyword_0());
            		
            // InternalAyin.g:742:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalAyin.g:743:4: (lv_name_1_0= RULE_ID )
            {
            // InternalAyin.g:743:4: (lv_name_1_0= RULE_ID )
            // InternalAyin.g:744:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_16); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAyinDataAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinDataRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"space.ayin.lang.Ayin.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,27,FOLLOW_17); 

            			newLeafNode(otherlv_2, grammarAccess.getAyinDataAccess().getLessThanSignKeyword_2());
            		
            // InternalAyin.g:764:3: ( ( (lv_arguments_3_0= RULE_ID ) ) (otherlv_4= ',' ( (lv_arguments_5_0= RULE_ID ) ) )* )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==RULE_ID) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalAyin.g:765:4: ( (lv_arguments_3_0= RULE_ID ) ) (otherlv_4= ',' ( (lv_arguments_5_0= RULE_ID ) ) )*
                    {
                    // InternalAyin.g:765:4: ( (lv_arguments_3_0= RULE_ID ) )
                    // InternalAyin.g:766:5: (lv_arguments_3_0= RULE_ID )
                    {
                    // InternalAyin.g:766:5: (lv_arguments_3_0= RULE_ID )
                    // InternalAyin.g:767:6: lv_arguments_3_0= RULE_ID
                    {
                    lv_arguments_3_0=(Token)match(input,RULE_ID,FOLLOW_18); 

                    						newLeafNode(lv_arguments_3_0, grammarAccess.getAyinDataAccess().getArgumentsIDTerminalRuleCall_3_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAyinDataRule());
                    						}
                    						addWithLastConsumed(
                    							current,
                    							"arguments",
                    							lv_arguments_3_0,
                    							"space.ayin.lang.Ayin.ID");
                    					

                    }


                    }

                    // InternalAyin.g:783:4: (otherlv_4= ',' ( (lv_arguments_5_0= RULE_ID ) ) )*
                    loop17:
                    do {
                        int alt17=2;
                        int LA17_0 = input.LA(1);

                        if ( (LA17_0==25) ) {
                            alt17=1;
                        }


                        switch (alt17) {
                    	case 1 :
                    	    // InternalAyin.g:784:5: otherlv_4= ',' ( (lv_arguments_5_0= RULE_ID ) )
                    	    {
                    	    otherlv_4=(Token)match(input,25,FOLLOW_5); 

                    	    					newLeafNode(otherlv_4, grammarAccess.getAyinDataAccess().getCommaKeyword_3_1_0());
                    	    				
                    	    // InternalAyin.g:788:5: ( (lv_arguments_5_0= RULE_ID ) )
                    	    // InternalAyin.g:789:6: (lv_arguments_5_0= RULE_ID )
                    	    {
                    	    // InternalAyin.g:789:6: (lv_arguments_5_0= RULE_ID )
                    	    // InternalAyin.g:790:7: lv_arguments_5_0= RULE_ID
                    	    {
                    	    lv_arguments_5_0=(Token)match(input,RULE_ID,FOLLOW_18); 

                    	    							newLeafNode(lv_arguments_5_0, grammarAccess.getAyinDataAccess().getArgumentsIDTerminalRuleCall_3_1_1_0());
                    	    						

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getAyinDataRule());
                    	    							}
                    	    							addWithLastConsumed(
                    	    								current,
                    	    								"arguments",
                    	    								lv_arguments_5_0,
                    	    								"space.ayin.lang.Ayin.ID");
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop17;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,28,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getAyinDataAccess().getGreaterThanSignKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinData"


    // $ANTLR start "entryRuleAyinParameter"
    // InternalAyin.g:816:1: entryRuleAyinParameter returns [EObject current=null] : iv_ruleAyinParameter= ruleAyinParameter EOF ;
    public final EObject entryRuleAyinParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinParameter = null;


        try {
            // InternalAyin.g:816:54: (iv_ruleAyinParameter= ruleAyinParameter EOF )
            // InternalAyin.g:817:2: iv_ruleAyinParameter= ruleAyinParameter EOF
            {
             newCompositeNode(grammarAccess.getAyinParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinParameter=ruleAyinParameter();

            state._fsp--;

             current =iv_ruleAyinParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinParameter"


    // $ANTLR start "ruleAyinParameter"
    // InternalAyin.g:823:1: ruleAyinParameter returns [EObject current=null] : ( ( (lv_type_0_0= ruleAyinType ) ) ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleAyinParameter() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        EObject lv_type_0_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:829:2: ( ( ( (lv_type_0_0= ruleAyinType ) ) ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalAyin.g:830:2: ( ( (lv_type_0_0= ruleAyinType ) ) ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalAyin.g:830:2: ( ( (lv_type_0_0= ruleAyinType ) ) ( (lv_name_1_0= RULE_ID ) ) )
            // InternalAyin.g:831:3: ( (lv_type_0_0= ruleAyinType ) ) ( (lv_name_1_0= RULE_ID ) )
            {
            // InternalAyin.g:831:3: ( (lv_type_0_0= ruleAyinType ) )
            // InternalAyin.g:832:4: (lv_type_0_0= ruleAyinType )
            {
            // InternalAyin.g:832:4: (lv_type_0_0= ruleAyinType )
            // InternalAyin.g:833:5: lv_type_0_0= ruleAyinType
            {

            					newCompositeNode(grammarAccess.getAyinParameterAccess().getTypeAyinTypeParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_5);
            lv_type_0_0=ruleAyinType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAyinParameterRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_0_0,
            						"space.ayin.lang.Ayin.AyinType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAyin.g:850:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalAyin.g:851:4: (lv_name_1_0= RULE_ID )
            {
            // InternalAyin.g:851:4: (lv_name_1_0= RULE_ID )
            // InternalAyin.g:852:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAyinParameterAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinParameterRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"space.ayin.lang.Ayin.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinParameter"


    // $ANTLR start "entryRuleAyinEvent"
    // InternalAyin.g:872:1: entryRuleAyinEvent returns [EObject current=null] : iv_ruleAyinEvent= ruleAyinEvent EOF ;
    public final EObject entryRuleAyinEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinEvent = null;


        try {
            // InternalAyin.g:872:50: (iv_ruleAyinEvent= ruleAyinEvent EOF )
            // InternalAyin.g:873:2: iv_ruleAyinEvent= ruleAyinEvent EOF
            {
             newCompositeNode(grammarAccess.getAyinEventRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinEvent=ruleAyinEvent();

            state._fsp--;

             current =iv_ruleAyinEvent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinEvent"


    // $ANTLR start "ruleAyinEvent"
    // InternalAyin.g:879:1: ruleAyinEvent returns [EObject current=null] : (otherlv_0= 'event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_paramStruct_3_0= ruleAyinEventParams ) ) otherlv_4= ')' ) ;
    public final EObject ruleAyinEvent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_paramStruct_3_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:885:2: ( (otherlv_0= 'event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_paramStruct_3_0= ruleAyinEventParams ) ) otherlv_4= ')' ) )
            // InternalAyin.g:886:2: (otherlv_0= 'event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_paramStruct_3_0= ruleAyinEventParams ) ) otherlv_4= ')' )
            {
            // InternalAyin.g:886:2: (otherlv_0= 'event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_paramStruct_3_0= ruleAyinEventParams ) ) otherlv_4= ')' )
            // InternalAyin.g:887:3: otherlv_0= 'event' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_paramStruct_3_0= ruleAyinEventParams ) ) otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,29,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getAyinEventAccess().getEventKeyword_0());
            		
            // InternalAyin.g:891:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalAyin.g:892:4: (lv_name_1_0= RULE_ID )
            {
            // InternalAyin.g:892:4: (lv_name_1_0= RULE_ID )
            // InternalAyin.g:893:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_19); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAyinEventAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinEventRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"space.ayin.lang.Ayin.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,30,FOLLOW_20); 

            			newLeafNode(otherlv_2, grammarAccess.getAyinEventAccess().getLeftParenthesisKeyword_2());
            		
            // InternalAyin.g:913:3: ( (lv_paramStruct_3_0= ruleAyinEventParams ) )
            // InternalAyin.g:914:4: (lv_paramStruct_3_0= ruleAyinEventParams )
            {
            // InternalAyin.g:914:4: (lv_paramStruct_3_0= ruleAyinEventParams )
            // InternalAyin.g:915:5: lv_paramStruct_3_0= ruleAyinEventParams
            {

            					newCompositeNode(grammarAccess.getAyinEventAccess().getParamStructAyinEventParamsParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_21);
            lv_paramStruct_3_0=ruleAyinEventParams();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAyinEventRule());
            					}
            					set(
            						current,
            						"paramStruct",
            						lv_paramStruct_3_0,
            						"space.ayin.lang.Ayin.AyinEventParams");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,31,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getAyinEventAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinEvent"


    // $ANTLR start "entryRuleAyinMethod"
    // InternalAyin.g:940:1: entryRuleAyinMethod returns [EObject current=null] : iv_ruleAyinMethod= ruleAyinMethod EOF ;
    public final EObject entryRuleAyinMethod() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinMethod = null;


        try {
            // InternalAyin.g:940:51: (iv_ruleAyinMethod= ruleAyinMethod EOF )
            // InternalAyin.g:941:2: iv_ruleAyinMethod= ruleAyinMethod EOF
            {
             newCompositeNode(grammarAccess.getAyinMethodRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinMethod=ruleAyinMethod();

            state._fsp--;

             current =iv_ruleAyinMethod; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinMethod"


    // $ANTLR start "ruleAyinMethod"
    // InternalAyin.g:947:1: ruleAyinMethod returns [EObject current=null] : ( (otherlv_0= 'void' | ( (lv_retType_1_0= ruleAyinType ) ) | ( (lv_resultStruct_2_0= ruleAyinResultFields ) ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( ( (lv_paramStruct_5_0= ruleAyinFields ) ) (otherlv_6= ',' ( (lv_callbacks_7_0= ruleAyinCallback ) ) )* ) | ( ( (lv_callbacks_8_0= ruleAyinCallback ) ) (otherlv_9= ',' ( (lv_callbacks_10_0= ruleAyinCallback ) ) )* ) )? otherlv_11= ')' (otherlv_12= 'throws' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* )? (otherlv_16= ';' | ( (lv_body_17_0= ruleAyinBlock ) ) ) ) ;
    public final EObject ruleAyinMethod() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        EObject lv_retType_1_0 = null;

        EObject lv_resultStruct_2_0 = null;

        EObject lv_paramStruct_5_0 = null;

        EObject lv_callbacks_7_0 = null;

        EObject lv_callbacks_8_0 = null;

        EObject lv_callbacks_10_0 = null;

        EObject lv_body_17_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:953:2: ( ( (otherlv_0= 'void' | ( (lv_retType_1_0= ruleAyinType ) ) | ( (lv_resultStruct_2_0= ruleAyinResultFields ) ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( ( (lv_paramStruct_5_0= ruleAyinFields ) ) (otherlv_6= ',' ( (lv_callbacks_7_0= ruleAyinCallback ) ) )* ) | ( ( (lv_callbacks_8_0= ruleAyinCallback ) ) (otherlv_9= ',' ( (lv_callbacks_10_0= ruleAyinCallback ) ) )* ) )? otherlv_11= ')' (otherlv_12= 'throws' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* )? (otherlv_16= ';' | ( (lv_body_17_0= ruleAyinBlock ) ) ) ) )
            // InternalAyin.g:954:2: ( (otherlv_0= 'void' | ( (lv_retType_1_0= ruleAyinType ) ) | ( (lv_resultStruct_2_0= ruleAyinResultFields ) ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( ( (lv_paramStruct_5_0= ruleAyinFields ) ) (otherlv_6= ',' ( (lv_callbacks_7_0= ruleAyinCallback ) ) )* ) | ( ( (lv_callbacks_8_0= ruleAyinCallback ) ) (otherlv_9= ',' ( (lv_callbacks_10_0= ruleAyinCallback ) ) )* ) )? otherlv_11= ')' (otherlv_12= 'throws' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* )? (otherlv_16= ';' | ( (lv_body_17_0= ruleAyinBlock ) ) ) )
            {
            // InternalAyin.g:954:2: ( (otherlv_0= 'void' | ( (lv_retType_1_0= ruleAyinType ) ) | ( (lv_resultStruct_2_0= ruleAyinResultFields ) ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( ( (lv_paramStruct_5_0= ruleAyinFields ) ) (otherlv_6= ',' ( (lv_callbacks_7_0= ruleAyinCallback ) ) )* ) | ( ( (lv_callbacks_8_0= ruleAyinCallback ) ) (otherlv_9= ',' ( (lv_callbacks_10_0= ruleAyinCallback ) ) )* ) )? otherlv_11= ')' (otherlv_12= 'throws' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* )? (otherlv_16= ';' | ( (lv_body_17_0= ruleAyinBlock ) ) ) )
            // InternalAyin.g:955:3: (otherlv_0= 'void' | ( (lv_retType_1_0= ruleAyinType ) ) | ( (lv_resultStruct_2_0= ruleAyinResultFields ) ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= '(' ( ( ( (lv_paramStruct_5_0= ruleAyinFields ) ) (otherlv_6= ',' ( (lv_callbacks_7_0= ruleAyinCallback ) ) )* ) | ( ( (lv_callbacks_8_0= ruleAyinCallback ) ) (otherlv_9= ',' ( (lv_callbacks_10_0= ruleAyinCallback ) ) )* ) )? otherlv_11= ')' (otherlv_12= 'throws' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* )? (otherlv_16= ';' | ( (lv_body_17_0= ruleAyinBlock ) ) )
            {
            // InternalAyin.g:955:3: (otherlv_0= 'void' | ( (lv_retType_1_0= ruleAyinType ) ) | ( (lv_resultStruct_2_0= ruleAyinResultFields ) ) )
            int alt19=3;
            switch ( input.LA(1) ) {
            case 32:
                {
                alt19=1;
                }
                break;
            case RULE_ID:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
                {
                alt19=2;
                }
                break;
            case 30:
                {
                alt19=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }

            switch (alt19) {
                case 1 :
                    // InternalAyin.g:956:4: otherlv_0= 'void'
                    {
                    otherlv_0=(Token)match(input,32,FOLLOW_5); 

                    				newLeafNode(otherlv_0, grammarAccess.getAyinMethodAccess().getVoidKeyword_0_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalAyin.g:961:4: ( (lv_retType_1_0= ruleAyinType ) )
                    {
                    // InternalAyin.g:961:4: ( (lv_retType_1_0= ruleAyinType ) )
                    // InternalAyin.g:962:5: (lv_retType_1_0= ruleAyinType )
                    {
                    // InternalAyin.g:962:5: (lv_retType_1_0= ruleAyinType )
                    // InternalAyin.g:963:6: lv_retType_1_0= ruleAyinType
                    {

                    						newCompositeNode(grammarAccess.getAyinMethodAccess().getRetTypeAyinTypeParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_retType_1_0=ruleAyinType();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinMethodRule());
                    						}
                    						set(
                    							current,
                    							"retType",
                    							lv_retType_1_0,
                    							"space.ayin.lang.Ayin.AyinType");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalAyin.g:981:4: ( (lv_resultStruct_2_0= ruleAyinResultFields ) )
                    {
                    // InternalAyin.g:981:4: ( (lv_resultStruct_2_0= ruleAyinResultFields ) )
                    // InternalAyin.g:982:5: (lv_resultStruct_2_0= ruleAyinResultFields )
                    {
                    // InternalAyin.g:982:5: (lv_resultStruct_2_0= ruleAyinResultFields )
                    // InternalAyin.g:983:6: lv_resultStruct_2_0= ruleAyinResultFields
                    {

                    						newCompositeNode(grammarAccess.getAyinMethodAccess().getResultStructAyinResultFieldsParserRuleCall_0_2_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_resultStruct_2_0=ruleAyinResultFields();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinMethodRule());
                    						}
                    						set(
                    							current,
                    							"resultStruct",
                    							lv_resultStruct_2_0,
                    							"space.ayin.lang.Ayin.AyinResultFields");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalAyin.g:1001:3: ( (lv_name_3_0= RULE_ID ) )
            // InternalAyin.g:1002:4: (lv_name_3_0= RULE_ID )
            {
            // InternalAyin.g:1002:4: (lv_name_3_0= RULE_ID )
            // InternalAyin.g:1003:5: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FOLLOW_19); 

            					newLeafNode(lv_name_3_0, grammarAccess.getAyinMethodAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinMethodRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_3_0,
            						"space.ayin.lang.Ayin.ID");
            				

            }


            }

            otherlv_4=(Token)match(input,30,FOLLOW_22); 

            			newLeafNode(otherlv_4, grammarAccess.getAyinMethodAccess().getLeftParenthesisKeyword_2());
            		
            // InternalAyin.g:1023:3: ( ( ( (lv_paramStruct_5_0= ruleAyinFields ) ) (otherlv_6= ',' ( (lv_callbacks_7_0= ruleAyinCallback ) ) )* ) | ( ( (lv_callbacks_8_0= ruleAyinCallback ) ) (otherlv_9= ',' ( (lv_callbacks_10_0= ruleAyinCallback ) ) )* ) )?
            int alt22=3;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==RULE_ID||(LA22_0>=47 && LA22_0<=53)) ) {
                alt22=1;
            }
            else if ( (LA22_0==34) ) {
                alt22=2;
            }
            switch (alt22) {
                case 1 :
                    // InternalAyin.g:1024:4: ( ( (lv_paramStruct_5_0= ruleAyinFields ) ) (otherlv_6= ',' ( (lv_callbacks_7_0= ruleAyinCallback ) ) )* )
                    {
                    // InternalAyin.g:1024:4: ( ( (lv_paramStruct_5_0= ruleAyinFields ) ) (otherlv_6= ',' ( (lv_callbacks_7_0= ruleAyinCallback ) ) )* )
                    // InternalAyin.g:1025:5: ( (lv_paramStruct_5_0= ruleAyinFields ) ) (otherlv_6= ',' ( (lv_callbacks_7_0= ruleAyinCallback ) ) )*
                    {
                    // InternalAyin.g:1025:5: ( (lv_paramStruct_5_0= ruleAyinFields ) )
                    // InternalAyin.g:1026:6: (lv_paramStruct_5_0= ruleAyinFields )
                    {
                    // InternalAyin.g:1026:6: (lv_paramStruct_5_0= ruleAyinFields )
                    // InternalAyin.g:1027:7: lv_paramStruct_5_0= ruleAyinFields
                    {

                    							newCompositeNode(grammarAccess.getAyinMethodAccess().getParamStructAyinFieldsParserRuleCall_3_0_0_0());
                    						
                    pushFollow(FOLLOW_23);
                    lv_paramStruct_5_0=ruleAyinFields();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getAyinMethodRule());
                    							}
                    							set(
                    								current,
                    								"paramStruct",
                    								lv_paramStruct_5_0,
                    								"space.ayin.lang.Ayin.AyinFields");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalAyin.g:1044:5: (otherlv_6= ',' ( (lv_callbacks_7_0= ruleAyinCallback ) ) )*
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( (LA20_0==25) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // InternalAyin.g:1045:6: otherlv_6= ',' ( (lv_callbacks_7_0= ruleAyinCallback ) )
                    	    {
                    	    otherlv_6=(Token)match(input,25,FOLLOW_24); 

                    	    						newLeafNode(otherlv_6, grammarAccess.getAyinMethodAccess().getCommaKeyword_3_0_1_0());
                    	    					
                    	    // InternalAyin.g:1049:6: ( (lv_callbacks_7_0= ruleAyinCallback ) )
                    	    // InternalAyin.g:1050:7: (lv_callbacks_7_0= ruleAyinCallback )
                    	    {
                    	    // InternalAyin.g:1050:7: (lv_callbacks_7_0= ruleAyinCallback )
                    	    // InternalAyin.g:1051:8: lv_callbacks_7_0= ruleAyinCallback
                    	    {

                    	    								newCompositeNode(grammarAccess.getAyinMethodAccess().getCallbacksAyinCallbackParserRuleCall_3_0_1_1_0());
                    	    							
                    	    pushFollow(FOLLOW_23);
                    	    lv_callbacks_7_0=ruleAyinCallback();

                    	    state._fsp--;


                    	    								if (current==null) {
                    	    									current = createModelElementForParent(grammarAccess.getAyinMethodRule());
                    	    								}
                    	    								add(
                    	    									current,
                    	    									"callbacks",
                    	    									lv_callbacks_7_0,
                    	    									"space.ayin.lang.Ayin.AyinCallback");
                    	    								afterParserOrEnumRuleCall();
                    	    							

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop20;
                        }
                    } while (true);


                    }


                    }
                    break;
                case 2 :
                    // InternalAyin.g:1071:4: ( ( (lv_callbacks_8_0= ruleAyinCallback ) ) (otherlv_9= ',' ( (lv_callbacks_10_0= ruleAyinCallback ) ) )* )
                    {
                    // InternalAyin.g:1071:4: ( ( (lv_callbacks_8_0= ruleAyinCallback ) ) (otherlv_9= ',' ( (lv_callbacks_10_0= ruleAyinCallback ) ) )* )
                    // InternalAyin.g:1072:5: ( (lv_callbacks_8_0= ruleAyinCallback ) ) (otherlv_9= ',' ( (lv_callbacks_10_0= ruleAyinCallback ) ) )*
                    {
                    // InternalAyin.g:1072:5: ( (lv_callbacks_8_0= ruleAyinCallback ) )
                    // InternalAyin.g:1073:6: (lv_callbacks_8_0= ruleAyinCallback )
                    {
                    // InternalAyin.g:1073:6: (lv_callbacks_8_0= ruleAyinCallback )
                    // InternalAyin.g:1074:7: lv_callbacks_8_0= ruleAyinCallback
                    {

                    							newCompositeNode(grammarAccess.getAyinMethodAccess().getCallbacksAyinCallbackParserRuleCall_3_1_0_0());
                    						
                    pushFollow(FOLLOW_23);
                    lv_callbacks_8_0=ruleAyinCallback();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getAyinMethodRule());
                    							}
                    							add(
                    								current,
                    								"callbacks",
                    								lv_callbacks_8_0,
                    								"space.ayin.lang.Ayin.AyinCallback");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    // InternalAyin.g:1091:5: (otherlv_9= ',' ( (lv_callbacks_10_0= ruleAyinCallback ) ) )*
                    loop21:
                    do {
                        int alt21=2;
                        int LA21_0 = input.LA(1);

                        if ( (LA21_0==25) ) {
                            alt21=1;
                        }


                        switch (alt21) {
                    	case 1 :
                    	    // InternalAyin.g:1092:6: otherlv_9= ',' ( (lv_callbacks_10_0= ruleAyinCallback ) )
                    	    {
                    	    otherlv_9=(Token)match(input,25,FOLLOW_24); 

                    	    						newLeafNode(otherlv_9, grammarAccess.getAyinMethodAccess().getCommaKeyword_3_1_1_0());
                    	    					
                    	    // InternalAyin.g:1096:6: ( (lv_callbacks_10_0= ruleAyinCallback ) )
                    	    // InternalAyin.g:1097:7: (lv_callbacks_10_0= ruleAyinCallback )
                    	    {
                    	    // InternalAyin.g:1097:7: (lv_callbacks_10_0= ruleAyinCallback )
                    	    // InternalAyin.g:1098:8: lv_callbacks_10_0= ruleAyinCallback
                    	    {

                    	    								newCompositeNode(grammarAccess.getAyinMethodAccess().getCallbacksAyinCallbackParserRuleCall_3_1_1_1_0());
                    	    							
                    	    pushFollow(FOLLOW_23);
                    	    lv_callbacks_10_0=ruleAyinCallback();

                    	    state._fsp--;


                    	    								if (current==null) {
                    	    									current = createModelElementForParent(grammarAccess.getAyinMethodRule());
                    	    								}
                    	    								add(
                    	    									current,
                    	    									"callbacks",
                    	    									lv_callbacks_10_0,
                    	    									"space.ayin.lang.Ayin.AyinCallback");
                    	    								afterParserOrEnumRuleCall();
                    	    							

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop21;
                        }
                    } while (true);


                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,31,FOLLOW_25); 

            			newLeafNode(otherlv_11, grammarAccess.getAyinMethodAccess().getRightParenthesisKeyword_4());
            		
            // InternalAyin.g:1122:3: (otherlv_12= 'throws' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==33) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalAyin.g:1123:4: otherlv_12= 'throws' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )*
                    {
                    otherlv_12=(Token)match(input,33,FOLLOW_5); 

                    				newLeafNode(otherlv_12, grammarAccess.getAyinMethodAccess().getThrowsKeyword_5_0());
                    			
                    // InternalAyin.g:1127:4: ( (otherlv_13= RULE_ID ) )
                    // InternalAyin.g:1128:5: (otherlv_13= RULE_ID )
                    {
                    // InternalAyin.g:1128:5: (otherlv_13= RULE_ID )
                    // InternalAyin.g:1129:6: otherlv_13= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAyinMethodRule());
                    						}
                    					
                    otherlv_13=(Token)match(input,RULE_ID,FOLLOW_26); 

                    						newLeafNode(otherlv_13, grammarAccess.getAyinMethodAccess().getExceptionsAyinExceptionCrossReference_5_1_0());
                    					

                    }


                    }

                    // InternalAyin.g:1140:4: (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )*
                    loop23:
                    do {
                        int alt23=2;
                        int LA23_0 = input.LA(1);

                        if ( (LA23_0==25) ) {
                            alt23=1;
                        }


                        switch (alt23) {
                    	case 1 :
                    	    // InternalAyin.g:1141:5: otherlv_14= ',' ( (otherlv_15= RULE_ID ) )
                    	    {
                    	    otherlv_14=(Token)match(input,25,FOLLOW_5); 

                    	    					newLeafNode(otherlv_14, grammarAccess.getAyinMethodAccess().getCommaKeyword_5_2_0());
                    	    				
                    	    // InternalAyin.g:1145:5: ( (otherlv_15= RULE_ID ) )
                    	    // InternalAyin.g:1146:6: (otherlv_15= RULE_ID )
                    	    {
                    	    // InternalAyin.g:1146:6: (otherlv_15= RULE_ID )
                    	    // InternalAyin.g:1147:7: otherlv_15= RULE_ID
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getAyinMethodRule());
                    	    							}
                    	    						
                    	    otherlv_15=(Token)match(input,RULE_ID,FOLLOW_26); 

                    	    							newLeafNode(otherlv_15, grammarAccess.getAyinMethodAccess().getExceptionsAyinExceptionCrossReference_5_2_1_0());
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop23;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalAyin.g:1160:3: (otherlv_16= ';' | ( (lv_body_17_0= ruleAyinBlock ) ) )
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==14) ) {
                alt25=1;
            }
            else if ( (LA25_0==17) ) {
                alt25=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }
            switch (alt25) {
                case 1 :
                    // InternalAyin.g:1161:4: otherlv_16= ';'
                    {
                    otherlv_16=(Token)match(input,14,FOLLOW_2); 

                    				newLeafNode(otherlv_16, grammarAccess.getAyinMethodAccess().getSemicolonKeyword_6_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalAyin.g:1166:4: ( (lv_body_17_0= ruleAyinBlock ) )
                    {
                    // InternalAyin.g:1166:4: ( (lv_body_17_0= ruleAyinBlock ) )
                    // InternalAyin.g:1167:5: (lv_body_17_0= ruleAyinBlock )
                    {
                    // InternalAyin.g:1167:5: (lv_body_17_0= ruleAyinBlock )
                    // InternalAyin.g:1168:6: lv_body_17_0= ruleAyinBlock
                    {

                    						newCompositeNode(grammarAccess.getAyinMethodAccess().getBodyAyinBlockParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_body_17_0=ruleAyinBlock();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinMethodRule());
                    						}
                    						set(
                    							current,
                    							"body",
                    							lv_body_17_0,
                    							"space.ayin.lang.Ayin.AyinBlock");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinMethod"


    // $ANTLR start "entryRuleAyinCallback"
    // InternalAyin.g:1190:1: entryRuleAyinCallback returns [EObject current=null] : iv_ruleAyinCallback= ruleAyinCallback EOF ;
    public final EObject entryRuleAyinCallback() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinCallback = null;


        try {
            // InternalAyin.g:1190:53: (iv_ruleAyinCallback= ruleAyinCallback EOF )
            // InternalAyin.g:1191:2: iv_ruleAyinCallback= ruleAyinCallback EOF
            {
             newCompositeNode(grammarAccess.getAyinCallbackRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinCallback=ruleAyinCallback();

            state._fsp--;

             current =iv_ruleAyinCallback; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinCallback"


    // $ANTLR start "ruleAyinCallback"
    // InternalAyin.g:1197:1: ruleAyinCallback returns [EObject current=null] : ( () otherlv_1= 'callback' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_arguments_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_arguments_6_0= ruleAyinParameter ) ) )* )? ( ( (lv_callbacks_7_0= ruleAyinCallback ) ) (otherlv_8= ',' ( (lv_callbacks_9_0= ruleAyinCallback ) ) )* )? otherlv_10= ')' ) ;
    public final EObject ruleAyinCallback() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_arguments_4_0 = null;

        EObject lv_arguments_6_0 = null;

        EObject lv_callbacks_7_0 = null;

        EObject lv_callbacks_9_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:1203:2: ( ( () otherlv_1= 'callback' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_arguments_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_arguments_6_0= ruleAyinParameter ) ) )* )? ( ( (lv_callbacks_7_0= ruleAyinCallback ) ) (otherlv_8= ',' ( (lv_callbacks_9_0= ruleAyinCallback ) ) )* )? otherlv_10= ')' ) )
            // InternalAyin.g:1204:2: ( () otherlv_1= 'callback' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_arguments_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_arguments_6_0= ruleAyinParameter ) ) )* )? ( ( (lv_callbacks_7_0= ruleAyinCallback ) ) (otherlv_8= ',' ( (lv_callbacks_9_0= ruleAyinCallback ) ) )* )? otherlv_10= ')' )
            {
            // InternalAyin.g:1204:2: ( () otherlv_1= 'callback' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_arguments_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_arguments_6_0= ruleAyinParameter ) ) )* )? ( ( (lv_callbacks_7_0= ruleAyinCallback ) ) (otherlv_8= ',' ( (lv_callbacks_9_0= ruleAyinCallback ) ) )* )? otherlv_10= ')' )
            // InternalAyin.g:1205:3: () otherlv_1= 'callback' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '(' ( ( (lv_arguments_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_arguments_6_0= ruleAyinParameter ) ) )* )? ( ( (lv_callbacks_7_0= ruleAyinCallback ) ) (otherlv_8= ',' ( (lv_callbacks_9_0= ruleAyinCallback ) ) )* )? otherlv_10= ')'
            {
            // InternalAyin.g:1205:3: ()
            // InternalAyin.g:1206:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAyinCallbackAccess().getAyinCallbackAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,34,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getAyinCallbackAccess().getCallbackKeyword_1());
            		
            // InternalAyin.g:1216:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalAyin.g:1217:4: (lv_name_2_0= RULE_ID )
            {
            // InternalAyin.g:1217:4: (lv_name_2_0= RULE_ID )
            // InternalAyin.g:1218:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_19); 

            					newLeafNode(lv_name_2_0, grammarAccess.getAyinCallbackAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinCallbackRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"space.ayin.lang.Ayin.ID");
            				

            }


            }

            otherlv_3=(Token)match(input,30,FOLLOW_22); 

            			newLeafNode(otherlv_3, grammarAccess.getAyinCallbackAccess().getLeftParenthesisKeyword_3());
            		
            // InternalAyin.g:1238:3: ( ( (lv_arguments_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_arguments_6_0= ruleAyinParameter ) ) )* )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==RULE_ID||(LA27_0>=47 && LA27_0<=53)) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalAyin.g:1239:4: ( (lv_arguments_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_arguments_6_0= ruleAyinParameter ) ) )*
                    {
                    // InternalAyin.g:1239:4: ( (lv_arguments_4_0= ruleAyinParameter ) )
                    // InternalAyin.g:1240:5: (lv_arguments_4_0= ruleAyinParameter )
                    {
                    // InternalAyin.g:1240:5: (lv_arguments_4_0= ruleAyinParameter )
                    // InternalAyin.g:1241:6: lv_arguments_4_0= ruleAyinParameter
                    {

                    						newCompositeNode(grammarAccess.getAyinCallbackAccess().getArgumentsAyinParameterParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_27);
                    lv_arguments_4_0=ruleAyinParameter();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinCallbackRule());
                    						}
                    						add(
                    							current,
                    							"arguments",
                    							lv_arguments_4_0,
                    							"space.ayin.lang.Ayin.AyinParameter");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalAyin.g:1258:4: (otherlv_5= ',' ( (lv_arguments_6_0= ruleAyinParameter ) ) )*
                    loop26:
                    do {
                        int alt26=2;
                        int LA26_0 = input.LA(1);

                        if ( (LA26_0==25) ) {
                            alt26=1;
                        }


                        switch (alt26) {
                    	case 1 :
                    	    // InternalAyin.g:1259:5: otherlv_5= ',' ( (lv_arguments_6_0= ruleAyinParameter ) )
                    	    {
                    	    otherlv_5=(Token)match(input,25,FOLLOW_28); 

                    	    					newLeafNode(otherlv_5, grammarAccess.getAyinCallbackAccess().getCommaKeyword_4_1_0());
                    	    				
                    	    // InternalAyin.g:1263:5: ( (lv_arguments_6_0= ruleAyinParameter ) )
                    	    // InternalAyin.g:1264:6: (lv_arguments_6_0= ruleAyinParameter )
                    	    {
                    	    // InternalAyin.g:1264:6: (lv_arguments_6_0= ruleAyinParameter )
                    	    // InternalAyin.g:1265:7: lv_arguments_6_0= ruleAyinParameter
                    	    {

                    	    							newCompositeNode(grammarAccess.getAyinCallbackAccess().getArgumentsAyinParameterParserRuleCall_4_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_27);
                    	    lv_arguments_6_0=ruleAyinParameter();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getAyinCallbackRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"arguments",
                    	    								lv_arguments_6_0,
                    	    								"space.ayin.lang.Ayin.AyinParameter");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop26;
                        }
                    } while (true);


                    }
                    break;

            }

            // InternalAyin.g:1284:3: ( ( (lv_callbacks_7_0= ruleAyinCallback ) ) (otherlv_8= ',' ( (lv_callbacks_9_0= ruleAyinCallback ) ) )* )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==34) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalAyin.g:1285:4: ( (lv_callbacks_7_0= ruleAyinCallback ) ) (otherlv_8= ',' ( (lv_callbacks_9_0= ruleAyinCallback ) ) )*
                    {
                    // InternalAyin.g:1285:4: ( (lv_callbacks_7_0= ruleAyinCallback ) )
                    // InternalAyin.g:1286:5: (lv_callbacks_7_0= ruleAyinCallback )
                    {
                    // InternalAyin.g:1286:5: (lv_callbacks_7_0= ruleAyinCallback )
                    // InternalAyin.g:1287:6: lv_callbacks_7_0= ruleAyinCallback
                    {

                    						newCompositeNode(grammarAccess.getAyinCallbackAccess().getCallbacksAyinCallbackParserRuleCall_5_0_0());
                    					
                    pushFollow(FOLLOW_23);
                    lv_callbacks_7_0=ruleAyinCallback();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinCallbackRule());
                    						}
                    						add(
                    							current,
                    							"callbacks",
                    							lv_callbacks_7_0,
                    							"space.ayin.lang.Ayin.AyinCallback");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalAyin.g:1304:4: (otherlv_8= ',' ( (lv_callbacks_9_0= ruleAyinCallback ) ) )*
                    loop28:
                    do {
                        int alt28=2;
                        int LA28_0 = input.LA(1);

                        if ( (LA28_0==25) ) {
                            alt28=1;
                        }


                        switch (alt28) {
                    	case 1 :
                    	    // InternalAyin.g:1305:5: otherlv_8= ',' ( (lv_callbacks_9_0= ruleAyinCallback ) )
                    	    {
                    	    otherlv_8=(Token)match(input,25,FOLLOW_24); 

                    	    					newLeafNode(otherlv_8, grammarAccess.getAyinCallbackAccess().getCommaKeyword_5_1_0());
                    	    				
                    	    // InternalAyin.g:1309:5: ( (lv_callbacks_9_0= ruleAyinCallback ) )
                    	    // InternalAyin.g:1310:6: (lv_callbacks_9_0= ruleAyinCallback )
                    	    {
                    	    // InternalAyin.g:1310:6: (lv_callbacks_9_0= ruleAyinCallback )
                    	    // InternalAyin.g:1311:7: lv_callbacks_9_0= ruleAyinCallback
                    	    {

                    	    							newCompositeNode(grammarAccess.getAyinCallbackAccess().getCallbacksAyinCallbackParserRuleCall_5_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_23);
                    	    lv_callbacks_9_0=ruleAyinCallback();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getAyinCallbackRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"callbacks",
                    	    								lv_callbacks_9_0,
                    	    								"space.ayin.lang.Ayin.AyinCallback");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop28;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_10=(Token)match(input,31,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getAyinCallbackAccess().getRightParenthesisKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinCallback"


    // $ANTLR start "entryRuleAyinEventHandler"
    // InternalAyin.g:1338:1: entryRuleAyinEventHandler returns [EObject current=null] : iv_ruleAyinEventHandler= ruleAyinEventHandler EOF ;
    public final EObject entryRuleAyinEventHandler() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinEventHandler = null;


        try {
            // InternalAyin.g:1338:57: (iv_ruleAyinEventHandler= ruleAyinEventHandler EOF )
            // InternalAyin.g:1339:2: iv_ruleAyinEventHandler= ruleAyinEventHandler EOF
            {
             newCompositeNode(grammarAccess.getAyinEventHandlerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinEventHandler=ruleAyinEventHandler();

            state._fsp--;

             current =iv_ruleAyinEventHandler; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinEventHandler"


    // $ANTLR start "ruleAyinEventHandler"
    // InternalAyin.g:1345:1: ruleAyinEventHandler returns [EObject current=null] : ( ( ( ruleQN ) ) otherlv_1= '=' ( (lv_block_2_0= ruleAyinBlock ) ) ) ;
    public final EObject ruleAyinEventHandler() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_block_2_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:1351:2: ( ( ( ( ruleQN ) ) otherlv_1= '=' ( (lv_block_2_0= ruleAyinBlock ) ) ) )
            // InternalAyin.g:1352:2: ( ( ( ruleQN ) ) otherlv_1= '=' ( (lv_block_2_0= ruleAyinBlock ) ) )
            {
            // InternalAyin.g:1352:2: ( ( ( ruleQN ) ) otherlv_1= '=' ( (lv_block_2_0= ruleAyinBlock ) ) )
            // InternalAyin.g:1353:3: ( ( ruleQN ) ) otherlv_1= '=' ( (lv_block_2_0= ruleAyinBlock ) )
            {
            // InternalAyin.g:1353:3: ( ( ruleQN ) )
            // InternalAyin.g:1354:4: ( ruleQN )
            {
            // InternalAyin.g:1354:4: ( ruleQN )
            // InternalAyin.g:1355:5: ruleQN
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinEventHandlerRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAyinEventHandlerAccess().getEventAyinEventCrossReference_0_0());
            				
            pushFollow(FOLLOW_29);
            ruleQN();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,35,FOLLOW_25); 

            			newLeafNode(otherlv_1, grammarAccess.getAyinEventHandlerAccess().getEqualsSignKeyword_1());
            		
            // InternalAyin.g:1373:3: ( (lv_block_2_0= ruleAyinBlock ) )
            // InternalAyin.g:1374:4: (lv_block_2_0= ruleAyinBlock )
            {
            // InternalAyin.g:1374:4: (lv_block_2_0= ruleAyinBlock )
            // InternalAyin.g:1375:5: lv_block_2_0= ruleAyinBlock
            {

            					newCompositeNode(grammarAccess.getAyinEventHandlerAccess().getBlockAyinBlockParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_block_2_0=ruleAyinBlock();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAyinEventHandlerRule());
            					}
            					set(
            						current,
            						"block",
            						lv_block_2_0,
            						"space.ayin.lang.Ayin.AyinBlock");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinEventHandler"


    // $ANTLR start "entryRuleAyinBlock"
    // InternalAyin.g:1396:1: entryRuleAyinBlock returns [EObject current=null] : iv_ruleAyinBlock= ruleAyinBlock EOF ;
    public final EObject entryRuleAyinBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinBlock = null;


        try {
            // InternalAyin.g:1396:50: (iv_ruleAyinBlock= ruleAyinBlock EOF )
            // InternalAyin.g:1397:2: iv_ruleAyinBlock= ruleAyinBlock EOF
            {
             newCompositeNode(grammarAccess.getAyinBlockRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinBlock=ruleAyinBlock();

            state._fsp--;

             current =iv_ruleAyinBlock; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinBlock"


    // $ANTLR start "ruleAyinBlock"
    // InternalAyin.g:1403:1: ruleAyinBlock returns [EObject current=null] : ( () otherlv_1= '{' ( (lv_expressions_2_0= ruleAyinExpression ) )* otherlv_3= '}' ) ;
    public final EObject ruleAyinBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_expressions_2_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:1409:2: ( ( () otherlv_1= '{' ( (lv_expressions_2_0= ruleAyinExpression ) )* otherlv_3= '}' ) )
            // InternalAyin.g:1410:2: ( () otherlv_1= '{' ( (lv_expressions_2_0= ruleAyinExpression ) )* otherlv_3= '}' )
            {
            // InternalAyin.g:1410:2: ( () otherlv_1= '{' ( (lv_expressions_2_0= ruleAyinExpression ) )* otherlv_3= '}' )
            // InternalAyin.g:1411:3: () otherlv_1= '{' ( (lv_expressions_2_0= ruleAyinExpression ) )* otherlv_3= '}'
            {
            // InternalAyin.g:1411:3: ()
            // InternalAyin.g:1412:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAyinBlockAccess().getAyinBlockAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,17,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getAyinBlockAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalAyin.g:1422:3: ( (lv_expressions_2_0= ruleAyinExpression ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==RULE_ID||(LA30_0>=36 && LA30_0<=37)||(LA30_0>=39 && LA30_0<=40)||LA30_0==43||LA30_0==45||(LA30_0>=47 && LA30_0<=53)) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalAyin.g:1423:4: (lv_expressions_2_0= ruleAyinExpression )
            	    {
            	    // InternalAyin.g:1423:4: (lv_expressions_2_0= ruleAyinExpression )
            	    // InternalAyin.g:1424:5: lv_expressions_2_0= ruleAyinExpression
            	    {

            	    					newCompositeNode(grammarAccess.getAyinBlockAccess().getExpressionsAyinExpressionParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_30);
            	    lv_expressions_2_0=ruleAyinExpression();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAyinBlockRule());
            	    					}
            	    					add(
            	    						current,
            	    						"expressions",
            	    						lv_expressions_2_0,
            	    						"space.ayin.lang.Ayin.AyinExpression");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

            otherlv_3=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getAyinBlockAccess().getRightCurlyBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinBlock"


    // $ANTLR start "entryRuleAyinExpression"
    // InternalAyin.g:1449:1: entryRuleAyinExpression returns [EObject current=null] : iv_ruleAyinExpression= ruleAyinExpression EOF ;
    public final EObject entryRuleAyinExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinExpression = null;


        try {
            // InternalAyin.g:1449:55: (iv_ruleAyinExpression= ruleAyinExpression EOF )
            // InternalAyin.g:1450:2: iv_ruleAyinExpression= ruleAyinExpression EOF
            {
             newCompositeNode(grammarAccess.getAyinExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinExpression=ruleAyinExpression();

            state._fsp--;

             current =iv_ruleAyinExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinExpression"


    // $ANTLR start "ruleAyinExpression"
    // InternalAyin.g:1456:1: ruleAyinExpression returns [EObject current=null] : (this_AyinAssignment_0= ruleAyinAssignment | this_AyinIf_1= ruleAyinIf | this_AyinWhile_2= ruleAyinWhile | this_AyinTry_3= ruleAyinTry | this_AyinThrow_4= ruleAyinThrow ) ;
    public final EObject ruleAyinExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AyinAssignment_0 = null;

        EObject this_AyinIf_1 = null;

        EObject this_AyinWhile_2 = null;

        EObject this_AyinTry_3 = null;

        EObject this_AyinThrow_4 = null;



        	enterRule();

        try {
            // InternalAyin.g:1462:2: ( (this_AyinAssignment_0= ruleAyinAssignment | this_AyinIf_1= ruleAyinIf | this_AyinWhile_2= ruleAyinWhile | this_AyinTry_3= ruleAyinTry | this_AyinThrow_4= ruleAyinThrow ) )
            // InternalAyin.g:1463:2: (this_AyinAssignment_0= ruleAyinAssignment | this_AyinIf_1= ruleAyinIf | this_AyinWhile_2= ruleAyinWhile | this_AyinTry_3= ruleAyinTry | this_AyinThrow_4= ruleAyinThrow )
            {
            // InternalAyin.g:1463:2: (this_AyinAssignment_0= ruleAyinAssignment | this_AyinIf_1= ruleAyinIf | this_AyinWhile_2= ruleAyinWhile | this_AyinTry_3= ruleAyinTry | this_AyinThrow_4= ruleAyinThrow )
            int alt31=5;
            switch ( input.LA(1) ) {
            case RULE_ID:
            case 36:
            case 45:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
                {
                alt31=1;
                }
                break;
            case 37:
                {
                alt31=2;
                }
                break;
            case 39:
                {
                alt31=3;
                }
                break;
            case 40:
                {
                alt31=4;
                }
                break;
            case 43:
                {
                alt31=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }

            switch (alt31) {
                case 1 :
                    // InternalAyin.g:1464:3: this_AyinAssignment_0= ruleAyinAssignment
                    {

                    			newCompositeNode(grammarAccess.getAyinExpressionAccess().getAyinAssignmentParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinAssignment_0=ruleAyinAssignment();

                    state._fsp--;


                    			current = this_AyinAssignment_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAyin.g:1473:3: this_AyinIf_1= ruleAyinIf
                    {

                    			newCompositeNode(grammarAccess.getAyinExpressionAccess().getAyinIfParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinIf_1=ruleAyinIf();

                    state._fsp--;


                    			current = this_AyinIf_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalAyin.g:1482:3: this_AyinWhile_2= ruleAyinWhile
                    {

                    			newCompositeNode(grammarAccess.getAyinExpressionAccess().getAyinWhileParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinWhile_2=ruleAyinWhile();

                    state._fsp--;


                    			current = this_AyinWhile_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalAyin.g:1491:3: this_AyinTry_3= ruleAyinTry
                    {

                    			newCompositeNode(grammarAccess.getAyinExpressionAccess().getAyinTryParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinTry_3=ruleAyinTry();

                    state._fsp--;


                    			current = this_AyinTry_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalAyin.g:1500:3: this_AyinThrow_4= ruleAyinThrow
                    {

                    			newCompositeNode(grammarAccess.getAyinExpressionAccess().getAyinThrowParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinThrow_4=ruleAyinThrow();

                    state._fsp--;


                    			current = this_AyinThrow_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinExpression"


    // $ANTLR start "entryRuleAyinMethodCall"
    // InternalAyin.g:1512:1: entryRuleAyinMethodCall returns [EObject current=null] : iv_ruleAyinMethodCall= ruleAyinMethodCall EOF ;
    public final EObject entryRuleAyinMethodCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinMethodCall = null;


        try {
            // InternalAyin.g:1512:55: (iv_ruleAyinMethodCall= ruleAyinMethodCall EOF )
            // InternalAyin.g:1513:2: iv_ruleAyinMethodCall= ruleAyinMethodCall EOF
            {
             newCompositeNode(grammarAccess.getAyinMethodCallRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinMethodCall=ruleAyinMethodCall();

            state._fsp--;

             current =iv_ruleAyinMethodCall; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinMethodCall"


    // $ANTLR start "ruleAyinMethodCall"
    // InternalAyin.g:1519:1: ruleAyinMethodCall returns [EObject current=null] : ( ( ( (lv_isThis_0_0= 'this' ) ) | ( (lv_executor_1_0= ruleAyinRef ) ) ) otherlv_2= '.' ( (otherlv_3= RULE_ID ) ) otherlv_4= '(' ( ( (lv_assignments_5_0= ruleAyinArgumentAssignment ) ) (otherlv_6= ',' ( (lv_assignments_7_0= ruleAyinArgumentAssignment ) ) )* )? otherlv_8= ')' ) ;
    public final EObject ruleAyinMethodCall() throws RecognitionException {
        EObject current = null;

        Token lv_isThis_0_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_executor_1_0 = null;

        EObject lv_assignments_5_0 = null;

        EObject lv_assignments_7_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:1525:2: ( ( ( ( (lv_isThis_0_0= 'this' ) ) | ( (lv_executor_1_0= ruleAyinRef ) ) ) otherlv_2= '.' ( (otherlv_3= RULE_ID ) ) otherlv_4= '(' ( ( (lv_assignments_5_0= ruleAyinArgumentAssignment ) ) (otherlv_6= ',' ( (lv_assignments_7_0= ruleAyinArgumentAssignment ) ) )* )? otherlv_8= ')' ) )
            // InternalAyin.g:1526:2: ( ( ( (lv_isThis_0_0= 'this' ) ) | ( (lv_executor_1_0= ruleAyinRef ) ) ) otherlv_2= '.' ( (otherlv_3= RULE_ID ) ) otherlv_4= '(' ( ( (lv_assignments_5_0= ruleAyinArgumentAssignment ) ) (otherlv_6= ',' ( (lv_assignments_7_0= ruleAyinArgumentAssignment ) ) )* )? otherlv_8= ')' )
            {
            // InternalAyin.g:1526:2: ( ( ( (lv_isThis_0_0= 'this' ) ) | ( (lv_executor_1_0= ruleAyinRef ) ) ) otherlv_2= '.' ( (otherlv_3= RULE_ID ) ) otherlv_4= '(' ( ( (lv_assignments_5_0= ruleAyinArgumentAssignment ) ) (otherlv_6= ',' ( (lv_assignments_7_0= ruleAyinArgumentAssignment ) ) )* )? otherlv_8= ')' )
            // InternalAyin.g:1527:3: ( ( (lv_isThis_0_0= 'this' ) ) | ( (lv_executor_1_0= ruleAyinRef ) ) ) otherlv_2= '.' ( (otherlv_3= RULE_ID ) ) otherlv_4= '(' ( ( (lv_assignments_5_0= ruleAyinArgumentAssignment ) ) (otherlv_6= ',' ( (lv_assignments_7_0= ruleAyinArgumentAssignment ) ) )* )? otherlv_8= ')'
            {
            // InternalAyin.g:1527:3: ( ( (lv_isThis_0_0= 'this' ) ) | ( (lv_executor_1_0= ruleAyinRef ) ) )
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==36) ) {
                alt32=1;
            }
            else if ( (LA32_0==RULE_ID) ) {
                alt32=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }
            switch (alt32) {
                case 1 :
                    // InternalAyin.g:1528:4: ( (lv_isThis_0_0= 'this' ) )
                    {
                    // InternalAyin.g:1528:4: ( (lv_isThis_0_0= 'this' ) )
                    // InternalAyin.g:1529:5: (lv_isThis_0_0= 'this' )
                    {
                    // InternalAyin.g:1529:5: (lv_isThis_0_0= 'this' )
                    // InternalAyin.g:1530:6: lv_isThis_0_0= 'this'
                    {
                    lv_isThis_0_0=(Token)match(input,36,FOLLOW_31); 

                    						newLeafNode(lv_isThis_0_0, grammarAccess.getAyinMethodCallAccess().getIsThisThisKeyword_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAyinMethodCallRule());
                    						}
                    						setWithLastConsumed(current, "isThis", true, "this");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAyin.g:1543:4: ( (lv_executor_1_0= ruleAyinRef ) )
                    {
                    // InternalAyin.g:1543:4: ( (lv_executor_1_0= ruleAyinRef ) )
                    // InternalAyin.g:1544:5: (lv_executor_1_0= ruleAyinRef )
                    {
                    // InternalAyin.g:1544:5: (lv_executor_1_0= ruleAyinRef )
                    // InternalAyin.g:1545:6: lv_executor_1_0= ruleAyinRef
                    {

                    						newCompositeNode(grammarAccess.getAyinMethodCallAccess().getExecutorAyinRefParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_31);
                    lv_executor_1_0=ruleAyinRef();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinMethodCallRule());
                    						}
                    						set(
                    							current,
                    							"executor",
                    							lv_executor_1_0,
                    							"space.ayin.lang.Ayin.AyinRef");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,15,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getAyinMethodCallAccess().getFullStopKeyword_1());
            		
            // InternalAyin.g:1567:3: ( (otherlv_3= RULE_ID ) )
            // InternalAyin.g:1568:4: (otherlv_3= RULE_ID )
            {
            // InternalAyin.g:1568:4: (otherlv_3= RULE_ID )
            // InternalAyin.g:1569:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinMethodCallRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_19); 

            					newLeafNode(otherlv_3, grammarAccess.getAyinMethodCallAccess().getMethodAyinMethodCrossReference_2_0());
            				

            }


            }

            otherlv_4=(Token)match(input,30,FOLLOW_32); 

            			newLeafNode(otherlv_4, grammarAccess.getAyinMethodCallAccess().getLeftParenthesisKeyword_3());
            		
            // InternalAyin.g:1584:3: ( ( (lv_assignments_5_0= ruleAyinArgumentAssignment ) ) (otherlv_6= ',' ( (lv_assignments_7_0= ruleAyinArgumentAssignment ) ) )* )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==RULE_ID||LA34_0==45) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalAyin.g:1585:4: ( (lv_assignments_5_0= ruleAyinArgumentAssignment ) ) (otherlv_6= ',' ( (lv_assignments_7_0= ruleAyinArgumentAssignment ) ) )*
                    {
                    // InternalAyin.g:1585:4: ( (lv_assignments_5_0= ruleAyinArgumentAssignment ) )
                    // InternalAyin.g:1586:5: (lv_assignments_5_0= ruleAyinArgumentAssignment )
                    {
                    // InternalAyin.g:1586:5: (lv_assignments_5_0= ruleAyinArgumentAssignment )
                    // InternalAyin.g:1587:6: lv_assignments_5_0= ruleAyinArgumentAssignment
                    {

                    						newCompositeNode(grammarAccess.getAyinMethodCallAccess().getAssignmentsAyinArgumentAssignmentParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_23);
                    lv_assignments_5_0=ruleAyinArgumentAssignment();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinMethodCallRule());
                    						}
                    						add(
                    							current,
                    							"assignments",
                    							lv_assignments_5_0,
                    							"space.ayin.lang.Ayin.AyinArgumentAssignment");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalAyin.g:1604:4: (otherlv_6= ',' ( (lv_assignments_7_0= ruleAyinArgumentAssignment ) ) )*
                    loop33:
                    do {
                        int alt33=2;
                        int LA33_0 = input.LA(1);

                        if ( (LA33_0==25) ) {
                            alt33=1;
                        }


                        switch (alt33) {
                    	case 1 :
                    	    // InternalAyin.g:1605:5: otherlv_6= ',' ( (lv_assignments_7_0= ruleAyinArgumentAssignment ) )
                    	    {
                    	    otherlv_6=(Token)match(input,25,FOLLOW_33); 

                    	    					newLeafNode(otherlv_6, grammarAccess.getAyinMethodCallAccess().getCommaKeyword_4_1_0());
                    	    				
                    	    // InternalAyin.g:1609:5: ( (lv_assignments_7_0= ruleAyinArgumentAssignment ) )
                    	    // InternalAyin.g:1610:6: (lv_assignments_7_0= ruleAyinArgumentAssignment )
                    	    {
                    	    // InternalAyin.g:1610:6: (lv_assignments_7_0= ruleAyinArgumentAssignment )
                    	    // InternalAyin.g:1611:7: lv_assignments_7_0= ruleAyinArgumentAssignment
                    	    {

                    	    							newCompositeNode(grammarAccess.getAyinMethodCallAccess().getAssignmentsAyinArgumentAssignmentParserRuleCall_4_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_23);
                    	    lv_assignments_7_0=ruleAyinArgumentAssignment();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getAyinMethodCallRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"assignments",
                    	    								lv_assignments_7_0,
                    	    								"space.ayin.lang.Ayin.AyinArgumentAssignment");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop33;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_8=(Token)match(input,31,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getAyinMethodCallAccess().getRightParenthesisKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinMethodCall"


    // $ANTLR start "entryRuleAyinIf"
    // InternalAyin.g:1638:1: entryRuleAyinIf returns [EObject current=null] : iv_ruleAyinIf= ruleAyinIf EOF ;
    public final EObject entryRuleAyinIf() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinIf = null;


        try {
            // InternalAyin.g:1638:47: (iv_ruleAyinIf= ruleAyinIf EOF )
            // InternalAyin.g:1639:2: iv_ruleAyinIf= ruleAyinIf EOF
            {
             newCompositeNode(grammarAccess.getAyinIfRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinIf=ruleAyinIf();

            state._fsp--;

             current =iv_ruleAyinIf; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinIf"


    // $ANTLR start "ruleAyinIf"
    // InternalAyin.g:1645:1: ruleAyinIf returns [EObject current=null] : (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleAyinValueExpression ) ) otherlv_3= ')' ( (lv_thenBlock_4_0= ruleAyinBlock ) ) (otherlv_5= 'else' ( (lv_elseClause_6_0= ruleAyinElseClause ) ) )? ) ;
    public final EObject ruleAyinIf() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_condition_2_0 = null;

        EObject lv_thenBlock_4_0 = null;

        EObject lv_elseClause_6_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:1651:2: ( (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleAyinValueExpression ) ) otherlv_3= ')' ( (lv_thenBlock_4_0= ruleAyinBlock ) ) (otherlv_5= 'else' ( (lv_elseClause_6_0= ruleAyinElseClause ) ) )? ) )
            // InternalAyin.g:1652:2: (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleAyinValueExpression ) ) otherlv_3= ')' ( (lv_thenBlock_4_0= ruleAyinBlock ) ) (otherlv_5= 'else' ( (lv_elseClause_6_0= ruleAyinElseClause ) ) )? )
            {
            // InternalAyin.g:1652:2: (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleAyinValueExpression ) ) otherlv_3= ')' ( (lv_thenBlock_4_0= ruleAyinBlock ) ) (otherlv_5= 'else' ( (lv_elseClause_6_0= ruleAyinElseClause ) ) )? )
            // InternalAyin.g:1653:3: otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleAyinValueExpression ) ) otherlv_3= ')' ( (lv_thenBlock_4_0= ruleAyinBlock ) ) (otherlv_5= 'else' ( (lv_elseClause_6_0= ruleAyinElseClause ) ) )?
            {
            otherlv_0=(Token)match(input,37,FOLLOW_19); 

            			newLeafNode(otherlv_0, grammarAccess.getAyinIfAccess().getIfKeyword_0());
            		
            otherlv_1=(Token)match(input,30,FOLLOW_34); 

            			newLeafNode(otherlv_1, grammarAccess.getAyinIfAccess().getLeftParenthesisKeyword_1());
            		
            // InternalAyin.g:1661:3: ( (lv_condition_2_0= ruleAyinValueExpression ) )
            // InternalAyin.g:1662:4: (lv_condition_2_0= ruleAyinValueExpression )
            {
            // InternalAyin.g:1662:4: (lv_condition_2_0= ruleAyinValueExpression )
            // InternalAyin.g:1663:5: lv_condition_2_0= ruleAyinValueExpression
            {

            					newCompositeNode(grammarAccess.getAyinIfAccess().getConditionAyinValueExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_21);
            lv_condition_2_0=ruleAyinValueExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAyinIfRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_2_0,
            						"space.ayin.lang.Ayin.AyinValueExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,31,FOLLOW_25); 

            			newLeafNode(otherlv_3, grammarAccess.getAyinIfAccess().getRightParenthesisKeyword_3());
            		
            // InternalAyin.g:1684:3: ( (lv_thenBlock_4_0= ruleAyinBlock ) )
            // InternalAyin.g:1685:4: (lv_thenBlock_4_0= ruleAyinBlock )
            {
            // InternalAyin.g:1685:4: (lv_thenBlock_4_0= ruleAyinBlock )
            // InternalAyin.g:1686:5: lv_thenBlock_4_0= ruleAyinBlock
            {

            					newCompositeNode(grammarAccess.getAyinIfAccess().getThenBlockAyinBlockParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_35);
            lv_thenBlock_4_0=ruleAyinBlock();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAyinIfRule());
            					}
            					set(
            						current,
            						"thenBlock",
            						lv_thenBlock_4_0,
            						"space.ayin.lang.Ayin.AyinBlock");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAyin.g:1703:3: (otherlv_5= 'else' ( (lv_elseClause_6_0= ruleAyinElseClause ) ) )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==38) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalAyin.g:1704:4: otherlv_5= 'else' ( (lv_elseClause_6_0= ruleAyinElseClause ) )
                    {
                    otherlv_5=(Token)match(input,38,FOLLOW_36); 

                    				newLeafNode(otherlv_5, grammarAccess.getAyinIfAccess().getElseKeyword_5_0());
                    			
                    // InternalAyin.g:1708:4: ( (lv_elseClause_6_0= ruleAyinElseClause ) )
                    // InternalAyin.g:1709:5: (lv_elseClause_6_0= ruleAyinElseClause )
                    {
                    // InternalAyin.g:1709:5: (lv_elseClause_6_0= ruleAyinElseClause )
                    // InternalAyin.g:1710:6: lv_elseClause_6_0= ruleAyinElseClause
                    {

                    						newCompositeNode(grammarAccess.getAyinIfAccess().getElseClauseAyinElseClauseParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_elseClause_6_0=ruleAyinElseClause();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinIfRule());
                    						}
                    						set(
                    							current,
                    							"elseClause",
                    							lv_elseClause_6_0,
                    							"space.ayin.lang.Ayin.AyinElseClause");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinIf"


    // $ANTLR start "entryRuleAyinElseClause"
    // InternalAyin.g:1732:1: entryRuleAyinElseClause returns [EObject current=null] : iv_ruleAyinElseClause= ruleAyinElseClause EOF ;
    public final EObject entryRuleAyinElseClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinElseClause = null;


        try {
            // InternalAyin.g:1732:55: (iv_ruleAyinElseClause= ruleAyinElseClause EOF )
            // InternalAyin.g:1733:2: iv_ruleAyinElseClause= ruleAyinElseClause EOF
            {
             newCompositeNode(grammarAccess.getAyinElseClauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinElseClause=ruleAyinElseClause();

            state._fsp--;

             current =iv_ruleAyinElseClause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinElseClause"


    // $ANTLR start "ruleAyinElseClause"
    // InternalAyin.g:1739:1: ruleAyinElseClause returns [EObject current=null] : (this_AyinIf_0= ruleAyinIf | this_AyinWhile_1= ruleAyinWhile | this_AyinBlock_2= ruleAyinBlock | this_AyinTry_3= ruleAyinTry ) ;
    public final EObject ruleAyinElseClause() throws RecognitionException {
        EObject current = null;

        EObject this_AyinIf_0 = null;

        EObject this_AyinWhile_1 = null;

        EObject this_AyinBlock_2 = null;

        EObject this_AyinTry_3 = null;



        	enterRule();

        try {
            // InternalAyin.g:1745:2: ( (this_AyinIf_0= ruleAyinIf | this_AyinWhile_1= ruleAyinWhile | this_AyinBlock_2= ruleAyinBlock | this_AyinTry_3= ruleAyinTry ) )
            // InternalAyin.g:1746:2: (this_AyinIf_0= ruleAyinIf | this_AyinWhile_1= ruleAyinWhile | this_AyinBlock_2= ruleAyinBlock | this_AyinTry_3= ruleAyinTry )
            {
            // InternalAyin.g:1746:2: (this_AyinIf_0= ruleAyinIf | this_AyinWhile_1= ruleAyinWhile | this_AyinBlock_2= ruleAyinBlock | this_AyinTry_3= ruleAyinTry )
            int alt36=4;
            switch ( input.LA(1) ) {
            case 37:
                {
                alt36=1;
                }
                break;
            case 39:
                {
                alt36=2;
                }
                break;
            case 17:
                {
                alt36=3;
                }
                break;
            case 40:
                {
                alt36=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;
            }

            switch (alt36) {
                case 1 :
                    // InternalAyin.g:1747:3: this_AyinIf_0= ruleAyinIf
                    {

                    			newCompositeNode(grammarAccess.getAyinElseClauseAccess().getAyinIfParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinIf_0=ruleAyinIf();

                    state._fsp--;


                    			current = this_AyinIf_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAyin.g:1756:3: this_AyinWhile_1= ruleAyinWhile
                    {

                    			newCompositeNode(grammarAccess.getAyinElseClauseAccess().getAyinWhileParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinWhile_1=ruleAyinWhile();

                    state._fsp--;


                    			current = this_AyinWhile_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalAyin.g:1765:3: this_AyinBlock_2= ruleAyinBlock
                    {

                    			newCompositeNode(grammarAccess.getAyinElseClauseAccess().getAyinBlockParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinBlock_2=ruleAyinBlock();

                    state._fsp--;


                    			current = this_AyinBlock_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalAyin.g:1774:3: this_AyinTry_3= ruleAyinTry
                    {

                    			newCompositeNode(grammarAccess.getAyinElseClauseAccess().getAyinTryParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinTry_3=ruleAyinTry();

                    state._fsp--;


                    			current = this_AyinTry_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinElseClause"


    // $ANTLR start "entryRuleAyinWhile"
    // InternalAyin.g:1786:1: entryRuleAyinWhile returns [EObject current=null] : iv_ruleAyinWhile= ruleAyinWhile EOF ;
    public final EObject entryRuleAyinWhile() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinWhile = null;


        try {
            // InternalAyin.g:1786:50: (iv_ruleAyinWhile= ruleAyinWhile EOF )
            // InternalAyin.g:1787:2: iv_ruleAyinWhile= ruleAyinWhile EOF
            {
             newCompositeNode(grammarAccess.getAyinWhileRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinWhile=ruleAyinWhile();

            state._fsp--;

             current =iv_ruleAyinWhile; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinWhile"


    // $ANTLR start "ruleAyinWhile"
    // InternalAyin.g:1793:1: ruleAyinWhile returns [EObject current=null] : (otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleAyinValueExpression ) ) otherlv_3= ')' ( (lv_block_4_0= ruleAyinBlock ) ) ) ;
    public final EObject ruleAyinWhile() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_condition_2_0 = null;

        EObject lv_block_4_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:1799:2: ( (otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleAyinValueExpression ) ) otherlv_3= ')' ( (lv_block_4_0= ruleAyinBlock ) ) ) )
            // InternalAyin.g:1800:2: (otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleAyinValueExpression ) ) otherlv_3= ')' ( (lv_block_4_0= ruleAyinBlock ) ) )
            {
            // InternalAyin.g:1800:2: (otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleAyinValueExpression ) ) otherlv_3= ')' ( (lv_block_4_0= ruleAyinBlock ) ) )
            // InternalAyin.g:1801:3: otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleAyinValueExpression ) ) otherlv_3= ')' ( (lv_block_4_0= ruleAyinBlock ) )
            {
            otherlv_0=(Token)match(input,39,FOLLOW_19); 

            			newLeafNode(otherlv_0, grammarAccess.getAyinWhileAccess().getWhileKeyword_0());
            		
            otherlv_1=(Token)match(input,30,FOLLOW_34); 

            			newLeafNode(otherlv_1, grammarAccess.getAyinWhileAccess().getLeftParenthesisKeyword_1());
            		
            // InternalAyin.g:1809:3: ( (lv_condition_2_0= ruleAyinValueExpression ) )
            // InternalAyin.g:1810:4: (lv_condition_2_0= ruleAyinValueExpression )
            {
            // InternalAyin.g:1810:4: (lv_condition_2_0= ruleAyinValueExpression )
            // InternalAyin.g:1811:5: lv_condition_2_0= ruleAyinValueExpression
            {

            					newCompositeNode(grammarAccess.getAyinWhileAccess().getConditionAyinValueExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_21);
            lv_condition_2_0=ruleAyinValueExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAyinWhileRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_2_0,
            						"space.ayin.lang.Ayin.AyinValueExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,31,FOLLOW_25); 

            			newLeafNode(otherlv_3, grammarAccess.getAyinWhileAccess().getRightParenthesisKeyword_3());
            		
            // InternalAyin.g:1832:3: ( (lv_block_4_0= ruleAyinBlock ) )
            // InternalAyin.g:1833:4: (lv_block_4_0= ruleAyinBlock )
            {
            // InternalAyin.g:1833:4: (lv_block_4_0= ruleAyinBlock )
            // InternalAyin.g:1834:5: lv_block_4_0= ruleAyinBlock
            {

            					newCompositeNode(grammarAccess.getAyinWhileAccess().getBlockAyinBlockParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_2);
            lv_block_4_0=ruleAyinBlock();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAyinWhileRule());
            					}
            					set(
            						current,
            						"block",
            						lv_block_4_0,
            						"space.ayin.lang.Ayin.AyinBlock");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinWhile"


    // $ANTLR start "entryRuleAyinTry"
    // InternalAyin.g:1855:1: entryRuleAyinTry returns [EObject current=null] : iv_ruleAyinTry= ruleAyinTry EOF ;
    public final EObject entryRuleAyinTry() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinTry = null;


        try {
            // InternalAyin.g:1855:48: (iv_ruleAyinTry= ruleAyinTry EOF )
            // InternalAyin.g:1856:2: iv_ruleAyinTry= ruleAyinTry EOF
            {
             newCompositeNode(grammarAccess.getAyinTryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinTry=ruleAyinTry();

            state._fsp--;

             current =iv_ruleAyinTry; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinTry"


    // $ANTLR start "ruleAyinTry"
    // InternalAyin.g:1862:1: ruleAyinTry returns [EObject current=null] : (otherlv_0= 'try' ( (lv_tryBlock_1_0= ruleAyinBlock ) ) (otherlv_2= 'catch' otherlv_3= '(' ( (lv_catchedExceptions_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_catchedExceptions_6_0= ruleAyinParameter ) ) )* otherlv_7= ')' ( (lv_catchBlock_8_0= ruleAyinBlock ) ) ) (otherlv_9= 'finally' ( (lv_finallyBlock_10_0= ruleAyinBlock ) ) )? ) ;
    public final EObject ruleAyinTry() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_tryBlock_1_0 = null;

        EObject lv_catchedExceptions_4_0 = null;

        EObject lv_catchedExceptions_6_0 = null;

        EObject lv_catchBlock_8_0 = null;

        EObject lv_finallyBlock_10_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:1868:2: ( (otherlv_0= 'try' ( (lv_tryBlock_1_0= ruleAyinBlock ) ) (otherlv_2= 'catch' otherlv_3= '(' ( (lv_catchedExceptions_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_catchedExceptions_6_0= ruleAyinParameter ) ) )* otherlv_7= ')' ( (lv_catchBlock_8_0= ruleAyinBlock ) ) ) (otherlv_9= 'finally' ( (lv_finallyBlock_10_0= ruleAyinBlock ) ) )? ) )
            // InternalAyin.g:1869:2: (otherlv_0= 'try' ( (lv_tryBlock_1_0= ruleAyinBlock ) ) (otherlv_2= 'catch' otherlv_3= '(' ( (lv_catchedExceptions_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_catchedExceptions_6_0= ruleAyinParameter ) ) )* otherlv_7= ')' ( (lv_catchBlock_8_0= ruleAyinBlock ) ) ) (otherlv_9= 'finally' ( (lv_finallyBlock_10_0= ruleAyinBlock ) ) )? )
            {
            // InternalAyin.g:1869:2: (otherlv_0= 'try' ( (lv_tryBlock_1_0= ruleAyinBlock ) ) (otherlv_2= 'catch' otherlv_3= '(' ( (lv_catchedExceptions_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_catchedExceptions_6_0= ruleAyinParameter ) ) )* otherlv_7= ')' ( (lv_catchBlock_8_0= ruleAyinBlock ) ) ) (otherlv_9= 'finally' ( (lv_finallyBlock_10_0= ruleAyinBlock ) ) )? )
            // InternalAyin.g:1870:3: otherlv_0= 'try' ( (lv_tryBlock_1_0= ruleAyinBlock ) ) (otherlv_2= 'catch' otherlv_3= '(' ( (lv_catchedExceptions_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_catchedExceptions_6_0= ruleAyinParameter ) ) )* otherlv_7= ')' ( (lv_catchBlock_8_0= ruleAyinBlock ) ) ) (otherlv_9= 'finally' ( (lv_finallyBlock_10_0= ruleAyinBlock ) ) )?
            {
            otherlv_0=(Token)match(input,40,FOLLOW_25); 

            			newLeafNode(otherlv_0, grammarAccess.getAyinTryAccess().getTryKeyword_0());
            		
            // InternalAyin.g:1874:3: ( (lv_tryBlock_1_0= ruleAyinBlock ) )
            // InternalAyin.g:1875:4: (lv_tryBlock_1_0= ruleAyinBlock )
            {
            // InternalAyin.g:1875:4: (lv_tryBlock_1_0= ruleAyinBlock )
            // InternalAyin.g:1876:5: lv_tryBlock_1_0= ruleAyinBlock
            {

            					newCompositeNode(grammarAccess.getAyinTryAccess().getTryBlockAyinBlockParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_37);
            lv_tryBlock_1_0=ruleAyinBlock();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAyinTryRule());
            					}
            					set(
            						current,
            						"tryBlock",
            						lv_tryBlock_1_0,
            						"space.ayin.lang.Ayin.AyinBlock");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAyin.g:1893:3: (otherlv_2= 'catch' otherlv_3= '(' ( (lv_catchedExceptions_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_catchedExceptions_6_0= ruleAyinParameter ) ) )* otherlv_7= ')' ( (lv_catchBlock_8_0= ruleAyinBlock ) ) )
            // InternalAyin.g:1894:4: otherlv_2= 'catch' otherlv_3= '(' ( (lv_catchedExceptions_4_0= ruleAyinParameter ) ) (otherlv_5= ',' ( (lv_catchedExceptions_6_0= ruleAyinParameter ) ) )* otherlv_7= ')' ( (lv_catchBlock_8_0= ruleAyinBlock ) )
            {
            otherlv_2=(Token)match(input,41,FOLLOW_19); 

            				newLeafNode(otherlv_2, grammarAccess.getAyinTryAccess().getCatchKeyword_2_0());
            			
            otherlv_3=(Token)match(input,30,FOLLOW_28); 

            				newLeafNode(otherlv_3, grammarAccess.getAyinTryAccess().getLeftParenthesisKeyword_2_1());
            			
            // InternalAyin.g:1902:4: ( (lv_catchedExceptions_4_0= ruleAyinParameter ) )
            // InternalAyin.g:1903:5: (lv_catchedExceptions_4_0= ruleAyinParameter )
            {
            // InternalAyin.g:1903:5: (lv_catchedExceptions_4_0= ruleAyinParameter )
            // InternalAyin.g:1904:6: lv_catchedExceptions_4_0= ruleAyinParameter
            {

            						newCompositeNode(grammarAccess.getAyinTryAccess().getCatchedExceptionsAyinParameterParserRuleCall_2_2_0());
            					
            pushFollow(FOLLOW_23);
            lv_catchedExceptions_4_0=ruleAyinParameter();

            state._fsp--;


            						if (current==null) {
            							current = createModelElementForParent(grammarAccess.getAyinTryRule());
            						}
            						add(
            							current,
            							"catchedExceptions",
            							lv_catchedExceptions_4_0,
            							"space.ayin.lang.Ayin.AyinParameter");
            						afterParserOrEnumRuleCall();
            					

            }


            }

            // InternalAyin.g:1921:4: (otherlv_5= ',' ( (lv_catchedExceptions_6_0= ruleAyinParameter ) ) )*
            loop37:
            do {
                int alt37=2;
                int LA37_0 = input.LA(1);

                if ( (LA37_0==25) ) {
                    alt37=1;
                }


                switch (alt37) {
            	case 1 :
            	    // InternalAyin.g:1922:5: otherlv_5= ',' ( (lv_catchedExceptions_6_0= ruleAyinParameter ) )
            	    {
            	    otherlv_5=(Token)match(input,25,FOLLOW_28); 

            	    					newLeafNode(otherlv_5, grammarAccess.getAyinTryAccess().getCommaKeyword_2_3_0());
            	    				
            	    // InternalAyin.g:1926:5: ( (lv_catchedExceptions_6_0= ruleAyinParameter ) )
            	    // InternalAyin.g:1927:6: (lv_catchedExceptions_6_0= ruleAyinParameter )
            	    {
            	    // InternalAyin.g:1927:6: (lv_catchedExceptions_6_0= ruleAyinParameter )
            	    // InternalAyin.g:1928:7: lv_catchedExceptions_6_0= ruleAyinParameter
            	    {

            	    							newCompositeNode(grammarAccess.getAyinTryAccess().getCatchedExceptionsAyinParameterParserRuleCall_2_3_1_0());
            	    						
            	    pushFollow(FOLLOW_23);
            	    lv_catchedExceptions_6_0=ruleAyinParameter();

            	    state._fsp--;


            	    							if (current==null) {
            	    								current = createModelElementForParent(grammarAccess.getAyinTryRule());
            	    							}
            	    							add(
            	    								current,
            	    								"catchedExceptions",
            	    								lv_catchedExceptions_6_0,
            	    								"space.ayin.lang.Ayin.AyinParameter");
            	    							afterParserOrEnumRuleCall();
            	    						

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop37;
                }
            } while (true);

            otherlv_7=(Token)match(input,31,FOLLOW_25); 

            				newLeafNode(otherlv_7, grammarAccess.getAyinTryAccess().getRightParenthesisKeyword_2_4());
            			
            // InternalAyin.g:1950:4: ( (lv_catchBlock_8_0= ruleAyinBlock ) )
            // InternalAyin.g:1951:5: (lv_catchBlock_8_0= ruleAyinBlock )
            {
            // InternalAyin.g:1951:5: (lv_catchBlock_8_0= ruleAyinBlock )
            // InternalAyin.g:1952:6: lv_catchBlock_8_0= ruleAyinBlock
            {

            						newCompositeNode(grammarAccess.getAyinTryAccess().getCatchBlockAyinBlockParserRuleCall_2_5_0());
            					
            pushFollow(FOLLOW_38);
            lv_catchBlock_8_0=ruleAyinBlock();

            state._fsp--;


            						if (current==null) {
            							current = createModelElementForParent(grammarAccess.getAyinTryRule());
            						}
            						set(
            							current,
            							"catchBlock",
            							lv_catchBlock_8_0,
            							"space.ayin.lang.Ayin.AyinBlock");
            						afterParserOrEnumRuleCall();
            					

            }


            }


            }

            // InternalAyin.g:1970:3: (otherlv_9= 'finally' ( (lv_finallyBlock_10_0= ruleAyinBlock ) ) )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==42) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalAyin.g:1971:4: otherlv_9= 'finally' ( (lv_finallyBlock_10_0= ruleAyinBlock ) )
                    {
                    otherlv_9=(Token)match(input,42,FOLLOW_25); 

                    				newLeafNode(otherlv_9, grammarAccess.getAyinTryAccess().getFinallyKeyword_3_0());
                    			
                    // InternalAyin.g:1975:4: ( (lv_finallyBlock_10_0= ruleAyinBlock ) )
                    // InternalAyin.g:1976:5: (lv_finallyBlock_10_0= ruleAyinBlock )
                    {
                    // InternalAyin.g:1976:5: (lv_finallyBlock_10_0= ruleAyinBlock )
                    // InternalAyin.g:1977:6: lv_finallyBlock_10_0= ruleAyinBlock
                    {

                    						newCompositeNode(grammarAccess.getAyinTryAccess().getFinallyBlockAyinBlockParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_finallyBlock_10_0=ruleAyinBlock();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinTryRule());
                    						}
                    						set(
                    							current,
                    							"finallyBlock",
                    							lv_finallyBlock_10_0,
                    							"space.ayin.lang.Ayin.AyinBlock");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinTry"


    // $ANTLR start "entryRuleAyinValueExpression"
    // InternalAyin.g:1999:1: entryRuleAyinValueExpression returns [EObject current=null] : iv_ruleAyinValueExpression= ruleAyinValueExpression EOF ;
    public final EObject entryRuleAyinValueExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinValueExpression = null;


        try {
            // InternalAyin.g:1999:60: (iv_ruleAyinValueExpression= ruleAyinValueExpression EOF )
            // InternalAyin.g:2000:2: iv_ruleAyinValueExpression= ruleAyinValueExpression EOF
            {
             newCompositeNode(grammarAccess.getAyinValueExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinValueExpression=ruleAyinValueExpression();

            state._fsp--;

             current =iv_ruleAyinValueExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinValueExpression"


    // $ANTLR start "ruleAyinValueExpression"
    // InternalAyin.g:2006:1: ruleAyinValueExpression returns [EObject current=null] : (this_AyinFieldGet_0= ruleAyinFieldGet | this_AyinAtomValue_1= ruleAyinAtomValue | this_AyinNewOperator_2= ruleAyinNewOperator | this_AyinBlock_3= ruleAyinBlock ) ;
    public final EObject ruleAyinValueExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AyinFieldGet_0 = null;

        EObject this_AyinAtomValue_1 = null;

        EObject this_AyinNewOperator_2 = null;

        EObject this_AyinBlock_3 = null;



        	enterRule();

        try {
            // InternalAyin.g:2012:2: ( (this_AyinFieldGet_0= ruleAyinFieldGet | this_AyinAtomValue_1= ruleAyinAtomValue | this_AyinNewOperator_2= ruleAyinNewOperator | this_AyinBlock_3= ruleAyinBlock ) )
            // InternalAyin.g:2013:2: (this_AyinFieldGet_0= ruleAyinFieldGet | this_AyinAtomValue_1= ruleAyinAtomValue | this_AyinNewOperator_2= ruleAyinNewOperator | this_AyinBlock_3= ruleAyinBlock )
            {
            // InternalAyin.g:2013:2: (this_AyinFieldGet_0= ruleAyinFieldGet | this_AyinAtomValue_1= ruleAyinAtomValue | this_AyinNewOperator_2= ruleAyinNewOperator | this_AyinBlock_3= ruleAyinBlock )
            int alt39=4;
            switch ( input.LA(1) ) {
            case RULE_ID:
            case 36:
                {
                alt39=1;
                }
                break;
            case RULE_INTEGER:
            case RULE_DOUBLE:
            case RULE_STRING:
            case RULE_BOOL:
                {
                alt39=2;
                }
                break;
            case 44:
                {
                alt39=3;
                }
                break;
            case 17:
                {
                alt39=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 39, 0, input);

                throw nvae;
            }

            switch (alt39) {
                case 1 :
                    // InternalAyin.g:2014:3: this_AyinFieldGet_0= ruleAyinFieldGet
                    {

                    			newCompositeNode(grammarAccess.getAyinValueExpressionAccess().getAyinFieldGetParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinFieldGet_0=ruleAyinFieldGet();

                    state._fsp--;


                    			current = this_AyinFieldGet_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAyin.g:2023:3: this_AyinAtomValue_1= ruleAyinAtomValue
                    {

                    			newCompositeNode(grammarAccess.getAyinValueExpressionAccess().getAyinAtomValueParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinAtomValue_1=ruleAyinAtomValue();

                    state._fsp--;


                    			current = this_AyinAtomValue_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalAyin.g:2032:3: this_AyinNewOperator_2= ruleAyinNewOperator
                    {

                    			newCompositeNode(grammarAccess.getAyinValueExpressionAccess().getAyinNewOperatorParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinNewOperator_2=ruleAyinNewOperator();

                    state._fsp--;


                    			current = this_AyinNewOperator_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalAyin.g:2041:3: this_AyinBlock_3= ruleAyinBlock
                    {

                    			newCompositeNode(grammarAccess.getAyinValueExpressionAccess().getAyinBlockParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinBlock_3=ruleAyinBlock();

                    state._fsp--;


                    			current = this_AyinBlock_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinValueExpression"


    // $ANTLR start "entryRuleAyinThrow"
    // InternalAyin.g:2053:1: entryRuleAyinThrow returns [EObject current=null] : iv_ruleAyinThrow= ruleAyinThrow EOF ;
    public final EObject entryRuleAyinThrow() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinThrow = null;


        try {
            // InternalAyin.g:2053:50: (iv_ruleAyinThrow= ruleAyinThrow EOF )
            // InternalAyin.g:2054:2: iv_ruleAyinThrow= ruleAyinThrow EOF
            {
             newCompositeNode(grammarAccess.getAyinThrowRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinThrow=ruleAyinThrow();

            state._fsp--;

             current =iv_ruleAyinThrow; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinThrow"


    // $ANTLR start "ruleAyinThrow"
    // InternalAyin.g:2060:1: ruleAyinThrow returns [EObject current=null] : (otherlv_0= 'throw' ( (lv_exception_1_0= ruleAyinValueExpression ) ) otherlv_2= ';' ) ;
    public final EObject ruleAyinThrow() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_exception_1_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:2066:2: ( (otherlv_0= 'throw' ( (lv_exception_1_0= ruleAyinValueExpression ) ) otherlv_2= ';' ) )
            // InternalAyin.g:2067:2: (otherlv_0= 'throw' ( (lv_exception_1_0= ruleAyinValueExpression ) ) otherlv_2= ';' )
            {
            // InternalAyin.g:2067:2: (otherlv_0= 'throw' ( (lv_exception_1_0= ruleAyinValueExpression ) ) otherlv_2= ';' )
            // InternalAyin.g:2068:3: otherlv_0= 'throw' ( (lv_exception_1_0= ruleAyinValueExpression ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,43,FOLLOW_34); 

            			newLeafNode(otherlv_0, grammarAccess.getAyinThrowAccess().getThrowKeyword_0());
            		
            // InternalAyin.g:2072:3: ( (lv_exception_1_0= ruleAyinValueExpression ) )
            // InternalAyin.g:2073:4: (lv_exception_1_0= ruleAyinValueExpression )
            {
            // InternalAyin.g:2073:4: (lv_exception_1_0= ruleAyinValueExpression )
            // InternalAyin.g:2074:5: lv_exception_1_0= ruleAyinValueExpression
            {

            					newCompositeNode(grammarAccess.getAyinThrowAccess().getExceptionAyinValueExpressionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_8);
            lv_exception_1_0=ruleAyinValueExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAyinThrowRule());
            					}
            					set(
            						current,
            						"exception",
            						lv_exception_1_0,
            						"space.ayin.lang.Ayin.AyinValueExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getAyinThrowAccess().getSemicolonKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinThrow"


    // $ANTLR start "entryRuleAyinNewOperator"
    // InternalAyin.g:2099:1: entryRuleAyinNewOperator returns [EObject current=null] : iv_ruleAyinNewOperator= ruleAyinNewOperator EOF ;
    public final EObject entryRuleAyinNewOperator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinNewOperator = null;


        try {
            // InternalAyin.g:2099:56: (iv_ruleAyinNewOperator= ruleAyinNewOperator EOF )
            // InternalAyin.g:2100:2: iv_ruleAyinNewOperator= ruleAyinNewOperator EOF
            {
             newCompositeNode(grammarAccess.getAyinNewOperatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinNewOperator=ruleAyinNewOperator();

            state._fsp--;

             current =iv_ruleAyinNewOperator; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinNewOperator"


    // $ANTLR start "ruleAyinNewOperator"
    // InternalAyin.g:2106:1: ruleAyinNewOperator returns [EObject current=null] : ( ( ( () otherlv_1= 'new' ( (otherlv_2= RULE_ID ) ) ) | ( () otherlv_4= 'new' ( (otherlv_5= RULE_ID ) ) otherlv_6= '<' ( ( (lv_arguments_7_0= ruleAyinType ) ) (otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) ) )* )? otherlv_10= '>' ) ) (otherlv_11= '(' ( ( (lv_assignments_12_0= ruleAyinArgumentAssignment ) ) (otherlv_13= ',' ( (lv_assignments_14_0= ruleAyinArgumentAssignment ) ) )* )? otherlv_15= ')' )? ) ;
    public final EObject ruleAyinNewOperator() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        EObject lv_arguments_7_0 = null;

        EObject lv_arguments_9_0 = null;

        EObject lv_assignments_12_0 = null;

        EObject lv_assignments_14_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:2112:2: ( ( ( ( () otherlv_1= 'new' ( (otherlv_2= RULE_ID ) ) ) | ( () otherlv_4= 'new' ( (otherlv_5= RULE_ID ) ) otherlv_6= '<' ( ( (lv_arguments_7_0= ruleAyinType ) ) (otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) ) )* )? otherlv_10= '>' ) ) (otherlv_11= '(' ( ( (lv_assignments_12_0= ruleAyinArgumentAssignment ) ) (otherlv_13= ',' ( (lv_assignments_14_0= ruleAyinArgumentAssignment ) ) )* )? otherlv_15= ')' )? ) )
            // InternalAyin.g:2113:2: ( ( ( () otherlv_1= 'new' ( (otherlv_2= RULE_ID ) ) ) | ( () otherlv_4= 'new' ( (otherlv_5= RULE_ID ) ) otherlv_6= '<' ( ( (lv_arguments_7_0= ruleAyinType ) ) (otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) ) )* )? otherlv_10= '>' ) ) (otherlv_11= '(' ( ( (lv_assignments_12_0= ruleAyinArgumentAssignment ) ) (otherlv_13= ',' ( (lv_assignments_14_0= ruleAyinArgumentAssignment ) ) )* )? otherlv_15= ')' )? )
            {
            // InternalAyin.g:2113:2: ( ( ( () otherlv_1= 'new' ( (otherlv_2= RULE_ID ) ) ) | ( () otherlv_4= 'new' ( (otherlv_5= RULE_ID ) ) otherlv_6= '<' ( ( (lv_arguments_7_0= ruleAyinType ) ) (otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) ) )* )? otherlv_10= '>' ) ) (otherlv_11= '(' ( ( (lv_assignments_12_0= ruleAyinArgumentAssignment ) ) (otherlv_13= ',' ( (lv_assignments_14_0= ruleAyinArgumentAssignment ) ) )* )? otherlv_15= ')' )? )
            // InternalAyin.g:2114:3: ( ( () otherlv_1= 'new' ( (otherlv_2= RULE_ID ) ) ) | ( () otherlv_4= 'new' ( (otherlv_5= RULE_ID ) ) otherlv_6= '<' ( ( (lv_arguments_7_0= ruleAyinType ) ) (otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) ) )* )? otherlv_10= '>' ) ) (otherlv_11= '(' ( ( (lv_assignments_12_0= ruleAyinArgumentAssignment ) ) (otherlv_13= ',' ( (lv_assignments_14_0= ruleAyinArgumentAssignment ) ) )* )? otherlv_15= ')' )?
            {
            // InternalAyin.g:2114:3: ( ( () otherlv_1= 'new' ( (otherlv_2= RULE_ID ) ) ) | ( () otherlv_4= 'new' ( (otherlv_5= RULE_ID ) ) otherlv_6= '<' ( ( (lv_arguments_7_0= ruleAyinType ) ) (otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) ) )* )? otherlv_10= '>' ) )
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==44) ) {
                int LA42_1 = input.LA(2);

                if ( (LA42_1==RULE_ID) ) {
                    int LA42_2 = input.LA(3);

                    if ( (LA42_2==EOF||LA42_2==14||LA42_2==25||(LA42_2>=30 && LA42_2<=31)) ) {
                        alt42=1;
                    }
                    else if ( (LA42_2==27) ) {
                        alt42=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 42, 2, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 42, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 42, 0, input);

                throw nvae;
            }
            switch (alt42) {
                case 1 :
                    // InternalAyin.g:2115:4: ( () otherlv_1= 'new' ( (otherlv_2= RULE_ID ) ) )
                    {
                    // InternalAyin.g:2115:4: ( () otherlv_1= 'new' ( (otherlv_2= RULE_ID ) ) )
                    // InternalAyin.g:2116:5: () otherlv_1= 'new' ( (otherlv_2= RULE_ID ) )
                    {
                    // InternalAyin.g:2116:5: ()
                    // InternalAyin.g:2117:6: 
                    {

                    						current = forceCreateModelElement(
                    							grammarAccess.getAyinNewOperatorAccess().getAyinStructInstantionAction_0_0_0(),
                    							current);
                    					

                    }

                    otherlv_1=(Token)match(input,44,FOLLOW_5); 

                    					newLeafNode(otherlv_1, grammarAccess.getAyinNewOperatorAccess().getNewKeyword_0_0_1());
                    				
                    // InternalAyin.g:2127:5: ( (otherlv_2= RULE_ID ) )
                    // InternalAyin.g:2128:6: (otherlv_2= RULE_ID )
                    {
                    // InternalAyin.g:2128:6: (otherlv_2= RULE_ID )
                    // InternalAyin.g:2129:7: otherlv_2= RULE_ID
                    {

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getAyinNewOperatorRule());
                    							}
                    						
                    otherlv_2=(Token)match(input,RULE_ID,FOLLOW_39); 

                    							newLeafNode(otherlv_2, grammarAccess.getAyinNewOperatorAccess().getEntityAyinEntityCrossReference_0_0_2_0());
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAyin.g:2142:4: ( () otherlv_4= 'new' ( (otherlv_5= RULE_ID ) ) otherlv_6= '<' ( ( (lv_arguments_7_0= ruleAyinType ) ) (otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) ) )* )? otherlv_10= '>' )
                    {
                    // InternalAyin.g:2142:4: ( () otherlv_4= 'new' ( (otherlv_5= RULE_ID ) ) otherlv_6= '<' ( ( (lv_arguments_7_0= ruleAyinType ) ) (otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) ) )* )? otherlv_10= '>' )
                    // InternalAyin.g:2143:5: () otherlv_4= 'new' ( (otherlv_5= RULE_ID ) ) otherlv_6= '<' ( ( (lv_arguments_7_0= ruleAyinType ) ) (otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) ) )* )? otherlv_10= '>'
                    {
                    // InternalAyin.g:2143:5: ()
                    // InternalAyin.g:2144:6: 
                    {

                    						current = forceCreateModelElement(
                    							grammarAccess.getAyinNewOperatorAccess().getAyinDataTypeInstantionAction_0_1_0(),
                    							current);
                    					

                    }

                    otherlv_4=(Token)match(input,44,FOLLOW_5); 

                    					newLeafNode(otherlv_4, grammarAccess.getAyinNewOperatorAccess().getNewKeyword_0_1_1());
                    				
                    // InternalAyin.g:2154:5: ( (otherlv_5= RULE_ID ) )
                    // InternalAyin.g:2155:6: (otherlv_5= RULE_ID )
                    {
                    // InternalAyin.g:2155:6: (otherlv_5= RULE_ID )
                    // InternalAyin.g:2156:7: otherlv_5= RULE_ID
                    {

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getAyinNewOperatorRule());
                    							}
                    						
                    otherlv_5=(Token)match(input,RULE_ID,FOLLOW_16); 

                    							newLeafNode(otherlv_5, grammarAccess.getAyinNewOperatorAccess().getEntityAyinEntityCrossReference_0_1_2_0());
                    						

                    }


                    }

                    otherlv_6=(Token)match(input,27,FOLLOW_40); 

                    					newLeafNode(otherlv_6, grammarAccess.getAyinNewOperatorAccess().getLessThanSignKeyword_0_1_3());
                    				
                    // InternalAyin.g:2171:5: ( ( (lv_arguments_7_0= ruleAyinType ) ) (otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) ) )* )?
                    int alt41=2;
                    int LA41_0 = input.LA(1);

                    if ( (LA41_0==RULE_ID||(LA41_0>=47 && LA41_0<=53)) ) {
                        alt41=1;
                    }
                    switch (alt41) {
                        case 1 :
                            // InternalAyin.g:2172:6: ( (lv_arguments_7_0= ruleAyinType ) ) (otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) ) )*
                            {
                            // InternalAyin.g:2172:6: ( (lv_arguments_7_0= ruleAyinType ) )
                            // InternalAyin.g:2173:7: (lv_arguments_7_0= ruleAyinType )
                            {
                            // InternalAyin.g:2173:7: (lv_arguments_7_0= ruleAyinType )
                            // InternalAyin.g:2174:8: lv_arguments_7_0= ruleAyinType
                            {

                            								newCompositeNode(grammarAccess.getAyinNewOperatorAccess().getArgumentsAyinTypeParserRuleCall_0_1_4_0_0());
                            							
                            pushFollow(FOLLOW_18);
                            lv_arguments_7_0=ruleAyinType();

                            state._fsp--;


                            								if (current==null) {
                            									current = createModelElementForParent(grammarAccess.getAyinNewOperatorRule());
                            								}
                            								add(
                            									current,
                            									"arguments",
                            									lv_arguments_7_0,
                            									"space.ayin.lang.Ayin.AyinType");
                            								afterParserOrEnumRuleCall();
                            							

                            }


                            }

                            // InternalAyin.g:2191:6: (otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) ) )*
                            loop40:
                            do {
                                int alt40=2;
                                int LA40_0 = input.LA(1);

                                if ( (LA40_0==25) ) {
                                    alt40=1;
                                }


                                switch (alt40) {
                            	case 1 :
                            	    // InternalAyin.g:2192:7: otherlv_8= ',' ( (lv_arguments_9_0= ruleAyinType ) )
                            	    {
                            	    otherlv_8=(Token)match(input,25,FOLLOW_28); 

                            	    							newLeafNode(otherlv_8, grammarAccess.getAyinNewOperatorAccess().getCommaKeyword_0_1_4_1_0());
                            	    						
                            	    // InternalAyin.g:2196:7: ( (lv_arguments_9_0= ruleAyinType ) )
                            	    // InternalAyin.g:2197:8: (lv_arguments_9_0= ruleAyinType )
                            	    {
                            	    // InternalAyin.g:2197:8: (lv_arguments_9_0= ruleAyinType )
                            	    // InternalAyin.g:2198:9: lv_arguments_9_0= ruleAyinType
                            	    {

                            	    									newCompositeNode(grammarAccess.getAyinNewOperatorAccess().getArgumentsAyinTypeParserRuleCall_0_1_4_1_1_0());
                            	    								
                            	    pushFollow(FOLLOW_18);
                            	    lv_arguments_9_0=ruleAyinType();

                            	    state._fsp--;


                            	    									if (current==null) {
                            	    										current = createModelElementForParent(grammarAccess.getAyinNewOperatorRule());
                            	    									}
                            	    									add(
                            	    										current,
                            	    										"arguments",
                            	    										lv_arguments_9_0,
                            	    										"space.ayin.lang.Ayin.AyinType");
                            	    									afterParserOrEnumRuleCall();
                            	    								

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop40;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_10=(Token)match(input,28,FOLLOW_39); 

                    					newLeafNode(otherlv_10, grammarAccess.getAyinNewOperatorAccess().getGreaterThanSignKeyword_0_1_5());
                    				

                    }


                    }
                    break;

            }

            // InternalAyin.g:2223:3: (otherlv_11= '(' ( ( (lv_assignments_12_0= ruleAyinArgumentAssignment ) ) (otherlv_13= ',' ( (lv_assignments_14_0= ruleAyinArgumentAssignment ) ) )* )? otherlv_15= ')' )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==30) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalAyin.g:2224:4: otherlv_11= '(' ( ( (lv_assignments_12_0= ruleAyinArgumentAssignment ) ) (otherlv_13= ',' ( (lv_assignments_14_0= ruleAyinArgumentAssignment ) ) )* )? otherlv_15= ')'
                    {
                    otherlv_11=(Token)match(input,30,FOLLOW_32); 

                    				newLeafNode(otherlv_11, grammarAccess.getAyinNewOperatorAccess().getLeftParenthesisKeyword_1_0());
                    			
                    // InternalAyin.g:2228:4: ( ( (lv_assignments_12_0= ruleAyinArgumentAssignment ) ) (otherlv_13= ',' ( (lv_assignments_14_0= ruleAyinArgumentAssignment ) ) )* )?
                    int alt44=2;
                    int LA44_0 = input.LA(1);

                    if ( (LA44_0==RULE_ID||LA44_0==45) ) {
                        alt44=1;
                    }
                    switch (alt44) {
                        case 1 :
                            // InternalAyin.g:2229:5: ( (lv_assignments_12_0= ruleAyinArgumentAssignment ) ) (otherlv_13= ',' ( (lv_assignments_14_0= ruleAyinArgumentAssignment ) ) )*
                            {
                            // InternalAyin.g:2229:5: ( (lv_assignments_12_0= ruleAyinArgumentAssignment ) )
                            // InternalAyin.g:2230:6: (lv_assignments_12_0= ruleAyinArgumentAssignment )
                            {
                            // InternalAyin.g:2230:6: (lv_assignments_12_0= ruleAyinArgumentAssignment )
                            // InternalAyin.g:2231:7: lv_assignments_12_0= ruleAyinArgumentAssignment
                            {

                            							newCompositeNode(grammarAccess.getAyinNewOperatorAccess().getAssignmentsAyinArgumentAssignmentParserRuleCall_1_1_0_0());
                            						
                            pushFollow(FOLLOW_23);
                            lv_assignments_12_0=ruleAyinArgumentAssignment();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getAyinNewOperatorRule());
                            							}
                            							add(
                            								current,
                            								"assignments",
                            								lv_assignments_12_0,
                            								"space.ayin.lang.Ayin.AyinArgumentAssignment");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }

                            // InternalAyin.g:2248:5: (otherlv_13= ',' ( (lv_assignments_14_0= ruleAyinArgumentAssignment ) ) )*
                            loop43:
                            do {
                                int alt43=2;
                                int LA43_0 = input.LA(1);

                                if ( (LA43_0==25) ) {
                                    alt43=1;
                                }


                                switch (alt43) {
                            	case 1 :
                            	    // InternalAyin.g:2249:6: otherlv_13= ',' ( (lv_assignments_14_0= ruleAyinArgumentAssignment ) )
                            	    {
                            	    otherlv_13=(Token)match(input,25,FOLLOW_33); 

                            	    						newLeafNode(otherlv_13, grammarAccess.getAyinNewOperatorAccess().getCommaKeyword_1_1_1_0());
                            	    					
                            	    // InternalAyin.g:2253:6: ( (lv_assignments_14_0= ruleAyinArgumentAssignment ) )
                            	    // InternalAyin.g:2254:7: (lv_assignments_14_0= ruleAyinArgumentAssignment )
                            	    {
                            	    // InternalAyin.g:2254:7: (lv_assignments_14_0= ruleAyinArgumentAssignment )
                            	    // InternalAyin.g:2255:8: lv_assignments_14_0= ruleAyinArgumentAssignment
                            	    {

                            	    								newCompositeNode(grammarAccess.getAyinNewOperatorAccess().getAssignmentsAyinArgumentAssignmentParserRuleCall_1_1_1_1_0());
                            	    							
                            	    pushFollow(FOLLOW_23);
                            	    lv_assignments_14_0=ruleAyinArgumentAssignment();

                            	    state._fsp--;


                            	    								if (current==null) {
                            	    									current = createModelElementForParent(grammarAccess.getAyinNewOperatorRule());
                            	    								}
                            	    								add(
                            	    									current,
                            	    									"assignments",
                            	    									lv_assignments_14_0,
                            	    									"space.ayin.lang.Ayin.AyinArgumentAssignment");
                            	    								afterParserOrEnumRuleCall();
                            	    							

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop43;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_15=(Token)match(input,31,FOLLOW_2); 

                    				newLeafNode(otherlv_15, grammarAccess.getAyinNewOperatorAccess().getRightParenthesisKeyword_1_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinNewOperator"


    // $ANTLR start "entryRuleAyinAtomValue"
    // InternalAyin.g:2283:1: entryRuleAyinAtomValue returns [EObject current=null] : iv_ruleAyinAtomValue= ruleAyinAtomValue EOF ;
    public final EObject entryRuleAyinAtomValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinAtomValue = null;


        try {
            // InternalAyin.g:2283:54: (iv_ruleAyinAtomValue= ruleAyinAtomValue EOF )
            // InternalAyin.g:2284:2: iv_ruleAyinAtomValue= ruleAyinAtomValue EOF
            {
             newCompositeNode(grammarAccess.getAyinAtomValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinAtomValue=ruleAyinAtomValue();

            state._fsp--;

             current =iv_ruleAyinAtomValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinAtomValue"


    // $ANTLR start "ruleAyinAtomValue"
    // InternalAyin.g:2290:1: ruleAyinAtomValue returns [EObject current=null] : ( ( () ( (lv_value_1_0= RULE_INTEGER ) ) ) | ( () ( (lv_value_3_0= RULE_DOUBLE ) ) ) | ( () ( (lv_value_5_0= RULE_STRING ) ) ) | ( () ( (lv_value_7_0= RULE_BOOL ) ) ) ) ;
    public final EObject ruleAyinAtomValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token lv_value_3_0=null;
        Token lv_value_5_0=null;
        Token lv_value_7_0=null;


        	enterRule();

        try {
            // InternalAyin.g:2296:2: ( ( ( () ( (lv_value_1_0= RULE_INTEGER ) ) ) | ( () ( (lv_value_3_0= RULE_DOUBLE ) ) ) | ( () ( (lv_value_5_0= RULE_STRING ) ) ) | ( () ( (lv_value_7_0= RULE_BOOL ) ) ) ) )
            // InternalAyin.g:2297:2: ( ( () ( (lv_value_1_0= RULE_INTEGER ) ) ) | ( () ( (lv_value_3_0= RULE_DOUBLE ) ) ) | ( () ( (lv_value_5_0= RULE_STRING ) ) ) | ( () ( (lv_value_7_0= RULE_BOOL ) ) ) )
            {
            // InternalAyin.g:2297:2: ( ( () ( (lv_value_1_0= RULE_INTEGER ) ) ) | ( () ( (lv_value_3_0= RULE_DOUBLE ) ) ) | ( () ( (lv_value_5_0= RULE_STRING ) ) ) | ( () ( (lv_value_7_0= RULE_BOOL ) ) ) )
            int alt46=4;
            switch ( input.LA(1) ) {
            case RULE_INTEGER:
                {
                alt46=1;
                }
                break;
            case RULE_DOUBLE:
                {
                alt46=2;
                }
                break;
            case RULE_STRING:
                {
                alt46=3;
                }
                break;
            case RULE_BOOL:
                {
                alt46=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 46, 0, input);

                throw nvae;
            }

            switch (alt46) {
                case 1 :
                    // InternalAyin.g:2298:3: ( () ( (lv_value_1_0= RULE_INTEGER ) ) )
                    {
                    // InternalAyin.g:2298:3: ( () ( (lv_value_1_0= RULE_INTEGER ) ) )
                    // InternalAyin.g:2299:4: () ( (lv_value_1_0= RULE_INTEGER ) )
                    {
                    // InternalAyin.g:2299:4: ()
                    // InternalAyin.g:2300:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAyinAtomValueAccess().getAyinIntegerValueAction_0_0(),
                    						current);
                    				

                    }

                    // InternalAyin.g:2306:4: ( (lv_value_1_0= RULE_INTEGER ) )
                    // InternalAyin.g:2307:5: (lv_value_1_0= RULE_INTEGER )
                    {
                    // InternalAyin.g:2307:5: (lv_value_1_0= RULE_INTEGER )
                    // InternalAyin.g:2308:6: lv_value_1_0= RULE_INTEGER
                    {
                    lv_value_1_0=(Token)match(input,RULE_INTEGER,FOLLOW_2); 

                    						newLeafNode(lv_value_1_0, grammarAccess.getAyinAtomValueAccess().getValueINTEGERTerminalRuleCall_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAyinAtomValueRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_1_0,
                    							"space.ayin.lang.Ayin.INTEGER");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalAyin.g:2326:3: ( () ( (lv_value_3_0= RULE_DOUBLE ) ) )
                    {
                    // InternalAyin.g:2326:3: ( () ( (lv_value_3_0= RULE_DOUBLE ) ) )
                    // InternalAyin.g:2327:4: () ( (lv_value_3_0= RULE_DOUBLE ) )
                    {
                    // InternalAyin.g:2327:4: ()
                    // InternalAyin.g:2328:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAyinAtomValueAccess().getAyinDoubleValueAction_1_0(),
                    						current);
                    				

                    }

                    // InternalAyin.g:2334:4: ( (lv_value_3_0= RULE_DOUBLE ) )
                    // InternalAyin.g:2335:5: (lv_value_3_0= RULE_DOUBLE )
                    {
                    // InternalAyin.g:2335:5: (lv_value_3_0= RULE_DOUBLE )
                    // InternalAyin.g:2336:6: lv_value_3_0= RULE_DOUBLE
                    {
                    lv_value_3_0=(Token)match(input,RULE_DOUBLE,FOLLOW_2); 

                    						newLeafNode(lv_value_3_0, grammarAccess.getAyinAtomValueAccess().getValueDOUBLETerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAyinAtomValueRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_3_0,
                    							"space.ayin.lang.Ayin.DOUBLE");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalAyin.g:2354:3: ( () ( (lv_value_5_0= RULE_STRING ) ) )
                    {
                    // InternalAyin.g:2354:3: ( () ( (lv_value_5_0= RULE_STRING ) ) )
                    // InternalAyin.g:2355:4: () ( (lv_value_5_0= RULE_STRING ) )
                    {
                    // InternalAyin.g:2355:4: ()
                    // InternalAyin.g:2356:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAyinAtomValueAccess().getAyinStringValueAction_2_0(),
                    						current);
                    				

                    }

                    // InternalAyin.g:2362:4: ( (lv_value_5_0= RULE_STRING ) )
                    // InternalAyin.g:2363:5: (lv_value_5_0= RULE_STRING )
                    {
                    // InternalAyin.g:2363:5: (lv_value_5_0= RULE_STRING )
                    // InternalAyin.g:2364:6: lv_value_5_0= RULE_STRING
                    {
                    lv_value_5_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    						newLeafNode(lv_value_5_0, grammarAccess.getAyinAtomValueAccess().getValueSTRINGTerminalRuleCall_2_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAyinAtomValueRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_5_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalAyin.g:2382:3: ( () ( (lv_value_7_0= RULE_BOOL ) ) )
                    {
                    // InternalAyin.g:2382:3: ( () ( (lv_value_7_0= RULE_BOOL ) ) )
                    // InternalAyin.g:2383:4: () ( (lv_value_7_0= RULE_BOOL ) )
                    {
                    // InternalAyin.g:2383:4: ()
                    // InternalAyin.g:2384:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAyinAtomValueAccess().getAyinBooleanValueAction_3_0(),
                    						current);
                    				

                    }

                    // InternalAyin.g:2390:4: ( (lv_value_7_0= RULE_BOOL ) )
                    // InternalAyin.g:2391:5: (lv_value_7_0= RULE_BOOL )
                    {
                    // InternalAyin.g:2391:5: (lv_value_7_0= RULE_BOOL )
                    // InternalAyin.g:2392:6: lv_value_7_0= RULE_BOOL
                    {
                    lv_value_7_0=(Token)match(input,RULE_BOOL,FOLLOW_2); 

                    						newLeafNode(lv_value_7_0, grammarAccess.getAyinAtomValueAccess().getValueBOOLTerminalRuleCall_3_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAyinAtomValueRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_7_0,
                    							"space.ayin.lang.Ayin.BOOL");
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinAtomValue"


    // $ANTLR start "entryRuleAyinArgumentAssignment"
    // InternalAyin.g:2413:1: entryRuleAyinArgumentAssignment returns [EObject current=null] : iv_ruleAyinArgumentAssignment= ruleAyinArgumentAssignment EOF ;
    public final EObject entryRuleAyinArgumentAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinArgumentAssignment = null;


        try {
            // InternalAyin.g:2413:63: (iv_ruleAyinArgumentAssignment= ruleAyinArgumentAssignment EOF )
            // InternalAyin.g:2414:2: iv_ruleAyinArgumentAssignment= ruleAyinArgumentAssignment EOF
            {
             newCompositeNode(grammarAccess.getAyinArgumentAssignmentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinArgumentAssignment=ruleAyinArgumentAssignment();

            state._fsp--;

             current =iv_ruleAyinArgumentAssignment; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinArgumentAssignment"


    // $ANTLR start "ruleAyinArgumentAssignment"
    // InternalAyin.g:2420:1: ruleAyinArgumentAssignment returns [EObject current=null] : ( ( ( (lv_left_0_1= ruleAyinRef | lv_left_0_2= ruleAyinUnderscore ) ) ) (otherlv_1= ',' ( ( (lv_left_2_1= ruleAyinRef | lv_left_2_2= ruleAyinUnderscore ) ) ) )* otherlv_3= '=' ( (lv_right_4_0= ruleAyinValueExpression ) ) ) ;
    public final EObject ruleAyinArgumentAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_left_0_1 = null;

        EObject lv_left_0_2 = null;

        EObject lv_left_2_1 = null;

        EObject lv_left_2_2 = null;

        EObject lv_right_4_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:2426:2: ( ( ( ( (lv_left_0_1= ruleAyinRef | lv_left_0_2= ruleAyinUnderscore ) ) ) (otherlv_1= ',' ( ( (lv_left_2_1= ruleAyinRef | lv_left_2_2= ruleAyinUnderscore ) ) ) )* otherlv_3= '=' ( (lv_right_4_0= ruleAyinValueExpression ) ) ) )
            // InternalAyin.g:2427:2: ( ( ( (lv_left_0_1= ruleAyinRef | lv_left_0_2= ruleAyinUnderscore ) ) ) (otherlv_1= ',' ( ( (lv_left_2_1= ruleAyinRef | lv_left_2_2= ruleAyinUnderscore ) ) ) )* otherlv_3= '=' ( (lv_right_4_0= ruleAyinValueExpression ) ) )
            {
            // InternalAyin.g:2427:2: ( ( ( (lv_left_0_1= ruleAyinRef | lv_left_0_2= ruleAyinUnderscore ) ) ) (otherlv_1= ',' ( ( (lv_left_2_1= ruleAyinRef | lv_left_2_2= ruleAyinUnderscore ) ) ) )* otherlv_3= '=' ( (lv_right_4_0= ruleAyinValueExpression ) ) )
            // InternalAyin.g:2428:3: ( ( (lv_left_0_1= ruleAyinRef | lv_left_0_2= ruleAyinUnderscore ) ) ) (otherlv_1= ',' ( ( (lv_left_2_1= ruleAyinRef | lv_left_2_2= ruleAyinUnderscore ) ) ) )* otherlv_3= '=' ( (lv_right_4_0= ruleAyinValueExpression ) )
            {
            // InternalAyin.g:2428:3: ( ( (lv_left_0_1= ruleAyinRef | lv_left_0_2= ruleAyinUnderscore ) ) )
            // InternalAyin.g:2429:4: ( (lv_left_0_1= ruleAyinRef | lv_left_0_2= ruleAyinUnderscore ) )
            {
            // InternalAyin.g:2429:4: ( (lv_left_0_1= ruleAyinRef | lv_left_0_2= ruleAyinUnderscore ) )
            // InternalAyin.g:2430:5: (lv_left_0_1= ruleAyinRef | lv_left_0_2= ruleAyinUnderscore )
            {
            // InternalAyin.g:2430:5: (lv_left_0_1= ruleAyinRef | lv_left_0_2= ruleAyinUnderscore )
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==RULE_ID) ) {
                alt47=1;
            }
            else if ( (LA47_0==45) ) {
                alt47=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 47, 0, input);

                throw nvae;
            }
            switch (alt47) {
                case 1 :
                    // InternalAyin.g:2431:6: lv_left_0_1= ruleAyinRef
                    {

                    						newCompositeNode(grammarAccess.getAyinArgumentAssignmentAccess().getLeftAyinRefParserRuleCall_0_0_0());
                    					
                    pushFollow(FOLLOW_41);
                    lv_left_0_1=ruleAyinRef();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinArgumentAssignmentRule());
                    						}
                    						add(
                    							current,
                    							"left",
                    							lv_left_0_1,
                    							"space.ayin.lang.Ayin.AyinRef");
                    						afterParserOrEnumRuleCall();
                    					

                    }
                    break;
                case 2 :
                    // InternalAyin.g:2447:6: lv_left_0_2= ruleAyinUnderscore
                    {

                    						newCompositeNode(grammarAccess.getAyinArgumentAssignmentAccess().getLeftAyinUnderscoreParserRuleCall_0_0_1());
                    					
                    pushFollow(FOLLOW_41);
                    lv_left_0_2=ruleAyinUnderscore();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinArgumentAssignmentRule());
                    						}
                    						add(
                    							current,
                    							"left",
                    							lv_left_0_2,
                    							"space.ayin.lang.Ayin.AyinUnderscore");
                    						afterParserOrEnumRuleCall();
                    					

                    }
                    break;

            }


            }


            }

            // InternalAyin.g:2465:3: (otherlv_1= ',' ( ( (lv_left_2_1= ruleAyinRef | lv_left_2_2= ruleAyinUnderscore ) ) ) )*
            loop49:
            do {
                int alt49=2;
                int LA49_0 = input.LA(1);

                if ( (LA49_0==25) ) {
                    alt49=1;
                }


                switch (alt49) {
            	case 1 :
            	    // InternalAyin.g:2466:4: otherlv_1= ',' ( ( (lv_left_2_1= ruleAyinRef | lv_left_2_2= ruleAyinUnderscore ) ) )
            	    {
            	    otherlv_1=(Token)match(input,25,FOLLOW_33); 

            	    				newLeafNode(otherlv_1, grammarAccess.getAyinArgumentAssignmentAccess().getCommaKeyword_1_0());
            	    			
            	    // InternalAyin.g:2470:4: ( ( (lv_left_2_1= ruleAyinRef | lv_left_2_2= ruleAyinUnderscore ) ) )
            	    // InternalAyin.g:2471:5: ( (lv_left_2_1= ruleAyinRef | lv_left_2_2= ruleAyinUnderscore ) )
            	    {
            	    // InternalAyin.g:2471:5: ( (lv_left_2_1= ruleAyinRef | lv_left_2_2= ruleAyinUnderscore ) )
            	    // InternalAyin.g:2472:6: (lv_left_2_1= ruleAyinRef | lv_left_2_2= ruleAyinUnderscore )
            	    {
            	    // InternalAyin.g:2472:6: (lv_left_2_1= ruleAyinRef | lv_left_2_2= ruleAyinUnderscore )
            	    int alt48=2;
            	    int LA48_0 = input.LA(1);

            	    if ( (LA48_0==RULE_ID) ) {
            	        alt48=1;
            	    }
            	    else if ( (LA48_0==45) ) {
            	        alt48=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 48, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt48) {
            	        case 1 :
            	            // InternalAyin.g:2473:7: lv_left_2_1= ruleAyinRef
            	            {

            	            							newCompositeNode(grammarAccess.getAyinArgumentAssignmentAccess().getLeftAyinRefParserRuleCall_1_1_0_0());
            	            						
            	            pushFollow(FOLLOW_41);
            	            lv_left_2_1=ruleAyinRef();

            	            state._fsp--;


            	            							if (current==null) {
            	            								current = createModelElementForParent(grammarAccess.getAyinArgumentAssignmentRule());
            	            							}
            	            							add(
            	            								current,
            	            								"left",
            	            								lv_left_2_1,
            	            								"space.ayin.lang.Ayin.AyinRef");
            	            							afterParserOrEnumRuleCall();
            	            						

            	            }
            	            break;
            	        case 2 :
            	            // InternalAyin.g:2489:7: lv_left_2_2= ruleAyinUnderscore
            	            {

            	            							newCompositeNode(grammarAccess.getAyinArgumentAssignmentAccess().getLeftAyinUnderscoreParserRuleCall_1_1_0_1());
            	            						
            	            pushFollow(FOLLOW_41);
            	            lv_left_2_2=ruleAyinUnderscore();

            	            state._fsp--;


            	            							if (current==null) {
            	            								current = createModelElementForParent(grammarAccess.getAyinArgumentAssignmentRule());
            	            							}
            	            							add(
            	            								current,
            	            								"left",
            	            								lv_left_2_2,
            	            								"space.ayin.lang.Ayin.AyinUnderscore");
            	            							afterParserOrEnumRuleCall();
            	            						

            	            }
            	            break;

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop49;
                }
            } while (true);

            otherlv_3=(Token)match(input,35,FOLLOW_34); 

            			newLeafNode(otherlv_3, grammarAccess.getAyinArgumentAssignmentAccess().getEqualsSignKeyword_2());
            		
            // InternalAyin.g:2512:3: ( (lv_right_4_0= ruleAyinValueExpression ) )
            // InternalAyin.g:2513:4: (lv_right_4_0= ruleAyinValueExpression )
            {
            // InternalAyin.g:2513:4: (lv_right_4_0= ruleAyinValueExpression )
            // InternalAyin.g:2514:5: lv_right_4_0= ruleAyinValueExpression
            {

            					newCompositeNode(grammarAccess.getAyinArgumentAssignmentAccess().getRightAyinValueExpressionParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_right_4_0=ruleAyinValueExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAyinArgumentAssignmentRule());
            					}
            					set(
            						current,
            						"right",
            						lv_right_4_0,
            						"space.ayin.lang.Ayin.AyinValueExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinArgumentAssignment"


    // $ANTLR start "entryRuleAyinAssignment"
    // InternalAyin.g:2535:1: entryRuleAyinAssignment returns [EObject current=null] : iv_ruleAyinAssignment= ruleAyinAssignment EOF ;
    public final EObject entryRuleAyinAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinAssignment = null;


        try {
            // InternalAyin.g:2535:55: (iv_ruleAyinAssignment= ruleAyinAssignment EOF )
            // InternalAyin.g:2536:2: iv_ruleAyinAssignment= ruleAyinAssignment EOF
            {
             newCompositeNode(grammarAccess.getAyinAssignmentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinAssignment=ruleAyinAssignment();

            state._fsp--;

             current =iv_ruleAyinAssignment; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinAssignment"


    // $ANTLR start "ruleAyinAssignment"
    // InternalAyin.g:2542:1: ruleAyinAssignment returns [EObject current=null] : ( (this_AyinFieldGet_0= ruleAyinFieldGet | this_AyinParameter_1= ruleAyinParameter | this_AyinUnderscore_2= ruleAyinUnderscore ) ( () (otherlv_4= ',' ( ( (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore ) ) ) )* otherlv_6= '=' ( (lv_right_7_0= ruleAyinValueExpression ) ) )? otherlv_8= ';' ) ;
    public final EObject ruleAyinAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject this_AyinFieldGet_0 = null;

        EObject this_AyinParameter_1 = null;

        EObject this_AyinUnderscore_2 = null;

        EObject lv_left_5_1 = null;

        EObject lv_left_5_2 = null;

        EObject lv_left_5_3 = null;

        EObject lv_right_7_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:2548:2: ( ( (this_AyinFieldGet_0= ruleAyinFieldGet | this_AyinParameter_1= ruleAyinParameter | this_AyinUnderscore_2= ruleAyinUnderscore ) ( () (otherlv_4= ',' ( ( (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore ) ) ) )* otherlv_6= '=' ( (lv_right_7_0= ruleAyinValueExpression ) ) )? otherlv_8= ';' ) )
            // InternalAyin.g:2549:2: ( (this_AyinFieldGet_0= ruleAyinFieldGet | this_AyinParameter_1= ruleAyinParameter | this_AyinUnderscore_2= ruleAyinUnderscore ) ( () (otherlv_4= ',' ( ( (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore ) ) ) )* otherlv_6= '=' ( (lv_right_7_0= ruleAyinValueExpression ) ) )? otherlv_8= ';' )
            {
            // InternalAyin.g:2549:2: ( (this_AyinFieldGet_0= ruleAyinFieldGet | this_AyinParameter_1= ruleAyinParameter | this_AyinUnderscore_2= ruleAyinUnderscore ) ( () (otherlv_4= ',' ( ( (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore ) ) ) )* otherlv_6= '=' ( (lv_right_7_0= ruleAyinValueExpression ) ) )? otherlv_8= ';' )
            // InternalAyin.g:2550:3: (this_AyinFieldGet_0= ruleAyinFieldGet | this_AyinParameter_1= ruleAyinParameter | this_AyinUnderscore_2= ruleAyinUnderscore ) ( () (otherlv_4= ',' ( ( (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore ) ) ) )* otherlv_6= '=' ( (lv_right_7_0= ruleAyinValueExpression ) ) )? otherlv_8= ';'
            {
            // InternalAyin.g:2550:3: (this_AyinFieldGet_0= ruleAyinFieldGet | this_AyinParameter_1= ruleAyinParameter | this_AyinUnderscore_2= ruleAyinUnderscore )
            int alt50=3;
            switch ( input.LA(1) ) {
            case 36:
                {
                alt50=1;
                }
                break;
            case RULE_ID:
                {
                int LA50_2 = input.LA(2);

                if ( (LA50_2==RULE_ID||LA50_2==27) ) {
                    alt50=2;
                }
                else if ( ((LA50_2>=14 && LA50_2<=15)||LA50_2==25||LA50_2==35) ) {
                    alt50=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 50, 2, input);

                    throw nvae;
                }
                }
                break;
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
                {
                alt50=2;
                }
                break;
            case 45:
                {
                alt50=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;
            }

            switch (alt50) {
                case 1 :
                    // InternalAyin.g:2551:4: this_AyinFieldGet_0= ruleAyinFieldGet
                    {

                    				newCompositeNode(grammarAccess.getAyinAssignmentAccess().getAyinFieldGetParserRuleCall_0_0());
                    			
                    pushFollow(FOLLOW_42);
                    this_AyinFieldGet_0=ruleAyinFieldGet();

                    state._fsp--;


                    				current = this_AyinFieldGet_0;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;
                case 2 :
                    // InternalAyin.g:2560:4: this_AyinParameter_1= ruleAyinParameter
                    {

                    				newCompositeNode(grammarAccess.getAyinAssignmentAccess().getAyinParameterParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_42);
                    this_AyinParameter_1=ruleAyinParameter();

                    state._fsp--;


                    				current = this_AyinParameter_1;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;
                case 3 :
                    // InternalAyin.g:2569:4: this_AyinUnderscore_2= ruleAyinUnderscore
                    {

                    				newCompositeNode(grammarAccess.getAyinAssignmentAccess().getAyinUnderscoreParserRuleCall_0_2());
                    			
                    pushFollow(FOLLOW_42);
                    this_AyinUnderscore_2=ruleAyinUnderscore();

                    state._fsp--;


                    				current = this_AyinUnderscore_2;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalAyin.g:2578:3: ( () (otherlv_4= ',' ( ( (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore ) ) ) )* otherlv_6= '=' ( (lv_right_7_0= ruleAyinValueExpression ) ) )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==25||LA53_0==35) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // InternalAyin.g:2579:4: () (otherlv_4= ',' ( ( (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore ) ) ) )* otherlv_6= '=' ( (lv_right_7_0= ruleAyinValueExpression ) )
                    {
                    // InternalAyin.g:2579:4: ()
                    // InternalAyin.g:2580:5: 
                    {

                    					current = forceCreateModelElementAndAdd(
                    						grammarAccess.getAyinAssignmentAccess().getAyinAssignmentLeftAction_1_0(),
                    						current);
                    				

                    }

                    // InternalAyin.g:2586:4: (otherlv_4= ',' ( ( (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore ) ) ) )*
                    loop52:
                    do {
                        int alt52=2;
                        int LA52_0 = input.LA(1);

                        if ( (LA52_0==25) ) {
                            alt52=1;
                        }


                        switch (alt52) {
                    	case 1 :
                    	    // InternalAyin.g:2587:5: otherlv_4= ',' ( ( (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore ) ) )
                    	    {
                    	    otherlv_4=(Token)match(input,25,FOLLOW_33); 

                    	    					newLeafNode(otherlv_4, grammarAccess.getAyinAssignmentAccess().getCommaKeyword_1_1_0());
                    	    				
                    	    // InternalAyin.g:2591:5: ( ( (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore ) ) )
                    	    // InternalAyin.g:2592:6: ( (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore ) )
                    	    {
                    	    // InternalAyin.g:2592:6: ( (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore ) )
                    	    // InternalAyin.g:2593:7: (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore )
                    	    {
                    	    // InternalAyin.g:2593:7: (lv_left_5_1= ruleAyinFieldGet | lv_left_5_2= ruleAyinParameter | lv_left_5_3= ruleAyinUnderscore )
                    	    int alt51=3;
                    	    switch ( input.LA(1) ) {
                    	    case 36:
                    	        {
                    	        alt51=1;
                    	        }
                    	        break;
                    	    case RULE_ID:
                    	        {
                    	        int LA51_2 = input.LA(2);

                    	        if ( (LA51_2==RULE_ID||LA51_2==27) ) {
                    	            alt51=2;
                    	        }
                    	        else if ( (LA51_2==15||LA51_2==25||LA51_2==35) ) {
                    	            alt51=1;
                    	        }
                    	        else {
                    	            NoViableAltException nvae =
                    	                new NoViableAltException("", 51, 2, input);

                    	            throw nvae;
                    	        }
                    	        }
                    	        break;
                    	    case 47:
                    	    case 48:
                    	    case 49:
                    	    case 50:
                    	    case 51:
                    	    case 52:
                    	    case 53:
                    	        {
                    	        alt51=2;
                    	        }
                    	        break;
                    	    case 45:
                    	        {
                    	        alt51=3;
                    	        }
                    	        break;
                    	    default:
                    	        NoViableAltException nvae =
                    	            new NoViableAltException("", 51, 0, input);

                    	        throw nvae;
                    	    }

                    	    switch (alt51) {
                    	        case 1 :
                    	            // InternalAyin.g:2594:8: lv_left_5_1= ruleAyinFieldGet
                    	            {

                    	            								newCompositeNode(grammarAccess.getAyinAssignmentAccess().getLeftAyinFieldGetParserRuleCall_1_1_1_0_0());
                    	            							
                    	            pushFollow(FOLLOW_41);
                    	            lv_left_5_1=ruleAyinFieldGet();

                    	            state._fsp--;


                    	            								if (current==null) {
                    	            									current = createModelElementForParent(grammarAccess.getAyinAssignmentRule());
                    	            								}
                    	            								add(
                    	            									current,
                    	            									"left",
                    	            									lv_left_5_1,
                    	            									"space.ayin.lang.Ayin.AyinFieldGet");
                    	            								afterParserOrEnumRuleCall();
                    	            							

                    	            }
                    	            break;
                    	        case 2 :
                    	            // InternalAyin.g:2610:8: lv_left_5_2= ruleAyinParameter
                    	            {

                    	            								newCompositeNode(grammarAccess.getAyinAssignmentAccess().getLeftAyinParameterParserRuleCall_1_1_1_0_1());
                    	            							
                    	            pushFollow(FOLLOW_41);
                    	            lv_left_5_2=ruleAyinParameter();

                    	            state._fsp--;


                    	            								if (current==null) {
                    	            									current = createModelElementForParent(grammarAccess.getAyinAssignmentRule());
                    	            								}
                    	            								add(
                    	            									current,
                    	            									"left",
                    	            									lv_left_5_2,
                    	            									"space.ayin.lang.Ayin.AyinParameter");
                    	            								afterParserOrEnumRuleCall();
                    	            							

                    	            }
                    	            break;
                    	        case 3 :
                    	            // InternalAyin.g:2626:8: lv_left_5_3= ruleAyinUnderscore
                    	            {

                    	            								newCompositeNode(grammarAccess.getAyinAssignmentAccess().getLeftAyinUnderscoreParserRuleCall_1_1_1_0_2());
                    	            							
                    	            pushFollow(FOLLOW_41);
                    	            lv_left_5_3=ruleAyinUnderscore();

                    	            state._fsp--;


                    	            								if (current==null) {
                    	            									current = createModelElementForParent(grammarAccess.getAyinAssignmentRule());
                    	            								}
                    	            								add(
                    	            									current,
                    	            									"left",
                    	            									lv_left_5_3,
                    	            									"space.ayin.lang.Ayin.AyinUnderscore");
                    	            								afterParserOrEnumRuleCall();
                    	            							

                    	            }
                    	            break;

                    	    }


                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop52;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,35,FOLLOW_34); 

                    				newLeafNode(otherlv_6, grammarAccess.getAyinAssignmentAccess().getEqualsSignKeyword_1_2());
                    			
                    // InternalAyin.g:2649:4: ( (lv_right_7_0= ruleAyinValueExpression ) )
                    // InternalAyin.g:2650:5: (lv_right_7_0= ruleAyinValueExpression )
                    {
                    // InternalAyin.g:2650:5: (lv_right_7_0= ruleAyinValueExpression )
                    // InternalAyin.g:2651:6: lv_right_7_0= ruleAyinValueExpression
                    {

                    						newCompositeNode(grammarAccess.getAyinAssignmentAccess().getRightAyinValueExpressionParserRuleCall_1_3_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_right_7_0=ruleAyinValueExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinAssignmentRule());
                    						}
                    						set(
                    							current,
                    							"right",
                    							lv_right_7_0,
                    							"space.ayin.lang.Ayin.AyinValueExpression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getAyinAssignmentAccess().getSemicolonKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinAssignment"


    // $ANTLR start "entryRuleAyinFieldGet"
    // InternalAyin.g:2677:1: entryRuleAyinFieldGet returns [EObject current=null] : iv_ruleAyinFieldGet= ruleAyinFieldGet EOF ;
    public final EObject entryRuleAyinFieldGet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinFieldGet = null;


        try {
            // InternalAyin.g:2677:53: (iv_ruleAyinFieldGet= ruleAyinFieldGet EOF )
            // InternalAyin.g:2678:2: iv_ruleAyinFieldGet= ruleAyinFieldGet EOF
            {
             newCompositeNode(grammarAccess.getAyinFieldGetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinFieldGet=ruleAyinFieldGet();

            state._fsp--;

             current =iv_ruleAyinFieldGet; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinFieldGet"


    // $ANTLR start "ruleAyinFieldGet"
    // InternalAyin.g:2684:1: ruleAyinFieldGet returns [EObject current=null] : ( (this_AyinMethodCall_0= ruleAyinMethodCall | this_AyinRef_1= ruleAyinRef ) ( () otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) )? ) ;
    public final EObject ruleAyinFieldGet() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject this_AyinMethodCall_0 = null;

        EObject this_AyinRef_1 = null;



        	enterRule();

        try {
            // InternalAyin.g:2690:2: ( ( (this_AyinMethodCall_0= ruleAyinMethodCall | this_AyinRef_1= ruleAyinRef ) ( () otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) )? ) )
            // InternalAyin.g:2691:2: ( (this_AyinMethodCall_0= ruleAyinMethodCall | this_AyinRef_1= ruleAyinRef ) ( () otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) )? )
            {
            // InternalAyin.g:2691:2: ( (this_AyinMethodCall_0= ruleAyinMethodCall | this_AyinRef_1= ruleAyinRef ) ( () otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) )? )
            // InternalAyin.g:2692:3: (this_AyinMethodCall_0= ruleAyinMethodCall | this_AyinRef_1= ruleAyinRef ) ( () otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) )?
            {
            // InternalAyin.g:2692:3: (this_AyinMethodCall_0= ruleAyinMethodCall | this_AyinRef_1= ruleAyinRef )
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==36) ) {
                alt54=1;
            }
            else if ( (LA54_0==RULE_ID) ) {
                int LA54_2 = input.LA(2);

                if ( (LA54_2==15) ) {
                    int LA54_3 = input.LA(3);

                    if ( (LA54_3==RULE_ID) ) {
                        int LA54_5 = input.LA(4);

                        if ( (LA54_5==30) ) {
                            alt54=1;
                        }
                        else if ( (LA54_5==EOF||LA54_5==14||LA54_5==25||LA54_5==31||LA54_5==35) ) {
                            alt54=2;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 54, 5, input);

                            throw nvae;
                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 54, 3, input);

                        throw nvae;
                    }
                }
                else if ( (LA54_2==EOF||LA54_2==14||LA54_2==25||LA54_2==31||LA54_2==35) ) {
                    alt54=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 54, 2, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 54, 0, input);

                throw nvae;
            }
            switch (alt54) {
                case 1 :
                    // InternalAyin.g:2693:4: this_AyinMethodCall_0= ruleAyinMethodCall
                    {

                    				newCompositeNode(grammarAccess.getAyinFieldGetAccess().getAyinMethodCallParserRuleCall_0_0());
                    			
                    pushFollow(FOLLOW_4);
                    this_AyinMethodCall_0=ruleAyinMethodCall();

                    state._fsp--;


                    				current = this_AyinMethodCall_0;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;
                case 2 :
                    // InternalAyin.g:2702:4: this_AyinRef_1= ruleAyinRef
                    {

                    				newCompositeNode(grammarAccess.getAyinFieldGetAccess().getAyinRefParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_4);
                    this_AyinRef_1=ruleAyinRef();

                    state._fsp--;


                    				current = this_AyinRef_1;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalAyin.g:2711:3: ( () otherlv_3= '.' ( (otherlv_4= RULE_ID ) ) )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==15) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalAyin.g:2712:4: () otherlv_3= '.' ( (otherlv_4= RULE_ID ) )
                    {
                    // InternalAyin.g:2712:4: ()
                    // InternalAyin.g:2713:5: 
                    {

                    					current = forceCreateModelElementAndSet(
                    						grammarAccess.getAyinFieldGetAccess().getAyinFieldGetReceiverAction_1_0(),
                    						current);
                    				

                    }

                    otherlv_3=(Token)match(input,15,FOLLOW_5); 

                    				newLeafNode(otherlv_3, grammarAccess.getAyinFieldGetAccess().getFullStopKeyword_1_1());
                    			
                    // InternalAyin.g:2723:4: ( (otherlv_4= RULE_ID ) )
                    // InternalAyin.g:2724:5: (otherlv_4= RULE_ID )
                    {
                    // InternalAyin.g:2724:5: (otherlv_4= RULE_ID )
                    // InternalAyin.g:2725:6: otherlv_4= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getAyinFieldGetRule());
                    						}
                    					
                    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(otherlv_4, grammarAccess.getAyinFieldGetAccess().getFieldAyinParameterCrossReference_1_2_0());
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinFieldGet"


    // $ANTLR start "entryRuleAyinRef"
    // InternalAyin.g:2741:1: entryRuleAyinRef returns [EObject current=null] : iv_ruleAyinRef= ruleAyinRef EOF ;
    public final EObject entryRuleAyinRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinRef = null;


        try {
            // InternalAyin.g:2741:48: (iv_ruleAyinRef= ruleAyinRef EOF )
            // InternalAyin.g:2742:2: iv_ruleAyinRef= ruleAyinRef EOF
            {
             newCompositeNode(grammarAccess.getAyinRefRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinRef=ruleAyinRef();

            state._fsp--;

             current =iv_ruleAyinRef; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinRef"


    // $ANTLR start "ruleAyinRef"
    // InternalAyin.g:2748:1: ruleAyinRef returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleAyinRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalAyin.g:2754:2: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalAyin.g:2755:2: ( (otherlv_0= RULE_ID ) )
            {
            // InternalAyin.g:2755:2: ( (otherlv_0= RULE_ID ) )
            // InternalAyin.g:2756:3: (otherlv_0= RULE_ID )
            {
            // InternalAyin.g:2756:3: (otherlv_0= RULE_ID )
            // InternalAyin.g:2757:4: otherlv_0= RULE_ID
            {

            				if (current==null) {
            					current = createModelElement(grammarAccess.getAyinRefRule());
            				}
            			
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(otherlv_0, grammarAccess.getAyinRefAccess().getRefAyinParameterCrossReference_0());
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinRef"


    // $ANTLR start "entryRuleAyinUnderscore"
    // InternalAyin.g:2771:1: entryRuleAyinUnderscore returns [EObject current=null] : iv_ruleAyinUnderscore= ruleAyinUnderscore EOF ;
    public final EObject entryRuleAyinUnderscore() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinUnderscore = null;


        try {
            // InternalAyin.g:2771:55: (iv_ruleAyinUnderscore= ruleAyinUnderscore EOF )
            // InternalAyin.g:2772:2: iv_ruleAyinUnderscore= ruleAyinUnderscore EOF
            {
             newCompositeNode(grammarAccess.getAyinUnderscoreRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinUnderscore=ruleAyinUnderscore();

            state._fsp--;

             current =iv_ruleAyinUnderscore; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinUnderscore"


    // $ANTLR start "ruleAyinUnderscore"
    // InternalAyin.g:2778:1: ruleAyinUnderscore returns [EObject current=null] : ( () otherlv_1= '_' ) ;
    public final EObject ruleAyinUnderscore() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalAyin.g:2784:2: ( ( () otherlv_1= '_' ) )
            // InternalAyin.g:2785:2: ( () otherlv_1= '_' )
            {
            // InternalAyin.g:2785:2: ( () otherlv_1= '_' )
            // InternalAyin.g:2786:3: () otherlv_1= '_'
            {
            // InternalAyin.g:2786:3: ()
            // InternalAyin.g:2787:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAyinUnderscoreAccess().getAyinUnderscoreAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,45,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getAyinUnderscoreAccess().get_Keyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinUnderscore"


    // $ANTLR start "entryRuleAyinType"
    // InternalAyin.g:2801:1: entryRuleAyinType returns [EObject current=null] : iv_ruleAyinType= ruleAyinType EOF ;
    public final EObject entryRuleAyinType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinType = null;


        try {
            // InternalAyin.g:2801:49: (iv_ruleAyinType= ruleAyinType EOF )
            // InternalAyin.g:2802:2: iv_ruleAyinType= ruleAyinType EOF
            {
             newCompositeNode(grammarAccess.getAyinTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinType=ruleAyinType();

            state._fsp--;

             current =iv_ruleAyinType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinType"


    // $ANTLR start "ruleAyinType"
    // InternalAyin.g:2808:1: ruleAyinType returns [EObject current=null] : (this_AyinPrimitiveType_0= ruleAyinPrimitiveType | this_AyinEntityType_1= ruleAyinEntityType | this_AyinDataType_2= ruleAyinDataType ) ;
    public final EObject ruleAyinType() throws RecognitionException {
        EObject current = null;

        EObject this_AyinPrimitiveType_0 = null;

        EObject this_AyinEntityType_1 = null;

        EObject this_AyinDataType_2 = null;



        	enterRule();

        try {
            // InternalAyin.g:2814:2: ( (this_AyinPrimitiveType_0= ruleAyinPrimitiveType | this_AyinEntityType_1= ruleAyinEntityType | this_AyinDataType_2= ruleAyinDataType ) )
            // InternalAyin.g:2815:2: (this_AyinPrimitiveType_0= ruleAyinPrimitiveType | this_AyinEntityType_1= ruleAyinEntityType | this_AyinDataType_2= ruleAyinDataType )
            {
            // InternalAyin.g:2815:2: (this_AyinPrimitiveType_0= ruleAyinPrimitiveType | this_AyinEntityType_1= ruleAyinEntityType | this_AyinDataType_2= ruleAyinDataType )
            int alt56=3;
            int LA56_0 = input.LA(1);

            if ( ((LA56_0>=47 && LA56_0<=53)) ) {
                alt56=1;
            }
            else if ( (LA56_0==RULE_ID) ) {
                int LA56_2 = input.LA(2);

                if ( (LA56_2==EOF||LA56_2==RULE_ID||LA56_2==25||LA56_2==28) ) {
                    alt56=2;
                }
                else if ( (LA56_2==27) ) {
                    alt56=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 56, 2, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 56, 0, input);

                throw nvae;
            }
            switch (alt56) {
                case 1 :
                    // InternalAyin.g:2816:3: this_AyinPrimitiveType_0= ruleAyinPrimitiveType
                    {

                    			newCompositeNode(grammarAccess.getAyinTypeAccess().getAyinPrimitiveTypeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinPrimitiveType_0=ruleAyinPrimitiveType();

                    state._fsp--;


                    			current = this_AyinPrimitiveType_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAyin.g:2825:3: this_AyinEntityType_1= ruleAyinEntityType
                    {

                    			newCompositeNode(grammarAccess.getAyinTypeAccess().getAyinEntityTypeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinEntityType_1=ruleAyinEntityType();

                    state._fsp--;


                    			current = this_AyinEntityType_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalAyin.g:2834:3: this_AyinDataType_2= ruleAyinDataType
                    {

                    			newCompositeNode(grammarAccess.getAyinTypeAccess().getAyinDataTypeParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinDataType_2=ruleAyinDataType();

                    state._fsp--;


                    			current = this_AyinDataType_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinType"


    // $ANTLR start "entryRuleAyinDataType"
    // InternalAyin.g:2846:1: entryRuleAyinDataType returns [EObject current=null] : iv_ruleAyinDataType= ruleAyinDataType EOF ;
    public final EObject entryRuleAyinDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinDataType = null;


        try {
            // InternalAyin.g:2846:53: (iv_ruleAyinDataType= ruleAyinDataType EOF )
            // InternalAyin.g:2847:2: iv_ruleAyinDataType= ruleAyinDataType EOF
            {
             newCompositeNode(grammarAccess.getAyinDataTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinDataType=ruleAyinDataType();

            state._fsp--;

             current =iv_ruleAyinDataType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinDataType"


    // $ANTLR start "ruleAyinDataType"
    // InternalAyin.g:2853:1: ruleAyinDataType returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '<' ( ( (lv_arguments_2_0= ruleAyinTypeOrWildCard ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleAyinTypeOrWildCard ) ) )* )? otherlv_5= '>' ) ;
    public final EObject ruleAyinDataType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_arguments_2_0 = null;

        EObject lv_arguments_4_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:2859:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '<' ( ( (lv_arguments_2_0= ruleAyinTypeOrWildCard ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleAyinTypeOrWildCard ) ) )* )? otherlv_5= '>' ) )
            // InternalAyin.g:2860:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '<' ( ( (lv_arguments_2_0= ruleAyinTypeOrWildCard ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleAyinTypeOrWildCard ) ) )* )? otherlv_5= '>' )
            {
            // InternalAyin.g:2860:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '<' ( ( (lv_arguments_2_0= ruleAyinTypeOrWildCard ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleAyinTypeOrWildCard ) ) )* )? otherlv_5= '>' )
            // InternalAyin.g:2861:3: ( (otherlv_0= RULE_ID ) ) otherlv_1= '<' ( ( (lv_arguments_2_0= ruleAyinTypeOrWildCard ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleAyinTypeOrWildCard ) ) )* )? otherlv_5= '>'
            {
            // InternalAyin.g:2861:3: ( (otherlv_0= RULE_ID ) )
            // InternalAyin.g:2862:4: (otherlv_0= RULE_ID )
            {
            // InternalAyin.g:2862:4: (otherlv_0= RULE_ID )
            // InternalAyin.g:2863:5: otherlv_0= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinDataTypeRule());
            					}
            				
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_16); 

            					newLeafNode(otherlv_0, grammarAccess.getAyinDataTypeAccess().getDatatypeAyinDataCrossReference_0_0());
            				

            }


            }

            otherlv_1=(Token)match(input,27,FOLLOW_43); 

            			newLeafNode(otherlv_1, grammarAccess.getAyinDataTypeAccess().getLessThanSignKeyword_1());
            		
            // InternalAyin.g:2878:3: ( ( (lv_arguments_2_0= ruleAyinTypeOrWildCard ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleAyinTypeOrWildCard ) ) )* )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==RULE_ID||(LA58_0>=46 && LA58_0<=53)) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // InternalAyin.g:2879:4: ( (lv_arguments_2_0= ruleAyinTypeOrWildCard ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleAyinTypeOrWildCard ) ) )*
                    {
                    // InternalAyin.g:2879:4: ( (lv_arguments_2_0= ruleAyinTypeOrWildCard ) )
                    // InternalAyin.g:2880:5: (lv_arguments_2_0= ruleAyinTypeOrWildCard )
                    {
                    // InternalAyin.g:2880:5: (lv_arguments_2_0= ruleAyinTypeOrWildCard )
                    // InternalAyin.g:2881:6: lv_arguments_2_0= ruleAyinTypeOrWildCard
                    {

                    						newCompositeNode(grammarAccess.getAyinDataTypeAccess().getArgumentsAyinTypeOrWildCardParserRuleCall_2_0_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_arguments_2_0=ruleAyinTypeOrWildCard();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinDataTypeRule());
                    						}
                    						add(
                    							current,
                    							"arguments",
                    							lv_arguments_2_0,
                    							"space.ayin.lang.Ayin.AyinTypeOrWildCard");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalAyin.g:2898:4: (otherlv_3= ',' ( (lv_arguments_4_0= ruleAyinTypeOrWildCard ) ) )*
                    loop57:
                    do {
                        int alt57=2;
                        int LA57_0 = input.LA(1);

                        if ( (LA57_0==25) ) {
                            alt57=1;
                        }


                        switch (alt57) {
                    	case 1 :
                    	    // InternalAyin.g:2899:5: otherlv_3= ',' ( (lv_arguments_4_0= ruleAyinTypeOrWildCard ) )
                    	    {
                    	    otherlv_3=(Token)match(input,25,FOLLOW_44); 

                    	    					newLeafNode(otherlv_3, grammarAccess.getAyinDataTypeAccess().getCommaKeyword_2_1_0());
                    	    				
                    	    // InternalAyin.g:2903:5: ( (lv_arguments_4_0= ruleAyinTypeOrWildCard ) )
                    	    // InternalAyin.g:2904:6: (lv_arguments_4_0= ruleAyinTypeOrWildCard )
                    	    {
                    	    // InternalAyin.g:2904:6: (lv_arguments_4_0= ruleAyinTypeOrWildCard )
                    	    // InternalAyin.g:2905:7: lv_arguments_4_0= ruleAyinTypeOrWildCard
                    	    {

                    	    							newCompositeNode(grammarAccess.getAyinDataTypeAccess().getArgumentsAyinTypeOrWildCardParserRuleCall_2_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_18);
                    	    lv_arguments_4_0=ruleAyinTypeOrWildCard();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getAyinDataTypeRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"arguments",
                    	    								lv_arguments_4_0,
                    	    								"space.ayin.lang.Ayin.AyinTypeOrWildCard");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop57;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,28,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getAyinDataTypeAccess().getGreaterThanSignKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinDataType"


    // $ANTLR start "entryRuleAyinTypeOrWildCard"
    // InternalAyin.g:2932:1: entryRuleAyinTypeOrWildCard returns [EObject current=null] : iv_ruleAyinTypeOrWildCard= ruleAyinTypeOrWildCard EOF ;
    public final EObject entryRuleAyinTypeOrWildCard() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinTypeOrWildCard = null;


        try {
            // InternalAyin.g:2932:59: (iv_ruleAyinTypeOrWildCard= ruleAyinTypeOrWildCard EOF )
            // InternalAyin.g:2933:2: iv_ruleAyinTypeOrWildCard= ruleAyinTypeOrWildCard EOF
            {
             newCompositeNode(grammarAccess.getAyinTypeOrWildCardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinTypeOrWildCard=ruleAyinTypeOrWildCard();

            state._fsp--;

             current =iv_ruleAyinTypeOrWildCard; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinTypeOrWildCard"


    // $ANTLR start "ruleAyinTypeOrWildCard"
    // InternalAyin.g:2939:1: ruleAyinTypeOrWildCard returns [EObject current=null] : (this_AyinType_0= ruleAyinType | ( () otherlv_2= '?' ) ) ;
    public final EObject ruleAyinTypeOrWildCard() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_AyinType_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:2945:2: ( (this_AyinType_0= ruleAyinType | ( () otherlv_2= '?' ) ) )
            // InternalAyin.g:2946:2: (this_AyinType_0= ruleAyinType | ( () otherlv_2= '?' ) )
            {
            // InternalAyin.g:2946:2: (this_AyinType_0= ruleAyinType | ( () otherlv_2= '?' ) )
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==RULE_ID||(LA59_0>=47 && LA59_0<=53)) ) {
                alt59=1;
            }
            else if ( (LA59_0==46) ) {
                alt59=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 59, 0, input);

                throw nvae;
            }
            switch (alt59) {
                case 1 :
                    // InternalAyin.g:2947:3: this_AyinType_0= ruleAyinType
                    {

                    			newCompositeNode(grammarAccess.getAyinTypeOrWildCardAccess().getAyinTypeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AyinType_0=ruleAyinType();

                    state._fsp--;


                    			current = this_AyinType_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalAyin.g:2956:3: ( () otherlv_2= '?' )
                    {
                    // InternalAyin.g:2956:3: ( () otherlv_2= '?' )
                    // InternalAyin.g:2957:4: () otherlv_2= '?'
                    {
                    // InternalAyin.g:2957:4: ()
                    // InternalAyin.g:2958:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getAyinTypeOrWildCardAccess().getAyinWildcardAction_1_0(),
                    						current);
                    				

                    }

                    otherlv_2=(Token)match(input,46,FOLLOW_2); 

                    				newLeafNode(otherlv_2, grammarAccess.getAyinTypeOrWildCardAccess().getQuestionMarkKeyword_1_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinTypeOrWildCard"


    // $ANTLR start "entryRuleAyinEntityType"
    // InternalAyin.g:2973:1: entryRuleAyinEntityType returns [EObject current=null] : iv_ruleAyinEntityType= ruleAyinEntityType EOF ;
    public final EObject entryRuleAyinEntityType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinEntityType = null;


        try {
            // InternalAyin.g:2973:55: (iv_ruleAyinEntityType= ruleAyinEntityType EOF )
            // InternalAyin.g:2974:2: iv_ruleAyinEntityType= ruleAyinEntityType EOF
            {
             newCompositeNode(grammarAccess.getAyinEntityTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinEntityType=ruleAyinEntityType();

            state._fsp--;

             current =iv_ruleAyinEntityType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinEntityType"


    // $ANTLR start "ruleAyinEntityType"
    // InternalAyin.g:2980:1: ruleAyinEntityType returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleAyinEntityType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalAyin.g:2986:2: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalAyin.g:2987:2: ( (otherlv_0= RULE_ID ) )
            {
            // InternalAyin.g:2987:2: ( (otherlv_0= RULE_ID ) )
            // InternalAyin.g:2988:3: (otherlv_0= RULE_ID )
            {
            // InternalAyin.g:2988:3: (otherlv_0= RULE_ID )
            // InternalAyin.g:2989:4: otherlv_0= RULE_ID
            {

            				if (current==null) {
            					current = createModelElement(grammarAccess.getAyinEntityTypeRule());
            				}
            			
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(otherlv_0, grammarAccess.getAyinEntityTypeAccess().getEntityAyinEntityCrossReference_0());
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinEntityType"


    // $ANTLR start "entryRuleAyinPrimitiveType"
    // InternalAyin.g:3003:1: entryRuleAyinPrimitiveType returns [EObject current=null] : iv_ruleAyinPrimitiveType= ruleAyinPrimitiveType EOF ;
    public final EObject entryRuleAyinPrimitiveType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinPrimitiveType = null;


        try {
            // InternalAyin.g:3003:58: (iv_ruleAyinPrimitiveType= ruleAyinPrimitiveType EOF )
            // InternalAyin.g:3004:2: iv_ruleAyinPrimitiveType= ruleAyinPrimitiveType EOF
            {
             newCompositeNode(grammarAccess.getAyinPrimitiveTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinPrimitiveType=ruleAyinPrimitiveType();

            state._fsp--;

             current =iv_ruleAyinPrimitiveType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinPrimitiveType"


    // $ANTLR start "ruleAyinPrimitiveType"
    // InternalAyin.g:3010:1: ruleAyinPrimitiveType returns [EObject current=null] : ( ( (lv_name_0_1= 'i8' | lv_name_0_2= 'i16' | lv_name_0_3= 'i32' | lv_name_0_4= 'i64' | lv_name_0_5= 'string' | lv_name_0_6= 'boolean' | lv_name_0_7= 'double' ) ) ) ;
    public final EObject ruleAyinPrimitiveType() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_1=null;
        Token lv_name_0_2=null;
        Token lv_name_0_3=null;
        Token lv_name_0_4=null;
        Token lv_name_0_5=null;
        Token lv_name_0_6=null;
        Token lv_name_0_7=null;


        	enterRule();

        try {
            // InternalAyin.g:3016:2: ( ( ( (lv_name_0_1= 'i8' | lv_name_0_2= 'i16' | lv_name_0_3= 'i32' | lv_name_0_4= 'i64' | lv_name_0_5= 'string' | lv_name_0_6= 'boolean' | lv_name_0_7= 'double' ) ) ) )
            // InternalAyin.g:3017:2: ( ( (lv_name_0_1= 'i8' | lv_name_0_2= 'i16' | lv_name_0_3= 'i32' | lv_name_0_4= 'i64' | lv_name_0_5= 'string' | lv_name_0_6= 'boolean' | lv_name_0_7= 'double' ) ) )
            {
            // InternalAyin.g:3017:2: ( ( (lv_name_0_1= 'i8' | lv_name_0_2= 'i16' | lv_name_0_3= 'i32' | lv_name_0_4= 'i64' | lv_name_0_5= 'string' | lv_name_0_6= 'boolean' | lv_name_0_7= 'double' ) ) )
            // InternalAyin.g:3018:3: ( (lv_name_0_1= 'i8' | lv_name_0_2= 'i16' | lv_name_0_3= 'i32' | lv_name_0_4= 'i64' | lv_name_0_5= 'string' | lv_name_0_6= 'boolean' | lv_name_0_7= 'double' ) )
            {
            // InternalAyin.g:3018:3: ( (lv_name_0_1= 'i8' | lv_name_0_2= 'i16' | lv_name_0_3= 'i32' | lv_name_0_4= 'i64' | lv_name_0_5= 'string' | lv_name_0_6= 'boolean' | lv_name_0_7= 'double' ) )
            // InternalAyin.g:3019:4: (lv_name_0_1= 'i8' | lv_name_0_2= 'i16' | lv_name_0_3= 'i32' | lv_name_0_4= 'i64' | lv_name_0_5= 'string' | lv_name_0_6= 'boolean' | lv_name_0_7= 'double' )
            {
            // InternalAyin.g:3019:4: (lv_name_0_1= 'i8' | lv_name_0_2= 'i16' | lv_name_0_3= 'i32' | lv_name_0_4= 'i64' | lv_name_0_5= 'string' | lv_name_0_6= 'boolean' | lv_name_0_7= 'double' )
            int alt60=7;
            switch ( input.LA(1) ) {
            case 47:
                {
                alt60=1;
                }
                break;
            case 48:
                {
                alt60=2;
                }
                break;
            case 49:
                {
                alt60=3;
                }
                break;
            case 50:
                {
                alt60=4;
                }
                break;
            case 51:
                {
                alt60=5;
                }
                break;
            case 52:
                {
                alt60=6;
                }
                break;
            case 53:
                {
                alt60=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 60, 0, input);

                throw nvae;
            }

            switch (alt60) {
                case 1 :
                    // InternalAyin.g:3020:5: lv_name_0_1= 'i8'
                    {
                    lv_name_0_1=(Token)match(input,47,FOLLOW_2); 

                    					newLeafNode(lv_name_0_1, grammarAccess.getAyinPrimitiveTypeAccess().getNameI8Keyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAyinPrimitiveTypeRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_0_1, null);
                    				

                    }
                    break;
                case 2 :
                    // InternalAyin.g:3031:5: lv_name_0_2= 'i16'
                    {
                    lv_name_0_2=(Token)match(input,48,FOLLOW_2); 

                    					newLeafNode(lv_name_0_2, grammarAccess.getAyinPrimitiveTypeAccess().getNameI16Keyword_0_1());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAyinPrimitiveTypeRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_0_2, null);
                    				

                    }
                    break;
                case 3 :
                    // InternalAyin.g:3042:5: lv_name_0_3= 'i32'
                    {
                    lv_name_0_3=(Token)match(input,49,FOLLOW_2); 

                    					newLeafNode(lv_name_0_3, grammarAccess.getAyinPrimitiveTypeAccess().getNameI32Keyword_0_2());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAyinPrimitiveTypeRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_0_3, null);
                    				

                    }
                    break;
                case 4 :
                    // InternalAyin.g:3053:5: lv_name_0_4= 'i64'
                    {
                    lv_name_0_4=(Token)match(input,50,FOLLOW_2); 

                    					newLeafNode(lv_name_0_4, grammarAccess.getAyinPrimitiveTypeAccess().getNameI64Keyword_0_3());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAyinPrimitiveTypeRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_0_4, null);
                    				

                    }
                    break;
                case 5 :
                    // InternalAyin.g:3064:5: lv_name_0_5= 'string'
                    {
                    lv_name_0_5=(Token)match(input,51,FOLLOW_2); 

                    					newLeafNode(lv_name_0_5, grammarAccess.getAyinPrimitiveTypeAccess().getNameStringKeyword_0_4());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAyinPrimitiveTypeRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_0_5, null);
                    				

                    }
                    break;
                case 6 :
                    // InternalAyin.g:3075:5: lv_name_0_6= 'boolean'
                    {
                    lv_name_0_6=(Token)match(input,52,FOLLOW_2); 

                    					newLeafNode(lv_name_0_6, grammarAccess.getAyinPrimitiveTypeAccess().getNameBooleanKeyword_0_5());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAyinPrimitiveTypeRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_0_6, null);
                    				

                    }
                    break;
                case 7 :
                    // InternalAyin.g:3086:5: lv_name_0_7= 'double'
                    {
                    lv_name_0_7=(Token)match(input,53,FOLLOW_2); 

                    					newLeafNode(lv_name_0_7, grammarAccess.getAyinPrimitiveTypeAccess().getNameDoubleKeyword_0_6());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getAyinPrimitiveTypeRule());
                    					}
                    					setWithLastConsumed(current, "name", lv_name_0_7, null);
                    				

                    }
                    break;

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinPrimitiveType"


    // $ANTLR start "entryRuleAyinEnumElement"
    // InternalAyin.g:3102:1: entryRuleAyinEnumElement returns [EObject current=null] : iv_ruleAyinEnumElement= ruleAyinEnumElement EOF ;
    public final EObject entryRuleAyinEnumElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinEnumElement = null;


        try {
            // InternalAyin.g:3102:56: (iv_ruleAyinEnumElement= ruleAyinEnumElement EOF )
            // InternalAyin.g:3103:2: iv_ruleAyinEnumElement= ruleAyinEnumElement EOF
            {
             newCompositeNode(grammarAccess.getAyinEnumElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinEnumElement=ruleAyinEnumElement();

            state._fsp--;

             current =iv_ruleAyinEnumElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinEnumElement"


    // $ANTLR start "ruleAyinEnumElement"
    // InternalAyin.g:3109:1: ruleAyinEnumElement returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleAyinEnumElement() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalAyin.g:3115:2: ( ( () ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalAyin.g:3116:2: ( () ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalAyin.g:3116:2: ( () ( (lv_name_1_0= RULE_ID ) ) )
            // InternalAyin.g:3117:3: () ( (lv_name_1_0= RULE_ID ) )
            {
            // InternalAyin.g:3117:3: ()
            // InternalAyin.g:3118:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAyinEnumElementAccess().getAyinParameterAction_0(),
            					current);
            			

            }

            // InternalAyin.g:3124:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalAyin.g:3125:4: (lv_name_1_0= RULE_ID )
            {
            // InternalAyin.g:3125:4: (lv_name_1_0= RULE_ID )
            // InternalAyin.g:3126:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getAyinEnumElementAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAyinEnumElementRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"space.ayin.lang.Ayin.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinEnumElement"


    // $ANTLR start "entryRuleAyinEventParams"
    // InternalAyin.g:3146:1: entryRuleAyinEventParams returns [EObject current=null] : iv_ruleAyinEventParams= ruleAyinEventParams EOF ;
    public final EObject entryRuleAyinEventParams() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinEventParams = null;


        try {
            // InternalAyin.g:3146:56: (iv_ruleAyinEventParams= ruleAyinEventParams EOF )
            // InternalAyin.g:3147:2: iv_ruleAyinEventParams= ruleAyinEventParams EOF
            {
             newCompositeNode(grammarAccess.getAyinEventParamsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinEventParams=ruleAyinEventParams();

            state._fsp--;

             current =iv_ruleAyinEventParams; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinEventParams"


    // $ANTLR start "ruleAyinEventParams"
    // InternalAyin.g:3153:1: ruleAyinEventParams returns [EObject current=null] : ( () ( ( (lv_fields_1_0= ruleAyinParameter ) ) (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )* )? ) ;
    public final EObject ruleAyinEventParams() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_fields_1_0 = null;

        EObject lv_fields_3_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:3159:2: ( ( () ( ( (lv_fields_1_0= ruleAyinParameter ) ) (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )* )? ) )
            // InternalAyin.g:3160:2: ( () ( ( (lv_fields_1_0= ruleAyinParameter ) ) (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )* )? )
            {
            // InternalAyin.g:3160:2: ( () ( ( (lv_fields_1_0= ruleAyinParameter ) ) (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )* )? )
            // InternalAyin.g:3161:3: () ( ( (lv_fields_1_0= ruleAyinParameter ) ) (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )* )?
            {
            // InternalAyin.g:3161:3: ()
            // InternalAyin.g:3162:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAyinEventParamsAccess().getAyinStructAction_0(),
            					current);
            			

            }

            // InternalAyin.g:3168:3: ( ( (lv_fields_1_0= ruleAyinParameter ) ) (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )* )?
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==RULE_ID||(LA62_0>=47 && LA62_0<=53)) ) {
                alt62=1;
            }
            switch (alt62) {
                case 1 :
                    // InternalAyin.g:3169:4: ( (lv_fields_1_0= ruleAyinParameter ) ) (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )*
                    {
                    // InternalAyin.g:3169:4: ( (lv_fields_1_0= ruleAyinParameter ) )
                    // InternalAyin.g:3170:5: (lv_fields_1_0= ruleAyinParameter )
                    {
                    // InternalAyin.g:3170:5: (lv_fields_1_0= ruleAyinParameter )
                    // InternalAyin.g:3171:6: lv_fields_1_0= ruleAyinParameter
                    {

                    						newCompositeNode(grammarAccess.getAyinEventParamsAccess().getFieldsAyinParameterParserRuleCall_1_0_0());
                    					
                    pushFollow(FOLLOW_45);
                    lv_fields_1_0=ruleAyinParameter();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAyinEventParamsRule());
                    						}
                    						add(
                    							current,
                    							"fields",
                    							lv_fields_1_0,
                    							"space.ayin.lang.Ayin.AyinParameter");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalAyin.g:3188:4: (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )*
                    loop61:
                    do {
                        int alt61=2;
                        int LA61_0 = input.LA(1);

                        if ( (LA61_0==25) ) {
                            alt61=1;
                        }


                        switch (alt61) {
                    	case 1 :
                    	    // InternalAyin.g:3189:5: otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) )
                    	    {
                    	    otherlv_2=(Token)match(input,25,FOLLOW_28); 

                    	    					newLeafNode(otherlv_2, grammarAccess.getAyinEventParamsAccess().getCommaKeyword_1_1_0());
                    	    				
                    	    // InternalAyin.g:3193:5: ( (lv_fields_3_0= ruleAyinParameter ) )
                    	    // InternalAyin.g:3194:6: (lv_fields_3_0= ruleAyinParameter )
                    	    {
                    	    // InternalAyin.g:3194:6: (lv_fields_3_0= ruleAyinParameter )
                    	    // InternalAyin.g:3195:7: lv_fields_3_0= ruleAyinParameter
                    	    {

                    	    							newCompositeNode(grammarAccess.getAyinEventParamsAccess().getFieldsAyinParameterParserRuleCall_1_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_45);
                    	    lv_fields_3_0=ruleAyinParameter();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getAyinEventParamsRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"fields",
                    	    								lv_fields_3_0,
                    	    								"space.ayin.lang.Ayin.AyinParameter");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop61;
                        }
                    } while (true);


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinEventParams"


    // $ANTLR start "entryRuleAyinFields"
    // InternalAyin.g:3218:1: entryRuleAyinFields returns [EObject current=null] : iv_ruleAyinFields= ruleAyinFields EOF ;
    public final EObject entryRuleAyinFields() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinFields = null;


        try {
            // InternalAyin.g:3218:51: (iv_ruleAyinFields= ruleAyinFields EOF )
            // InternalAyin.g:3219:2: iv_ruleAyinFields= ruleAyinFields EOF
            {
             newCompositeNode(grammarAccess.getAyinFieldsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinFields=ruleAyinFields();

            state._fsp--;

             current =iv_ruleAyinFields; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinFields"


    // $ANTLR start "ruleAyinFields"
    // InternalAyin.g:3225:1: ruleAyinFields returns [EObject current=null] : ( ( (lv_fields_0_0= ruleAyinParameter ) ) (otherlv_1= ',' ( (lv_fields_2_0= ruleAyinParameter ) ) )* ) ;
    public final EObject ruleAyinFields() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_fields_0_0 = null;

        EObject lv_fields_2_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:3231:2: ( ( ( (lv_fields_0_0= ruleAyinParameter ) ) (otherlv_1= ',' ( (lv_fields_2_0= ruleAyinParameter ) ) )* ) )
            // InternalAyin.g:3232:2: ( ( (lv_fields_0_0= ruleAyinParameter ) ) (otherlv_1= ',' ( (lv_fields_2_0= ruleAyinParameter ) ) )* )
            {
            // InternalAyin.g:3232:2: ( ( (lv_fields_0_0= ruleAyinParameter ) ) (otherlv_1= ',' ( (lv_fields_2_0= ruleAyinParameter ) ) )* )
            // InternalAyin.g:3233:3: ( (lv_fields_0_0= ruleAyinParameter ) ) (otherlv_1= ',' ( (lv_fields_2_0= ruleAyinParameter ) ) )*
            {
            // InternalAyin.g:3233:3: ( (lv_fields_0_0= ruleAyinParameter ) )
            // InternalAyin.g:3234:4: (lv_fields_0_0= ruleAyinParameter )
            {
            // InternalAyin.g:3234:4: (lv_fields_0_0= ruleAyinParameter )
            // InternalAyin.g:3235:5: lv_fields_0_0= ruleAyinParameter
            {

            					newCompositeNode(grammarAccess.getAyinFieldsAccess().getFieldsAyinParameterParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_45);
            lv_fields_0_0=ruleAyinParameter();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAyinFieldsRule());
            					}
            					add(
            						current,
            						"fields",
            						lv_fields_0_0,
            						"space.ayin.lang.Ayin.AyinParameter");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAyin.g:3252:3: (otherlv_1= ',' ( (lv_fields_2_0= ruleAyinParameter ) ) )*
            loop63:
            do {
                int alt63=2;
                int LA63_0 = input.LA(1);

                if ( (LA63_0==25) ) {
                    int LA63_1 = input.LA(2);

                    if ( (LA63_1==RULE_ID||(LA63_1>=47 && LA63_1<=53)) ) {
                        alt63=1;
                    }


                }


                switch (alt63) {
            	case 1 :
            	    // InternalAyin.g:3253:4: otherlv_1= ',' ( (lv_fields_2_0= ruleAyinParameter ) )
            	    {
            	    otherlv_1=(Token)match(input,25,FOLLOW_28); 

            	    				newLeafNode(otherlv_1, grammarAccess.getAyinFieldsAccess().getCommaKeyword_1_0());
            	    			
            	    // InternalAyin.g:3257:4: ( (lv_fields_2_0= ruleAyinParameter ) )
            	    // InternalAyin.g:3258:5: (lv_fields_2_0= ruleAyinParameter )
            	    {
            	    // InternalAyin.g:3258:5: (lv_fields_2_0= ruleAyinParameter )
            	    // InternalAyin.g:3259:6: lv_fields_2_0= ruleAyinParameter
            	    {

            	    						newCompositeNode(grammarAccess.getAyinFieldsAccess().getFieldsAyinParameterParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_45);
            	    lv_fields_2_0=ruleAyinParameter();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAyinFieldsRule());
            	    						}
            	    						add(
            	    							current,
            	    							"fields",
            	    							lv_fields_2_0,
            	    							"space.ayin.lang.Ayin.AyinParameter");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop63;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinFields"


    // $ANTLR start "entryRuleAyinResultFields"
    // InternalAyin.g:3281:1: entryRuleAyinResultFields returns [EObject current=null] : iv_ruleAyinResultFields= ruleAyinResultFields EOF ;
    public final EObject entryRuleAyinResultFields() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAyinResultFields = null;


        try {
            // InternalAyin.g:3281:57: (iv_ruleAyinResultFields= ruleAyinResultFields EOF )
            // InternalAyin.g:3282:2: iv_ruleAyinResultFields= ruleAyinResultFields EOF
            {
             newCompositeNode(grammarAccess.getAyinResultFieldsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAyinResultFields=ruleAyinResultFields();

            state._fsp--;

             current =iv_ruleAyinResultFields; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAyinResultFields"


    // $ANTLR start "ruleAyinResultFields"
    // InternalAyin.g:3288:1: ruleAyinResultFields returns [EObject current=null] : (otherlv_0= '(' ( (lv_fields_1_0= ruleAyinParameter ) ) (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )* otherlv_4= ')' ) ;
    public final EObject ruleAyinResultFields() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_fields_1_0 = null;

        EObject lv_fields_3_0 = null;



        	enterRule();

        try {
            // InternalAyin.g:3294:2: ( (otherlv_0= '(' ( (lv_fields_1_0= ruleAyinParameter ) ) (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )* otherlv_4= ')' ) )
            // InternalAyin.g:3295:2: (otherlv_0= '(' ( (lv_fields_1_0= ruleAyinParameter ) ) (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )* otherlv_4= ')' )
            {
            // InternalAyin.g:3295:2: (otherlv_0= '(' ( (lv_fields_1_0= ruleAyinParameter ) ) (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )* otherlv_4= ')' )
            // InternalAyin.g:3296:3: otherlv_0= '(' ( (lv_fields_1_0= ruleAyinParameter ) ) (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )* otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,30,FOLLOW_28); 

            			newLeafNode(otherlv_0, grammarAccess.getAyinResultFieldsAccess().getLeftParenthesisKeyword_0());
            		
            // InternalAyin.g:3300:3: ( (lv_fields_1_0= ruleAyinParameter ) )
            // InternalAyin.g:3301:4: (lv_fields_1_0= ruleAyinParameter )
            {
            // InternalAyin.g:3301:4: (lv_fields_1_0= ruleAyinParameter )
            // InternalAyin.g:3302:5: lv_fields_1_0= ruleAyinParameter
            {

            					newCompositeNode(grammarAccess.getAyinResultFieldsAccess().getFieldsAyinParameterParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_23);
            lv_fields_1_0=ruleAyinParameter();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAyinResultFieldsRule());
            					}
            					add(
            						current,
            						"fields",
            						lv_fields_1_0,
            						"space.ayin.lang.Ayin.AyinParameter");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalAyin.g:3319:3: (otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) ) )*
            loop64:
            do {
                int alt64=2;
                int LA64_0 = input.LA(1);

                if ( (LA64_0==25) ) {
                    alt64=1;
                }


                switch (alt64) {
            	case 1 :
            	    // InternalAyin.g:3320:4: otherlv_2= ',' ( (lv_fields_3_0= ruleAyinParameter ) )
            	    {
            	    otherlv_2=(Token)match(input,25,FOLLOW_28); 

            	    				newLeafNode(otherlv_2, grammarAccess.getAyinResultFieldsAccess().getCommaKeyword_2_0());
            	    			
            	    // InternalAyin.g:3324:4: ( (lv_fields_3_0= ruleAyinParameter ) )
            	    // InternalAyin.g:3325:5: (lv_fields_3_0= ruleAyinParameter )
            	    {
            	    // InternalAyin.g:3325:5: (lv_fields_3_0= ruleAyinParameter )
            	    // InternalAyin.g:3326:6: lv_fields_3_0= ruleAyinParameter
            	    {

            	    						newCompositeNode(grammarAccess.getAyinResultFieldsAccess().getFieldsAyinParameterParserRuleCall_2_1_0());
            	    					
            	    pushFollow(FOLLOW_23);
            	    lv_fields_3_0=ruleAyinParameter();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAyinResultFieldsRule());
            	    						}
            	    						add(
            	    							current,
            	    							"fields",
            	    							lv_fields_3_0,
            	    							"space.ayin.lang.Ayin.AyinParameter");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop64;
                }
            } while (true);

            otherlv_4=(Token)match(input,31,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getAyinResultFieldsAccess().getRightParenthesisKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAyinResultFields"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000040010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x003F800160040010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x003F800140040010L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000220000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x003F800000040010L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000001220000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001020000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000002020000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000010000010L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000012000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x003F800080000010L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x003F800480000010L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000082000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000200024000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000202024000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000482000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x003F800000000010L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x003FA9B000040010L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x003FA01080000010L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x003FA01000000010L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x00001012000241F0L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x000001A200024000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000040000000002L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x003F800010000010L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000000802000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000802004000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x003FC00010000010L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x003FC00000000010L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000000002000002L});

}