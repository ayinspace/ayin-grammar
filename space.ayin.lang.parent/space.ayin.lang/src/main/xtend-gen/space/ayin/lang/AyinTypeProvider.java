package space.ayin.lang;

import com.google.common.base.Objects;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import space.ayin.lang.AyinModelUtil;
import space.ayin.lang.ayin.AyinBlock;
import space.ayin.lang.ayin.AyinBooleanValue;
import space.ayin.lang.ayin.AyinCallback;
import space.ayin.lang.ayin.AyinData;
import space.ayin.lang.ayin.AyinDataType;
import space.ayin.lang.ayin.AyinDataTypeInstantion;
import space.ayin.lang.ayin.AyinDoubleValue;
import space.ayin.lang.ayin.AyinEntity;
import space.ayin.lang.ayin.AyinEntityType;
import space.ayin.lang.ayin.AyinEnum;
import space.ayin.lang.ayin.AyinExpression;
import space.ayin.lang.ayin.AyinFactory;
import space.ayin.lang.ayin.AyinFieldGet;
import space.ayin.lang.ayin.AyinIntegerValue;
import space.ayin.lang.ayin.AyinMethodCall;
import space.ayin.lang.ayin.AyinParameter;
import space.ayin.lang.ayin.AyinParentable;
import space.ayin.lang.ayin.AyinPrimitiveType;
import space.ayin.lang.ayin.AyinRef;
import space.ayin.lang.ayin.AyinStringValue;
import space.ayin.lang.ayin.AyinStruct;
import space.ayin.lang.ayin.AyinStructInstantion;
import space.ayin.lang.ayin.AyinType;
import space.ayin.lang.ayin.AyinUnderscore;
import space.ayin.lang.ayin.AyinWildcard;

@SuppressWarnings("all")
public class AyinTypeProvider {
  public final static AyinPrimitiveType i8Type = ObjectExtensions.<AyinPrimitiveType>operator_doubleArrow(AyinFactory.eINSTANCE.createAyinPrimitiveType(), ((Procedure1<AyinPrimitiveType>) (AyinPrimitiveType it) -> {
    it.setName("i8");
  }));
  
  public final static AyinPrimitiveType i16Type = ObjectExtensions.<AyinPrimitiveType>operator_doubleArrow(AyinFactory.eINSTANCE.createAyinPrimitiveType(), ((Procedure1<AyinPrimitiveType>) (AyinPrimitiveType it) -> {
    it.setName("i16");
  }));
  
  public final static AyinPrimitiveType i32Type = ObjectExtensions.<AyinPrimitiveType>operator_doubleArrow(AyinFactory.eINSTANCE.createAyinPrimitiveType(), ((Procedure1<AyinPrimitiveType>) (AyinPrimitiveType it) -> {
    it.setName("i32");
  }));
  
  public final static AyinPrimitiveType i64Type = ObjectExtensions.<AyinPrimitiveType>operator_doubleArrow(AyinFactory.eINSTANCE.createAyinPrimitiveType(), ((Procedure1<AyinPrimitiveType>) (AyinPrimitiveType it) -> {
    it.setName("i64");
  }));
  
  public final static AyinPrimitiveType doubleType = ObjectExtensions.<AyinPrimitiveType>operator_doubleArrow(AyinFactory.eINSTANCE.createAyinPrimitiveType(), ((Procedure1<AyinPrimitiveType>) (AyinPrimitiveType it) -> {
    it.setName("double");
  }));
  
  public final static AyinPrimitiveType stringType = ObjectExtensions.<AyinPrimitiveType>operator_doubleArrow(AyinFactory.eINSTANCE.createAyinPrimitiveType(), ((Procedure1<AyinPrimitiveType>) (AyinPrimitiveType it) -> {
    it.setName("string");
  }));
  
  public final static AyinPrimitiveType booleanType = ObjectExtensions.<AyinPrimitiveType>operator_doubleArrow(AyinFactory.eINSTANCE.createAyinPrimitiveType(), ((Procedure1<AyinPrimitiveType>) (AyinPrimitiveType it) -> {
    it.setName("boolean");
  }));
  
  public final static AyinType voidType = ObjectExtensions.<AyinType>operator_doubleArrow(AyinFactory.eINSTANCE.createAyinType(), ((Procedure1<AyinType>) (AyinType it) -> {
    it.setName("void");
  }));
  
  public final static AyinType underscoreType = ObjectExtensions.<AyinType>operator_doubleArrow(AyinFactory.eINSTANCE.createAyinType(), ((Procedure1<AyinType>) (AyinType it) -> {
    it.setName("underscore");
  }));
  
  public final static AyinType callbackType = ObjectExtensions.<AyinType>operator_doubleArrow(AyinFactory.eINSTANCE.createAyinType(), ((Procedure1<AyinType>) (AyinType it) -> {
    it.setName("callback");
  }));
  
  public final static AyinType blockType = ObjectExtensions.<AyinType>operator_doubleArrow(AyinFactory.eINSTANCE.createAyinType(), ((Procedure1<AyinType>) (AyinType it) -> {
    it.setName("block");
  }));
  
  public List<AyinType> typeFor(final AyinExpression e) {
    final ArrayList<AyinType> res = new ArrayList<AyinType>();
    boolean _matched = false;
    if (e instanceof AyinDoubleValue) {
      _matched=true;
      res.add(AyinTypeProvider.doubleType);
    }
    if (!_matched) {
      if (e instanceof AyinStringValue) {
        _matched=true;
        res.add(AyinTypeProvider.stringType);
      }
    }
    if (!_matched) {
      if (e instanceof AyinBooleanValue) {
        _matched=true;
        res.add(AyinTypeProvider.booleanType);
      }
    }
    if (!_matched) {
      if (e instanceof AyinIntegerValue) {
        _matched=true;
        long _value = ((AyinIntegerValue)e).getValue();
        final long value = _value;
        boolean _matched_1 = false;
        if (((value <= Byte.MAX_VALUE) && (value >= Byte.MIN_VALUE))) {
          _matched_1=true;
          res.add(AyinTypeProvider.i8Type);
        }
        if (!_matched_1) {
          if (((value <= Short.MAX_VALUE) && (value >= Short.MIN_VALUE))) {
            _matched_1=true;
            res.add(AyinTypeProvider.i16Type);
          }
        }
        if (!_matched_1) {
          if (((value <= Integer.MAX_VALUE) && (value >= Integer.MIN_VALUE))) {
            _matched_1=true;
            res.add(AyinTypeProvider.i32Type);
          }
        }
        if (!_matched_1) {
          if (((value <= Long.MAX_VALUE) && (value >= Long.MIN_VALUE))) {
            _matched_1=true;
            res.add(AyinTypeProvider.i64Type);
          }
        }
      }
    }
    if (!_matched) {
      if (e instanceof AyinFieldGet) {
        _matched=true;
        final AyinExpression receiver = ((AyinFieldGet)e).getReceiver();
        if ((receiver instanceof AyinRef)) {
          AyinParameter _ref = ((AyinRef)receiver).getRef();
          if ((_ref instanceof AyinEnum)) {
            AyinEntityType _createAyinEntityType = AyinFactory.eINSTANCE.createAyinEntityType();
            final Procedure1<AyinEntityType> _function = (AyinEntityType it) -> {
              it.setEntity(((AyinRef)receiver).getRef());
            };
            AyinEntityType _doubleArrow = ObjectExtensions.<AyinEntityType>operator_doubleArrow(_createAyinEntityType, _function);
            res.add(_doubleArrow);
          } else {
            res.add(((AyinFieldGet)e).getField().getType());
          }
        } else {
          res.add(((AyinFieldGet)e).getField().getType());
        }
      }
    }
    if (!_matched) {
      if (e instanceof AyinParameter) {
        _matched=true;
        res.add(((AyinParameter)e).getType());
      }
    }
    if (!_matched) {
      if (e instanceof AyinRef) {
        _matched=true;
        AyinParameter _ref = ((AyinRef)e).getRef();
        if ((_ref instanceof AyinCallback)) {
          res.add(AyinTypeProvider.callbackType);
        } else {
          res.add(((AyinRef)e).getRef().getType());
        }
      }
    }
    if (!_matched) {
      if (e instanceof AyinMethodCall) {
        _matched=true;
        AyinStruct _resultStruct = ((AyinMethodCall)e).getMethod().getResultStruct();
        boolean _tripleNotEquals = (_resultStruct != null);
        if (_tripleNotEquals) {
          final Function1<AyinParameter, AyinType> _function = (AyinParameter it) -> {
            return it.getType();
          };
          res.addAll(ListExtensions.<AyinParameter, AyinType>map(((AyinMethodCall)e).getMethod().getResultStruct().getFields(), _function));
        } else {
          AyinType _retType = ((AyinMethodCall)e).getMethod().getRetType();
          boolean _tripleNotEquals_1 = (_retType != null);
          if (_tripleNotEquals_1) {
            res.add(((AyinMethodCall)e).getMethod().getRetType());
          } else {
            res.add(AyinTypeProvider.voidType);
          }
        }
      }
    }
    if (!_matched) {
      if (e instanceof AyinStructInstantion) {
        _matched=true;
        AyinEntityType _createAyinEntityType = AyinFactory.eINSTANCE.createAyinEntityType();
        final Procedure1<AyinEntityType> _function = (AyinEntityType it) -> {
          it.setEntity(((AyinStructInstantion)e).getEntity());
        };
        AyinEntityType _doubleArrow = ObjectExtensions.<AyinEntityType>operator_doubleArrow(_createAyinEntityType, _function);
        res.add(_doubleArrow);
      }
    }
    if (!_matched) {
      if (e instanceof AyinDataTypeInstantion) {
        _matched=true;
        AyinDataType _createAyinDataType = AyinFactory.eINSTANCE.createAyinDataType();
        final Procedure1<AyinDataType> _function = (AyinDataType it) -> {
          AyinEntity _entity = ((AyinDataTypeInstantion)e).getEntity();
          it.setDatatype(((AyinData) _entity));
        };
        final AyinDataType dataType = ObjectExtensions.<AyinDataType>operator_doubleArrow(_createAyinDataType, _function);
        dataType.getArguments().addAll(((AyinDataTypeInstantion)e).getArguments());
        res.add(dataType);
      }
    }
    if (!_matched) {
      if (e instanceof AyinUnderscore) {
        _matched=true;
        res.add(AyinTypeProvider.underscoreType);
      }
    }
    if (!_matched) {
      if (e instanceof AyinBlock) {
        _matched=true;
        res.add(AyinTypeProvider.blockType);
      }
    }
    if (!_matched) {
      res.add(AyinTypeProvider.voidType);
    }
    return res;
  }
  
  protected boolean _isConformant(final Void actualType, final Void expectedType) {
    return true;
  }
  
  protected boolean _isConformant(final Void actualType, final EObject expectedType) {
    return true;
  }
  
  protected boolean _isConformant(final EObject actualType, final Void expectedType) {
    return true;
  }
  
  protected boolean _isConformant(final EObject actualType, final AyinWildcard expectedType) {
    return true;
  }
  
  protected boolean _isConformant(final AyinDataType actualType, final AyinDataType expectedType) {
    int argumentCounter = 0;
    AyinData _datatype = actualType.getDatatype();
    AyinData _datatype_1 = expectedType.getDatatype();
    boolean res = Objects.equal(_datatype, _datatype_1);
    final EList<AyinType> actualArguments = actualType.getArguments();
    final EList<AyinType> expectedArguments = expectedType.getArguments();
    int _size = actualArguments.size();
    int _size_1 = expectedArguments.size();
    boolean _equals = (_size == _size_1);
    if (_equals) {
      for (final AyinType args : actualArguments) {
        {
          AyinType actualArg = actualArguments.get(argumentCounter);
          AyinType expectedArg = expectedArguments.get(argumentCounter);
          res = (res && this.isConformant(actualArg, expectedArg));
          argumentCounter++;
        }
      }
    } else {
      return false;
    }
    return res;
  }
  
  protected boolean _isConformant(final AyinEntityType actualType, final AyinEntityType expectedType) {
    boolean _xblockexpression = false;
    {
      AyinEntity _entity = expectedType.getEntity();
      if ((_entity instanceof AyinParentable)) {
        AyinEntity _entity_1 = actualType.getEntity();
        final List<AyinParentable> hier = AyinModelUtil.parentHierarchy(((AyinParentable) _entity_1));
        boolean _contains = hier.contains(expectedType.getEntity());
        if (_contains) {
          return true;
        }
      }
      AyinEntity _entity_2 = actualType.getEntity();
      AyinEntity _entity_3 = expectedType.getEntity();
      _xblockexpression = Objects.equal(_entity_2, _entity_3);
    }
    return _xblockexpression;
  }
  
  protected boolean _isConformant(final AyinPrimitiveType actualType, final AyinPrimitiveType expectedType) {
    final String actual = actualType.getName();
    final String expected = expectedType.getName();
    if ((Objects.equal(actual, "i8") && Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("i8", "i16", "i32", "i64")).contains(expected))) {
      return true;
    }
    if ((Objects.equal(actual, "i16") && Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("i16", "i32", "i64")).contains(expected))) {
      return true;
    }
    if ((Objects.equal(actual, "i32") && Collections.<String>unmodifiableList(CollectionLiterals.<String>newArrayList("i32", "i64")).contains(expected))) {
      return true;
    }
    return Objects.equal(actual, expected);
  }
  
  protected boolean _isConformant(final AyinType actual, final AyinType expected) {
    boolean _xblockexpression = false;
    {
      boolean _equals = Objects.equal(expected, AyinTypeProvider.underscoreType);
      if (_equals) {
        return true;
      }
      if ((Objects.equal(expected, AyinTypeProvider.callbackType) && Objects.equal(actual, AyinTypeProvider.blockType))) {
        return true;
      }
      _xblockexpression = Objects.equal(actual, expected);
    }
    return _xblockexpression;
  }
  
  protected static boolean _isEquals(final AyinDataType type1, final AyinDataType type2) {
    String _name = type1.getDatatype().getName();
    String _name_1 = type2.getDatatype().getName();
    return Objects.equal(_name, _name_1);
  }
  
  protected static boolean _isEquals(final AyinEntityType type1, final AyinEntityType type2) {
    AyinEntity _entity = type1.getEntity();
    AyinEntity _entity_1 = type2.getEntity();
    return Objects.equal(_entity, _entity_1);
  }
  
  protected static boolean _isEquals(final AyinPrimitiveType type1, final AyinPrimitiveType type2) {
    String _name = type1.getName();
    String _name_1 = type2.getName();
    return Objects.equal(_name, _name_1);
  }
  
  protected static boolean _isEquals(final AyinType type1, final AyinType type2) {
    return Objects.equal(type1, type2);
  }
  
  public boolean isConformant(final EObject actualType, final EObject expectedType) {
    if (actualType instanceof AyinDataType
         && expectedType instanceof AyinDataType) {
      return _isConformant((AyinDataType)actualType, (AyinDataType)expectedType);
    } else if (actualType instanceof AyinEntityType
         && expectedType instanceof AyinEntityType) {
      return _isConformant((AyinEntityType)actualType, (AyinEntityType)expectedType);
    } else if (actualType instanceof AyinPrimitiveType
         && expectedType instanceof AyinPrimitiveType) {
      return _isConformant((AyinPrimitiveType)actualType, (AyinPrimitiveType)expectedType);
    } else if (actualType instanceof AyinType
         && expectedType instanceof AyinType) {
      return _isConformant((AyinType)actualType, (AyinType)expectedType);
    } else if (actualType != null
         && expectedType instanceof AyinWildcard) {
      return _isConformant(actualType, (AyinWildcard)expectedType);
    } else if (actualType != null
         && expectedType == null) {
      return _isConformant(actualType, (Void)null);
    } else if (actualType == null
         && expectedType != null) {
      return _isConformant((Void)null, expectedType);
    } else if (actualType == null
         && expectedType == null) {
      return _isConformant((Void)null, (Void)null);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(actualType, expectedType).toString());
    }
  }
  
  public static boolean isEquals(final AyinType type1, final AyinType type2) {
    if (type1 instanceof AyinDataType
         && type2 instanceof AyinDataType) {
      return _isEquals((AyinDataType)type1, (AyinDataType)type2);
    } else if (type1 instanceof AyinEntityType
         && type2 instanceof AyinEntityType) {
      return _isEquals((AyinEntityType)type1, (AyinEntityType)type2);
    } else if (type1 instanceof AyinPrimitiveType
         && type2 instanceof AyinPrimitiveType) {
      return _isEquals((AyinPrimitiveType)type1, (AyinPrimitiveType)type2);
    } else if (type1 != null
         && type2 != null) {
      return _isEquals(type1, type2);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(type1, type2).toString());
    }
  }
}
