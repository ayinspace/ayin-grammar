package space.ayin.lang;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import space.ayin.lang.AyinTypeProvider;
import space.ayin.lang.ayin.AyinAssignment;
import space.ayin.lang.ayin.AyinBlock;
import space.ayin.lang.ayin.AyinCallback;
import space.ayin.lang.ayin.AyinEntity;
import space.ayin.lang.ayin.AyinEvent;
import space.ayin.lang.ayin.AyinEventHandler;
import space.ayin.lang.ayin.AyinExpression;
import space.ayin.lang.ayin.AyinMethod;
import space.ayin.lang.ayin.AyinMethodCall;
import space.ayin.lang.ayin.AyinNewOperator;
import space.ayin.lang.ayin.AyinParameter;
import space.ayin.lang.ayin.AyinParentable;
import space.ayin.lang.ayin.AyinRef;
import space.ayin.lang.ayin.AyinStruct;
import space.ayin.lang.ayin.AyinStructLike;

@SuppressWarnings("all")
public class AyinModelUtil {
  public static boolean isNotConflicting(final AyinStruct s1, final AyinStruct s2) {
    int _size = AyinModelUtil.getParameters(s1).size();
    int _size_1 = AyinModelUtil.getParameters(s2).size();
    boolean res = (_size != _size_1);
    res = (res && Objects.equal(s1.getName(), s2.getName()));
    for (int i = 0; (i < AyinModelUtil.getParameters(s1).size()); i++) {
      {
        final AyinParameter p1 = AyinModelUtil.getParameters(s1).get(i);
        final AyinParameter p2 = AyinModelUtil.getParameters(s2).get(i);
        res = (res && AyinModelUtil.isNotConflicting(p1, p2));
      }
    }
    return res;
  }
  
  public static boolean isNotConflicting(final AyinParameter p1, final AyinParameter p2) {
    return (Objects.equal(p1.getName(), p2.getName()) && AyinTypeProvider.isEquals(p1.getType(), p2.getType()));
  }
  
  public static boolean isNotConflicting(final AyinMethod m1, final AyinMethod m2) {
    String _name = m1.getName();
    String _name_1 = m2.getName();
    boolean res = Objects.equal(_name, _name_1);
    if (((m1.getParamStruct() == null) && (m2.getParamStruct() == null))) {
      return res;
    }
    res = (res && AyinModelUtil.isNotConflicting(m1.getParamStruct(), m2.getParamStruct()));
    return res;
  }
  
  public static boolean isNotConflicting(final AyinEvent m1, final AyinEvent m2) {
    String _name = m1.getName();
    String _name_1 = m2.getName();
    return Objects.equal(_name, _name_1);
  }
  
  /**
   * This method returns all parent's hierarchy of AyinParentable
   * (i.e. parent, parent of parent, parent of parent of parent, etc.)
   */
  public static List<AyinParentable> parentHierarchy(final AyinParentable parentable) {
    final ArrayList<AyinParentable> visited = new ArrayList<AyinParentable>();
    AyinParentable current = parentable.getParent();
    while (((current != null) && (!visited.contains(current)))) {
      {
        visited.add(current);
        current = current.getParent();
      }
    }
    return visited;
  }
  
  /**
   * This method returns all parameters defined before expressions
   * this includes:
   *  1. all parameters of this block defined before
   *  2. all parameters of outer block defined before the entrance in current block
   *  3. parameters of event for current event handler
   *  4. parameters of method (if we are inside script method)
   *  5. parameters of callback (if we are inside callback block)
   */
  public static List<AyinParameter> parametersDefinedBefore(final AyinExpression expression) {
    final ArrayList<AyinParameter> parameters = new ArrayList<AyinParameter>();
    parameters.addAll(AyinModelUtil.getParameters(expression));
    final Map.Entry<EObject, AyinBlock> entry = AyinModelUtil.<AyinBlock>getContainerOfTypeWithElem(expression, AyinBlock.class);
    final AyinBlock block = entry.getValue();
    EObject _key = entry.getKey();
    final AyinExpression element = ((AyinExpression) _key);
    parameters.addAll(AyinModelUtil.parameterDefinedBeforeInBlock(element, block));
    EObject elem = block;
    EObject cont = block.eContainer();
    while ((cont != null)) {
      {
        if ((cont instanceof AyinBlock)) {
          parameters.addAll(AyinModelUtil.parameterDefinedBeforeInBlock(((AyinExpression) elem), ((AyinBlock)cont)));
        }
        elem = cont;
        cont = elem.eContainer();
      }
    }
    return parameters;
  }
  
  /**
   * TODO: add documentation
   */
  public static List<AyinParameter> parameterDefinedBeforeInBlock(final AyinExpression expression, final AyinBlock block) {
    final ArrayList<AyinParameter> parameters = new ArrayList<AyinParameter>();
    final int index = block.getExpressions().indexOf(expression);
    for (int i = 0; (i < index); i++) {
      {
        final AyinExpression expr = block.getExpressions().get(i);
        if ((expr instanceof AyinAssignment)) {
          final AyinAssignment assign = ((AyinAssignment)expr);
          final List<AyinParameter> params = IterableExtensions.<AyinParameter>toList(Iterables.<AyinParameter>filter(assign.getLeft(), AyinParameter.class));
          parameters.addAll(params);
        } else {
          if ((expr instanceof AyinParameter)) {
            parameters.add(((AyinParameter)expr));
          }
        }
      }
    }
    final AyinAssignment assignment = EcoreUtil2.<AyinAssignment>getContainerOfType(block, AyinAssignment.class);
    if (((assignment != null) && (assignment.getLeft().size() == 1))) {
      final AyinExpression left = assignment.getLeft().get(0);
      if ((left instanceof AyinRef)) {
        final AyinRef ayinRef = ((AyinRef)left);
        AyinParameter _ref = ayinRef.getRef();
        if ((_ref instanceof AyinCallback)) {
          AyinParameter _ref_1 = ayinRef.getRef();
          final AyinCallback callback = ((AyinCallback) _ref_1);
          parameters.addAll(callback.getArguments());
        }
      }
    }
    return parameters;
  }
  
  /**
   * TODO: add documentation
   */
  public static <T extends EObject> Map.Entry<EObject, T> getContainerOfTypeWithElem(final EObject element, final Class<T> type) {
    EObject elem = element;
    EObject cont = elem;
    while ((cont != null)) {
      {
        boolean _isInstance = type.isInstance(cont);
        if (_isInstance) {
          T _cast = type.cast(cont);
          return new AbstractMap.SimpleEntry<EObject, T>(elem, _cast);
        }
        elem = cont;
        cont = cont.eContainer();
      }
    }
    return new AbstractMap.SimpleEntry<EObject, T>(elem, null);
  }
  
  protected static List<AyinParameter> _getParameters(final AyinMethodCall methodCall) {
    final ArrayList<AyinParameter> parameters = new ArrayList<AyinParameter>();
    parameters.addAll(methodCall.getMethod().getParamStruct().getFields());
    parameters.addAll(methodCall.getMethod().getCallbacks());
    parameters.addAll(AyinModelUtil.getParameters(methodCall.eContainer()));
    return parameters;
  }
  
  protected static List<AyinParameter> _getParameters(final AyinEventHandler eventHandler) {
    final ArrayList<AyinParameter> parameters = new ArrayList<AyinParameter>();
    List<AyinParameter> _elvis = null;
    AyinEvent _event = null;
    if (eventHandler!=null) {
      _event=eventHandler.getEvent();
    }
    AyinStruct _paramStruct = null;
    if (_event!=null) {
      _paramStruct=_event.getParamStruct();
    }
    EList<AyinParameter> _fields = null;
    if (_paramStruct!=null) {
      _fields=_paramStruct.getFields();
    }
    if (_fields != null) {
      _elvis = _fields;
    } else {
      List<AyinParameter> _emptyList = Collections.<AyinParameter>emptyList();
      _elvis = _emptyList;
    }
    parameters.addAll(_elvis);
    parameters.addAll(AyinModelUtil.getParameters(eventHandler.eContainer()));
    return parameters;
  }
  
  protected static List<AyinParameter> _getParameters(final AyinNewOperator newOperator) {
    final ArrayList<AyinParameter> parameters = new ArrayList<AyinParameter>();
    AyinEntity _entity = newOperator.getEntity();
    if ((_entity instanceof AyinStructLike)) {
      AyinEntity _entity_1 = newOperator.getEntity();
      parameters.addAll(((AyinStructLike) _entity_1).getFields());
    }
    parameters.addAll(AyinModelUtil.getParameters(newOperator.eContainer()));
    return parameters;
  }
  
  protected static List<AyinParameter> _getParameters(final AyinMethod method) {
    final ArrayList<AyinParameter> parameters = new ArrayList<AyinParameter>();
    parameters.addAll(method.getResultStruct().getFields());
    parameters.addAll(method.getParamStruct().getFields());
    parameters.addAll(AyinModelUtil.getParameters(method.eContainer()));
    return parameters;
  }
  
  protected static List<AyinParameter> _getParameters(final EObject obj) {
    return AyinModelUtil.getParameters(obj.eContainer());
  }
  
  protected static List<AyinParameter> _getParameters(final Void obj) {
    return Collections.<AyinParameter>emptyList();
  }
  
  public static List<AyinParameter> getParameters(final EObject methodCall) {
    if (methodCall instanceof AyinMethodCall) {
      return _getParameters((AyinMethodCall)methodCall);
    } else if (methodCall instanceof AyinNewOperator) {
      return _getParameters((AyinNewOperator)methodCall);
    } else if (methodCall instanceof AyinEventHandler) {
      return _getParameters((AyinEventHandler)methodCall);
    } else if (methodCall instanceof AyinMethod) {
      return _getParameters((AyinMethod)methodCall);
    } else if (methodCall != null) {
      return _getParameters(methodCall);
    } else if (methodCall == null) {
      return _getParameters((Void)null);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(methodCall).toString());
    }
  }
}
